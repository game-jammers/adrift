{
    "title": "Addison Missive Fragment",
    "message": "As excited as I am, I'm afraid I must cut this correspondence short. I fear whatever is afflicting Cooper is beginning to take hold of me as well.  I can feel the fever rising in me as I write."
}
