//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using System.Collections.Generic;

namespace blacktriangles
{
    public static class EnumExtension
    {
        //
        // --------------------------------------------------------------------
        //

        public static int ToFlag<EnumType>(this EnumType e)
            where EnumType: System.Enum
        {
            return 1 << System.Convert.ToInt32(e);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static bool IsInFlags<EnumType>(this EnumType e, int flags)
            where EnumType: System.Enum
        {
            return flags.IsFlagSet<EnumType>(e);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static int ToInt32<EnumType>(this EnumType e)
        {
            return System.Convert.ToInt32(e);
        }
    }
}
