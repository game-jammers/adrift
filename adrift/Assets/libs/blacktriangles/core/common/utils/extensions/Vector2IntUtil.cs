//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public static class Vector2IntUtil
    {
        public static Vector2Int Max(Vector2Int lhs, Vector2Int rhs)
        {
            return new Vector2Int(
                btMath.Max(lhs.x, rhs.x),
                btMath.Max(lhs.y, rhs.y)
            );
        }
    }
}
