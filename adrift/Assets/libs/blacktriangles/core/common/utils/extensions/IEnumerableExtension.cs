//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//=============================================================================

using System.Collections;
using System.Collections.Generic;

namespace blacktriangles
{
    public static class IEnumerableExtension
    {
        //
        // --------------------------------------------------------------------
        //

        public static List<T> Combine<T>( this IEnumerable<T> self, IEnumerable<T> other)
        {
            List<T> combined = new List<T>();
            combined.AddRange(self);
            combined.AddRange(other);
            return combined;
        }

        //
        // --------------------------------------------------------------------
        //

        public static T Find<T>( this IEnumerable<T> self, System.Predicate<T> pred )
            where T: class
        {
            foreach( T t in self )
            {
                if( pred(t) ) return t;
            }
            return null;
        }

        //
        // --------------------------------------------------------------------
        //

        public static T[] FindAll<T>( this IEnumerable<T> self, System.Predicate<T> pred )
            where T: class
        {
            List<T> result = new List<T>();
            foreach( T item in self )
            {
                if( pred(item) ) result.Add(item);
            }
            return result.ToArray();
        }

        //
        // --------------------------------------------------------------------
        //

    }
}
