//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//=============================================================================

namespace blacktriangles
{
    public static class DateTimeExtension
    {
        public static bool IsValid( this System.DateTime self )
        {
            return self.ToBinary() == 0;
        }
    }
}
