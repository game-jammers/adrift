//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;

namespace blacktriangles
{
    [System.Serializable]
    public struct IntVec3
        : IJsonSerializable
    {
        // constants //////////////////////////////////////////////////////////////
        public static readonly IntVec3 zero                     = new IntVec3( 0, 0, 0 );
        public static readonly IntVec3 one                      = new IntVec3( 1, 1, 1 );

        // members ////////////////////////////////////////////////////////////////
        public int x;
        public int y;
        public int z;

        public float magnitude                                   { get { return GetMagnitude(); } }
        public float sqrMagnitude                                { get { return GetSqrMagnitude(); } }

        // constructors ///////////////////////////////////////////////////////////
        public IntVec3( int _x, int _y, int _z )
            : this()
        {
            x = _x;
            y = _y;
            z = _z;
        }

        public IntVec3( float _x, float _y, float _z )
            : this( (int)_x, (int)_y, (int)_z )
        {
        }

        public IntVec3( IntVec3 copy )
            : this( copy.x, copy.y, copy.z )
        {
        }

        public IntVec3( Vector3 floatVec3 )
            : this( floatVec3.x, floatVec3.y, floatVec3.z )
        {
        }

        public static IntVec3 FromVector3Floor( Vector3 vec3 )
        {
            IntVec3 result = new IntVec3();
            result.x = (int)btMath.Floor(vec3.x);
            result.y = (int)btMath.Floor(vec3.y);
            result.z = (int)btMath.Floor(vec3.z);
            return result;
        }

        public static IntVec3 FromVector3Ceil( Vector3 vec3 )
        {
            IntVec3 result = new IntVec3();
            result.x = (int)btMath.Ceil(vec3.x);
            result.y = (int)btMath.Ceil(vec3.y);
            result.z = (int)btMath.Ceil(vec3.z);
            return result;
        }

        public static IntVec3 FromVector3Round( Vector3 vec3 )
        {
            IntVec3 result = new IntVec3();
            result.x = (int)btMath.Round(vec3.x);
            result.y = (int)btMath.Round(vec3.y);
            result.z = (int)btMath.Round(vec3.z);
            return result;
        }

        // public methods /////////////////////////////////////////////////////////
        public override bool Equals( object o )
        {
            IntVec3 test = (IntVec3)o;
            if( o == null ) return false;

            return (test == this);
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        // operators //////////////////////////////////////////////////////////////
        public override string ToString()
        {
            return System.String.Format( "[{0},{1},{2}]", x, y, z );
        }

        public Vector3 ToVector3()
        {
            return new Vector3( x, y, z );
        }

        public IntVec2 ToIntVec2()
        {
            return new IntVec2( x, y );
        }

        public static IntVec3 Min( IntVec3 vec1, IntVec3 vec2 )
        {
            return new IntVec3( vec1.x < vec2.x ? vec1.x : vec2.x,  
                                vec1.y < vec2.y ? vec1.y : vec2.y,
                                vec1.z < vec2.z ? vec1.z : vec2.z );
        }

        public static IntVec3 Max( IntVec3 vec1, IntVec3 vec2 )
        {
            return new IntVec3( vec1.x > vec2.x ? vec1.x : vec2.x, 
                                vec1.y > vec2.y ? vec1.y : vec2.y,
                                vec1.z > vec2.z ? vec1.z : vec2.z );
        }

        public static IntRect CreateRect( IntVec3 vec1, IntVec3 vec2 )
        {
            IntVec3 min = Min( vec1, vec2 );
            IntVec3 max = Max( vec1, vec2 );
            return new IntRect( min.ToIntVec2(), max.ToIntVec2() );
        }

        public static bool operator==( IntVec3 lhs, IntVec3 rhs )
        {
            return ( lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z );
        }

        public static bool operator!=( IntVec3 lhs, IntVec3 rhs )
        {
            return !( lhs == rhs );
        }

        public static IntVec3 operator+( IntVec3 lhs, IntVec3 rhs )
        {
            return new IntVec3( lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z );
        }

        public static IntVec3 operator-( IntVec3 lhs, IntVec3 rhs )
        {
            return new IntVec3( lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z );
        }

        public static IntVec3 operator*( IntVec3 lhs, IntVec3 rhs )
        {
            return new IntVec3( lhs.x * rhs.x, lhs.y * rhs.y, lhs.z * rhs.z );
        }

        /// vector3 ////////
        public static Vector3 operator*( IntVec3 lhs, Vector3 rhs )
        {
            return new Vector3( rhs.x * lhs.x, rhs.y * lhs.y, rhs.z * lhs.z );
        }

        public static Vector3 operator*( Vector3 lhs, IntVec3 rhs )
        {
            return rhs * lhs;
        }

        /// int ////////////
        public static IntVec3 operator/( IntVec3 lhs, int rhs )
        {
            return new IntVec3( lhs.x / rhs, lhs.y / rhs, lhs.z / rhs );
        }

        public static IntVec3 operator*( IntVec3 lhs, int rhs )
        {
            return new IntVec3( lhs.x * rhs, lhs.y * rhs, lhs.z * rhs );
        }

        /// float //////////
        public static Vector3 operator/( IntVec3 lhs, float rhs )
        {
            return new Vector3( lhs.x / rhs, lhs.y / rhs, lhs.z / rhs );
        }

        public static Vector3 operator*( IntVec3 lhs, float rhs )
        {
            return new Vector3( lhs.x * rhs, lhs.y * rhs, lhs.z / rhs );
        }

        /// json //////////
        public JsonObject ToJson()
        {
            JsonObject result = new JsonObject();
            result["x"] = x;
            result["y"] = y;
            result["z"] = z;
            return result;
        }

        public void FromJson( JsonObject json )
        {
            x = json.GetField<int>( "x" );
            y = json.GetField<int>( "y" );
            z = json.GetField<int>( "z" );
        }

        // private methods ////////////////////////////////////////////////////
        private float GetSqrMagnitude()
        {
            return x*x + y*y + z*z;
        }

        private float GetMagnitude()
        {
            return btMath.Sqrt( GetSqrMagnitude() );
        }
    }
}
