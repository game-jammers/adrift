//
// (c) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//

using Generic.Math;
using UnityEngine;
using System.Collections.Generic;

namespace blacktriangles
{
    [System.Serializable]
    public class BaseSmoothValue
    {
        public virtual object smoothedRaw                       { get { return null; } }
        public virtual object lastRaw                           { get { return null; } }
    }

    //
    // ////////////////////////////////////////////////////////////////////////
    //

    [System.Serializable]
    public class SmoothValue<T>
        : BaseSmoothValue
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public T total                                          { get { return GetTotalValue(); } }
        public T smoothed                                       { get { return GetSmoothedValue(); } }
        public T last                                           { get; private set; }
        public int size                                         { get { return values.Count; } }
        public int historySize                                  { get; set; }
    
        public Queue<T> values                                  { get; private set; }

        public override object smoothedRaw                      { get { return smoothed; } }
        public override object lastRaw                          { get { return last; } }

        private bool dirty;
        private T cachedTotal;
        private T defval;

        //
        // constructor / destructor ///////////////////////////////////////////
        //
    
        public SmoothValue( int _historySize )
            : this( _historySize, default(T) )
        {
        }

        //
        // --------------------------------------------------------------------
        //
        
        public SmoothValue( int _historySize, T _defval )
        {
            defval = _defval;
            last = defval;
            historySize = _historySize;
            values = new Queue<T>( historySize );

            dirty = false;
            cachedTotal = defval;
        }
    
        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public T Add( T value )
        {
            dirty = true;

            last = value;
            if( values.Count >= historySize )
            {
                values.Dequeue();
            }
    
            values.Enqueue( value );
            return GetSmoothedValue();
        }
    
        //
        // --------------------------------------------------------------------
        //
    
        public void Set( T value )
        {
            Reset();
            Add( value );
        }
    
        //
        // --------------------------------------------------------------------
        //
    
        public void Reset()
        {
            values.Clear();
        }
    
        //
        // private methods ////////////////////////////////////////////////////
        //
    
        private T GetSmoothedValue()
        {
            T result = default(T);
            if( values != null && values.Count > 0 )
            {
                try
                {
                    dynamic sized = size;
                    dynamic totald = total;

                    result = (T)totald / sized;
                }
                catch(System.Exception ex)
                {
                    Dbg.Error(ex.ToString());
                }
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //
        
        private T GetTotalValue()
        {
            if(dirty)
            {
                dirty = false;
                dynamic result = default(T);
                foreach(T val in values)
                {
                    dynamic v = val;
                    result += v;
                }

                cachedTotal = (T)result;
            }

            return cachedTotal;
        }
    }
}
