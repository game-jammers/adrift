//
// blacktriangles (c) 2019
//

using UnityEngine;

namespace blacktriangles
{

    //
    // Cardinal Directions ////////////////////////////////////////////////////
    //

    public enum CardinalDirection
    {
        North = 0,
        East = 1,
        South = 2,
        West = 3
    }

    //
    // Utilities //////////////////////////////////////////////////////////////
    //

    public static class CardinalDirectionUtils
    {
        //
        // --------------------------------------------------------------------
        //
        
        public static CardinalDirection Inverse(CardinalDirection dir)
        {
            switch(dir)
            {
                case CardinalDirection.North: return CardinalDirection.South;
                case CardinalDirection.South: return CardinalDirection.North;
                case CardinalDirection.East: return CardinalDirection.West;
                case CardinalDirection.West: return CardinalDirection.East;
            }

            throw new System.ArgumentException("Invalid input direction dir");
        }

        //
        // --------------------------------------------------------------------
        //

        public static CardinalDirection RotateClockwise(CardinalDirection dir, uint steps = 1)
        {
            uint rotated = (uint)dir + steps;
            rotated = rotated % 4;
            return (CardinalDirection)rotated;
        }

        //
        // --------------------------------------------------------------------
        //

        public static Vector3 ToDirectionXZ(CardinalDirection dir)
        {
            switch(dir)
            {
                case CardinalDirection.North: return Vector3.forward;
                case CardinalDirection.South: return Vector3.back;
                case CardinalDirection.East: return Vector3.right;
                case CardinalDirection.West: return Vector3.left;
            }

            throw new System.ArgumentException("Invalid input direction dir");
        }

        public static IntVec2 ToIntVec2(CardinalDirection dir)
        {
            switch(dir)
            {
                case CardinalDirection.North: return IntVec2.north;
                case CardinalDirection.South: return IntVec2.south;
                case CardinalDirection.East: return IntVec2.east;
                case CardinalDirection.West: return IntVec2.west;
            }
            throw new System.ArgumentException("Invalid input direction dir");
        }
    }
}
    
