//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using Generic.Math;
using UnityEngine;


namespace blacktriangles
{
    public struct Accumilator<T>
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public T current                                  { get; private set; }
        public T last                                     { get; private set; }
        public T delta                                    { get { return GenericMath.Subtract(current, last); } }

        //
        // constructor ////////////////////////////////////////////////////////
        //
       
        public Accumilator(T curr)
        {
            current = curr;
            last = curr;
        }

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public void Add(T _delta)
        {
            Set(GenericMath.Add(current, _delta));
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void Set(T newValue)
        {
            last = current;
            current = newValue;
        }
    }
}
