//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;

namespace blacktriangles
{
    public static class StringUtils
    {
        // members //////////////////////////////////////////////////////////////

        private static readonly string kLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        private static readonly string[] kQuasiLatin = new string[]{
                                            "Prime",
                                            "Secundus",
                                            "Tertian",
                                            "Quadran",
                                            "Quintan",
                                            "Sextan",
                                            "Septam",
                                            "Octan",
                                            "Novam",
                                            "Decem"
                                        };

        // public methods ///////////////////////////////////////////////////////
        public static string GenerateRandomString( int seed, int length )
        {
            System.Random rand = new System.Random(seed);
            System.Text.StringBuilder result = new System.Text.StringBuilder();
            for( int i = 0; i < length; ++i )
            {
                result.Append( kLetters[ rand.Next(0, kLetters.Length-1) ] );
            }
            return result.ToString();
        }


        public static string ToQuasiLatin( int value )
        {
            int index = value-1;
            string result = "Omega";
            if( kQuasiLatin.IsValidIndex(index) )
                result = kQuasiLatin[index];

            return result;
        }


        public static string ToRomanNumeral( int value )
        {
            string result = System.String.Empty;
            while( value > 0 && value < 3999 )
            {
                if( value >= 1000 ) { result += "M"; value -= 1000; }
                if( value >= 900 ) { result += "CM"; value -= 900; }
                if( value >= 500 ) { result += "D"; value -= 500; }
                if( value >= 400 ) { result += "CD"; value -= 400; }
                if( value >= 100 ) { result += "C"; value -= 100; }
                if( value >= 90 ) { result += "XC"; value -= 90; }
                if( value >= 50 ) { result += "L"; value -= 50; }
                if( value >= 40 ) { result += "XL"; value -= 40; }
                if( value >= 10 ) { result += "X"; value -= 10; }
                if( value >= 9 ) { result += "IX"; value -= 9; }
                if( value >= 5 ) { result += "V"; value -= 5; }
                if( value >= 4 ) { result += "IV"; value -= 4; }
                if( value >= 1 ) { result += "I"; value -= 1; }
            }

            return result;
        }
    }
}
