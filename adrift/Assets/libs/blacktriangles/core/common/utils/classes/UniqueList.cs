//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections;
using System.Collections.Generic;

namespace blacktriangles
{
    public class UniqueList<T>
        : IEnumerable<T>
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        private List<T> items                                   = new List<T>();

        public int Count                                        { get { return items.Count; } }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public int Add(T t)
        {
            if(items.Count > 0)
            {
                int hash = t.GetHashCode();
                for(int i = 0; i < items.Count; ++i)
                {
                    T other = items[i];
                    int otherHash = other.GetHashCode();
                    if(otherHash == hash)
                    {
                        return i;
                    }
                }
            }

            items.Add(t);
            return items.Count - 1;
        }

        //
        // --------------------------------------------------------------------
        //

        public T this[int idx]
        {
            get
            {
                return items[idx];
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public T[] ToArray()
        {
            return items.ToArray();
        }
        
        //
        // IEnumerable<T> /////////////////////////////////////////////////////
        //

        public IEnumerator<T> GetEnumerator()
        {
            return items.GetEnumerator();
        }

        //
        // --------------------------------------------------------------------
        //

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
