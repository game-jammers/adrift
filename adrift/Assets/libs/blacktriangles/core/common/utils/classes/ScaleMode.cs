//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;

namespace blacktriangles
{
    public enum ScaleMode { BestFit, Stretch, Center, None };
    public static class ScaleUtility
    {
        public static Rect GetBestFitRect( Rect srcRect, Rect dstRect )
        {
            Rect result = dstRect;
            float srcRatio = srcRect.width / srcRect.height;
            float tarRatio = dstRect.width / dstRect.height;

            if( tarRatio != srcRatio )
            {
                if( tarRatio > srcRatio )
                {
                    result.width =  srcRect.width * dstRect.height / srcRect.height;
                    result.height = dstRect.height;
                }
                else
                {
                    result.width = dstRect.width;
                    result.height = srcRect.height * dstRect.width / srcRect.width;
                }

                result.x += ( dstRect.width - result.width ) / 2f;
                result.y += ( dstRect.height - result.height ) / 2f;
            }

            return result;
        }

        public static Rect GetCenteredRect( Rect srcRect, Rect dstRect )
        {
            float x = (dstRect.width / 2f) - (srcRect.width/2f);
            float y = (dstRect.height / 2f) - (srcRect.height/2f);
            return new Rect( dstRect.x + x, dstRect.y + y, srcRect.width, srcRect.height );
        }
    }
}
