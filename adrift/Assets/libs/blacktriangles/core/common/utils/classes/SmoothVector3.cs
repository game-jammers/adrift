//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles
{
    public class SmoothVector3
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Vector3 total                                    { get { return GetTotalValue(); } }
        public Vector3 smoothed                                 { get { return GetSmoothedValue(); } }
        public Vector3 last                                     { get; private set; }
        public int size                                         { get { return values.Count; } }
        public int historySize                                  { get; set; }
    
        public Queue<Vector3> values                            { get; private set; }

        private bool dirty;
        private Vector3 cachedTotal;
        private Vector3 defval;

        //
        // constructor / destructor ///////////////////////////////////////////
        //
    
        public SmoothVector3( int _historySize )
            : this( _historySize, Vector3.zero )
        {
        }

        //
        // --------------------------------------------------------------------
        //
        
        public SmoothVector3( int _historySize, Vector3 _defval )
        {
            defval = _defval;
            last = defval;
            historySize = _historySize;
            values = new Queue<Vector3>( historySize );

            dirty = false;
            cachedTotal = defval;
        }
    
        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public Vector3 Add( Vector3 value )
        {
            dirty = true;

            last = value;
            if( values.Count >= historySize )
            {
                values.Dequeue();
            }
    
            values.Enqueue( value );
            return GetSmoothedValue();
        }
    
        //
        // --------------------------------------------------------------------
        //
    
        public void Set( Vector3 value )
        {
            Reset();
            Add( value );
        }
    
        //
        // --------------------------------------------------------------------
        //
    
        public void Reset()
        {
            values.Clear();
        }
    
        //
        // private methods ////////////////////////////////////////////////////
        //
    
        private Vector3 GetSmoothedValue()
        {
            Vector3 result = Vector3.zero;
            if( values != null && values.Count > 0 )
            {
                result = total / size;
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //
        
        private Vector3 GetTotalValue()
        {
            if(dirty)
            {
                dirty = false;
                Vector3 result = Vector3.zero;
                foreach(Vector3 val in values)
                {
                    result += val;
                }

                cachedTotal = result;
            }

            return cachedTotal;
        }

    }
}
