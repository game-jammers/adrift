//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.Collections.Generic;
using System.Reflection;

namespace blacktriangles
{
    public static class AssemblyUtility
    {
        //
        // constants //////////////////////////////////////////////////////////
        //

        private static readonly string kEditorAssemblyName      = "Assembly-CSharp-Editor";
        private static readonly string kGameAssemblyName        = "Assembly-CSharp";
        private static readonly string kBtCoreAssemblyName       = "btcore";

        //
        // public methods /////////////////////////////////////////////////////
        //
        public static Assembly GetEditorAssembly()
        {
            return GetAssembly((asm)=>asm.GetName().Name.StartsWith(kEditorAssemblyName));
        }

        //
        // --------------------------------------------------------------------
        //

        public static Assembly GetGameAssembly()
        {
            return GetAssembly((asm)=>asm.GetName().Name.StartsWith(kGameAssemblyName));
        }

        //
        //
        //

        public static Assembly GetBtCoreAssembly()
        {
            return GetAssembly((asm)=>asm.GetName().Name == kBtCoreAssemblyName);
        }

        //
        // --------------------------------------------------------------------
        //

        public static Assembly GetAssembly(System.Predicate<Assembly> filter)
        {
            Assembly[] asms = GetAllAssemblies();
            foreach(Assembly asm in asms)
            {
                if(filter(asm))
                    return asm;
            }

            return null;
        }

        //
        // --------------------------------------------------------------------
        //

        public static List<Assembly> GetAssemblies(System.Predicate<Assembly> filter)
        {
            List<Assembly> result = new List<Assembly>();
            Assembly[] assemblies = GetAllAssemblies();
            foreach(Assembly asm in assemblies)
            {
                if(filter(asm))
                {
                    result.Add(asm);
                }
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public static Assembly[] GetAllAssemblies()
        {
            return System.AppDomain.CurrentDomain.GetAssemblies();
        }

        //
        // --------------------------------------------------------------------
        //

        public static string[] GetAllAssemblyNames()
        {
            List<string> result = new List<string>();
            Assembly[] assemblies = GetAllAssemblies();
            foreach( Assembly asm in assemblies )
             {
                result.Add( asm.GetName().Name );
            }
            return result.ToArray();
        }

        //
        // --------------------------------------------------------------------
        //

        public static System.Type[] CollectTypesWithAttribute<T>()
        {
            return CollectTypesWithAttribute<T>( Assembly.GetExecutingAssembly() );
        }

        //
        // --------------------------------------------------------------------
        //

        public static System.Type[] CollectDescendantsOf<ClassType>()
            where ClassType: class
        {
            return CollectDescendantsOf<ClassType>( Assembly.GetExecutingAssembly() );
        }

        //
        // --------------------------------------------------------------------
        //

        public static List<System.Type> CollectAllOfType(System.Type type)
        {
            List<System.Type> result = new List<System.Type>() { type };
            Assembly[] asms = GetAllAssemblies();
            foreach(Assembly asm in asms)
            {
                result.AddRange(CollectDescendantsOf(type, asm));
            }
            return result;
        }
        
        //
        // --------------------------------------------------------------------
        //

        public static List<System.Type> CollectAllOfType<ClassType>()
            where ClassType: class
        {
            return CollectAllOfType(typeof(ClassType));
        }

        //
        // --------------------------------------------------------------------
        //

        public static System.Type[] CollectEditorTypesWithAttribute<T>()
        {
            return CollectTypesWithAttribute<T>( GetEditorAssembly() );
        }

        //
        // --------------------------------------------------------------------
        //

        public static System.Type[] CollectTypesWithAttribute<T>( Assembly asm  )
        {
            return CollectTypes( asm, (type)=>{
                return System.Attribute.IsDefined( type, typeof( T ) );
            });
        }

        //
        // --------------------------------------------------------------------
        //

        public static System.Type[] CollectDescendantsOf<ClassType>( Assembly asm )
        {
            return CollectDescendantsOf(typeof(ClassType), asm);
        }

        //
        // ----------------------------------------------------------------------------
        //

        public static System.Type[] CollectDescendantsOf(System.Type t, Assembly asm)
        {
            return CollectTypes( asm, (type)=>{
                return type.IsSubclassOf(t);
            });
        }

        //
        // --------------------------------------------------------------------
        //

        public static System.Type[] CollectWithInterface<InterfaceType>( Assembly asm, bool isInterface = false, bool isAbstract = false )
        {
            System.Type interfaceType = typeof(InterfaceType);
            return CollectTypes( asm, (type)=>{
                return ( interfaceType.IsAssignableFrom( type ) && type.IsInterface == isInterface && type.IsAbstract == isAbstract );
            });
        }

        //
        // --------------------------------------------------------------------
        //

        public static System.Type[] CollectAllTypes( System.Predicate<System.Type> predicate)
        {
            List<System.Type> types = new List<System.Type>();
            Assembly[] asms = GetAllAssemblies();
            foreach(Assembly asm in asms)
            {
                types.AddRange(CollectTypes(asm, predicate));
            }

            return types.ToArray();
        }

        //
        // ----------------------------------------------------------------------------
        //

        public static System.Type[] CollectTypes( Assembly asm, System.Predicate<System.Type> predicate )
        {
            List<System.Type> result = new List<System.Type>();
            foreach( System.Type type in asm.GetTypes() )
            {
                if( predicate( type ) )
                {
                    result.Add( type );
                }
            }

            return result.ToArray();
        }

        //
        // --------------------------------------------------------------------
        //

        public static T GetAttribute<T>( System.Type type )
            where T: System.Attribute
        {
            T result = null;
            System.Reflection.MemberInfo info = type;
            System.Object[] attributes = info.GetCustomAttributes( typeof( T ), true );
            if( attributes != null && attributes.Length > 0 )
            {
                 result = (T)attributes[0];
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //


        public static T GetAndStoreAttribute<T>( System.Type type, ref T store )
            where T: System.Attribute
        {
            if( store == null )
            {
                store = GetAttribute<T>( type );
            }

            Dbg.Assert( store != null, "Attempted to get and store attribute of type " + typeof(T).ToString() + " but it did not exist." );
            return store;
        }

        //
        // End Class //////////////////////////////////////////////////////////
        //
    }
}
