//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using Jint;
using System.Collections.Generic;

namespace blacktriangles
{
    public static class JintUtility
    {
        //
        // public methods /////////////////////////////////////////////////////
        //

        public static Dictionary<string, T> Enum<T>()
            where T: System.Enum
        {
            var result = new Dictionary<string, T>();
            string[] names = System.Enum.GetNames(typeof(T));
            T[] values = EnumUtility.GetValues<T>();
            for(int i = 0; i < names.Length; ++i)
            {
                result[names[i]] = values[i];
            }

            return result;
        }
    }
}
