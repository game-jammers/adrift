//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//

using System.Collections.Generic;
using System.Reflection;

namespace blacktriangles
{
    public class BaseRegistry< KeyType, BaseType, AttributeType >
        where BaseType: class
        where AttributeType: System.Attribute
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public class Entry
        {
            public System.Type type                             = null;
            public AttributeType attribute                      = null;
        }

        //
        // members ////////////////////////////////////////////////////////////
        //

        private Assembly assembly                               = null;
        private bool isInitialized                              = false;
        public Dictionary<KeyType, Entry> registry              { get; private set; }

        //
        // constructor / initializer //////////////////////////////////////////
        //
        public BaseRegistry()
        {
        }

        //
        // --------------------------------------------------------------------
        //

        public BaseRegistry( Assembly asm )
        {
            assembly = asm;
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Initialize( System.Func< AttributeType, KeyType > converter )
        {
            if( !isInitialized )
            {
                registry = new Dictionary<KeyType,Entry>();

                if( assembly != null )
                {
                    ExtractFromAssembly( assembly, converter );
                }
                else
                {
                    Assembly[] assemblies = System.AppDomain.CurrentDomain.GetAssemblies();
                    foreach( Assembly asm in assemblies )
                    {
                        ExtractFromAssembly( asm, converter );
                    }
                }

                isInitialized = true;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public BaseType Instantiate( KeyType key, params object[] constructionParameters )
        {
            if( !isInitialized ) return null;
            Dbg.Assert( key != null, "Key cannot be null" );

            BaseType result = null;
            System.Type type = GetSystemTypeForKey( key );
            if( type != null )
            {
                result = System.Activator.CreateInstance( type, constructionParameters ) as BaseType;
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public AttributeType GetAttributeForKey( KeyType key )
        {
            if( !isInitialized ) return null;
            Dbg.Assert( key != null, "Key cannot be null" );

            AttributeType attribute = null;
            Entry entry = null;
            if( registry.TryGetValue( key, out entry ) )
            {
                attribute = entry.attribute;
            }

            return attribute;
        }

        //
        // --------------------------------------------------------------------
        //

        public System.Type GetSystemTypeForKey( KeyType key )
        {
            if( !isInitialized ) return null;
            Dbg.Assert( key != null, "Key cannot be null" );

            System.Type result = null;
            Entry entry = null;
            if( registry.TryGetValue( key, out entry ) )
            {
                result = entry.type;
            }
            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public List<KeyType> GetAllKeys()
        {
              return new List<KeyType>( registry.Keys );
        }

        //
        // --------------------------------------------------------------------
        //

        public List<System.Type> GetAllTypes()
        {
            return blacktriangles.Convert.All<Entry,System.Type>( registry.Values, (e)=>{ return e.type; } );
        }

        //
        // --------------------------------------------------------------------
        //

        public List<AttributeType> GetAllAttributes()
        {
            return blacktriangles.Convert.All<Entry,AttributeType>( registry.Values, (e)=>{ return e.attribute; } );
        }

        //
        // --------------------------------------------------------------------
        //

        public void PrintRegistered( string header )
        {
            System.Console.WriteLine( header );
            foreach( var item in registry )
            {
                System.Console.WriteLine( "Key: {0}\tValue: {1}", item.Key.ToString(), item.Value.type.ToString() );
            }
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void ExtractFromAssembly( Assembly asm, System.Func< AttributeType, KeyType > converter )
        {
            System.Type[] typeArray = AssemblyUtility.CollectTypesWithAttribute<AttributeType>( asm );
            foreach( System.Type type in typeArray )
            {
                AttributeType attrib = AssemblyUtility.GetAttribute<AttributeType>( type );
                KeyType key = converter( attrib );
                Entry entry = new Entry();
                entry.type = type;
                entry.attribute = attrib;
                registry[ key ] = entry;
            }
        }
    }
}
