//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

namespace blacktriangles
{
    public static class Hash
    {
        private const int kOffsetBasis                          = unchecked((int)0x811c9dc5);
        private const int kPrime                                = unchecked((int)16777619);

        public static int Generate(params object[] objs)
        {
            int hash = kOffsetBasis;
            foreach(object obj in objs)
            {
                hash = hash * kPrime;
                hash = hash ^ obj.GetHashCode();
            }

            return hash;
        }
    }
}
