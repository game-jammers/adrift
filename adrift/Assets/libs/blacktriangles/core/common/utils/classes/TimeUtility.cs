//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;

namespace blacktriangles
{
    public static class TimeUtility
    {
        private static string PadNumber( double time )
        {
            time = System.Math.Abs(time);
            return time < 10.0 ? "0" + time.ToString() : time.ToString();
        }

        public static string Format( System.TimeSpan span )
        {
            return System.String.Format( @"{0}:{1}:{2}:{3}"
                , PadNumber( System.Math.Floor( span.TotalDays ) )
                , PadNumber( span.Hours )
                , PadNumber( span.Minutes )
                , PadNumber( span.Seconds )
            );
        }

        public static string Format( System.DateTime time )
        {
            return time.ToLocalTime().ToString();
        }
    }
}
