//
// (c) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//

using Generic.Math;

namespace blacktriangles
{
    [System.Serializable]
    public class Range<T>
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public T min;
        public T max;
        public T length                                         { get { return GenericMath.Subtract( max, min ); } }

        //
        // constructor / initializers /////////////////////////////////////////
        //
        public Range()
            : this( default(T), default(T) )
        {
        }

        //
        // --------------------------------------------------------------------
        //

        public Range( T _min, T _max )
        {
            min = _min;
            max = _max;
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public T Clamp( T val )
        {
            T result = GenericMath.LessThan( val, min ) ? min : val;
            result = GenericMath.GreaterThan( result, max ) ? max : result;
            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public bool IsInRange( T val )
        {
            return GenericMath.GreaterThan( val, min ) && GenericMath.LessThan( val, max );
        }
    }

    //
    // ########################################################################
    //

    [System.Serializable]
    public class IntRange
        : Range<int>
    {
        public IntRange( int min, int max )
            : base( min, max )
        {
        }

        public int Random()
        {
            return btRandom.Range( min, max );
        }

        public int Random( System.Random rng )
        {
            return rng.Next(min,max);
        }
    }

    //
    // ########################################################################
    //
    

    [System.Serializable]
    public class FloatRange
        : Range<float>
    {
        public FloatRange( float min, float max )
            : base( min, max )
        {
        }

        public float Random()
        {
            return btRandom.Range( min, max );
        }

        public float Random( System.Random rng )
        {
            return rng.NextFloat(min,max);
        }

        public float Normalized( float val )
        {
            return btMath.Clamp( (val-min)/(max-min), 0f, 1f);
        }
    }
}
