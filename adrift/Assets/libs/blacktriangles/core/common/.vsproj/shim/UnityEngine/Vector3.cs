﻿//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

namespace UnityEngine
{
    public struct Vector3
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public float x;
        public float y;
        public float z;


    }
}
