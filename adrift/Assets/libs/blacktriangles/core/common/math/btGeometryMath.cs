//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public static partial class btMath
    {
        //
        // --------------------------------------------------------------------
        //
        
        public static Vector3 CalcTriangleNormal(Vector3 v0, Vector3 v1, Vector3 v2)
        {
            Vector3 e0 = v1 - v0;
            Vector3 e1 = v2 - v0;
            return Vector3.Cross(e0, e1).normalized;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static Vector3 PointOnSphere(Vector3 pos)
        {
            return PointOnSphere(pos.x, pos.y, pos.z);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static Vector3 PointOnSphere(float degx, float degy, float r)
        {
            float radx = degx * Deg2Rad;
            float rady = degy * Deg2Rad;

            return new Vector3(
                r * Cos(rady) * Sin(radx),
                r * Sin(rady) * Sin(radx),
                r * Cos(radx)
            );
        }
    }
}
