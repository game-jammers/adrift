//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//=============================================================================

#define NOPRINT

namespace blacktriangles
{
    public static class Performance
    {
        public static void Profile( string name, System.Action command )
        {
            #if BT_DEBUG || UNITY_EDITOR
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            timer.Start();
            #endif

            command();

            #if BT_DEBUG || UNITY_EDITOR
            timer.Stop();
            Dbg.Log( "{0} took {1}", name, timer.Elapsed );
            #endif
        }

        public static void ProfileAssert( string name, double maxTime, System.Action command )
        {
            #if BT_DEBUG || UNITY_EDITOR
            System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();
            timer.Start();
            #endif

            command();

            #if BT_DEBUG || UNITY_EDITOR
            timer.Stop();
            if( timer.Elapsed.TotalSeconds >= maxTime )
                Dbg.Error( "{0} took {1}", name, timer.Elapsed );
            #endif
        }
    }
}
