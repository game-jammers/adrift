//
// (c) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//

using blacktriangles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Security;

namespace blacktriangles.Network
{
    public class RestClient
    {
        //
        // ////////////////////////////////////////////////////////////////////////
        //

        public enum PostBody
        {
            Json,
            UrlEncoded
        }
        
        //
        // members /////////////////////////////////////////////////////////////
        //

        public string host                                  { get; set; }
        public int port                                     { get; set; }

        //
        // constructor / destructor ////////////////////////////////////////////
        //

        public RestClient( string _host, int _port )
        {
            host = _host;
            port = _port;
        }

        //
        // public methods //////////////////////////////////////////////////////
        //

        public bool Get( string request, out JsonObject json )
        {
            return GetInternal( FormEndpoint( request ), out json );
        }

        //
        // --------------------------------------------------------------------
        //

        public bool Post( string request, JsonObject body, out JsonObject json, PostBody bodyType = PostBody.Json )
        {
            return PostInternal( FormEndpoint( request ), body, out json, bodyType );
        }

        //
        // --------------------------------------------------------------------
        //
        
        public bool Delete( string request, JsonObject body, out JsonObject json )
        {
            return DeleteInternal( FormEndpoint(request), body, out json );
        }

        //
        // private methods /////////////////////////////////////////////////////
        //

        private string FormEndpoint( string request )
        {
            return System.String.Format( "http://{0}:{1}/{2}", host, port, request );
        }

        //
        // --------------------------------------------------------------------
        //
        
        private string UrlEncode(JsonObject json)
        {
            var items = new List<string>();
            foreach(string key in json.keys)
            {
               items.Add(key + '=' + json.GetField<string>(key)); 
            }

            return string.Join("&", items);
        }

        //
        // --------------------------------------------------------------------
        //

        private bool GetInternal( string url, out JsonObject json )
        {
            json = null;
            bool result = false;
            string resultString = System.String.Empty;

            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";

                var response = (HttpWebResponse)request.GetResponse();
                using( var reader = new StreamReader(response.GetResponseStream()) )
                {
                    resultString = reader.ReadToEnd();
                }

                json = JsonObject.FromString(resultString);
                result = true;
            }
            catch( System.Exception ex)
            {
                Dbg.Warn(ex.ToString());
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        private bool PostInternal( string url, JsonObject body, out JsonObject json, PostBody bodyType )
        {
            return RequestInternal(url, body, out json, bodyType, "POST");
        }

        //
        // --------------------------------------------------------------------
        //
        
        private bool DeleteInternal(string url, JsonObject body, out JsonObject json)
        {
            return RequestInternal(url, body, out json, PostBody.Json, "DELETE");
        }

        //
        // --------------------------------------------------------------------
        //

        private bool RequestInternal( string url, JsonObject body, out JsonObject json, PostBody bodyType, string method )
        {
            json = null;
            bool result = false;
            string resultString = System.String.Empty;
            string bodyString = System.String.Empty;
            string contentType = System.String.Empty;
           
            switch(bodyType)
            {
                case PostBody.Json: 
                    bodyString = body.ToString();
                    contentType = "application/json";
                    break;

                case PostBody.UrlEncoded:
                    bodyString = UrlEncode(body);
                    contentType = "application/x-www-form-urlencoded";
                    break;
            }

            try
            {
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.ContentType = contentType;
                request.Method = method;

                using( var writer = new StreamWriter(request.GetRequestStream()) )
                {
                    writer.Write( bodyString );
                    writer.Flush();
                    writer.Close();
                }

                var response = (HttpWebResponse)request.GetResponse();
                using( var reader = new StreamReader(response.GetResponseStream()) )
                {
                    resultString = reader.ReadToEnd();
                }

                json = JsonObject.FromString(resultString);
                result = true;
            }
            catch( System.Exception ex )
            {
                Dbg.Warn(ex.ToString());
            }

            return result;

        }
        

        //
        // End Class //////////////////////////////////////////////////////////
        //
    }
}
