//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System.Collections.Generic;
using System.Reflection;

namespace blacktriangles.Network
{
    public class PacketRouter
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        private bool isInitialized                              = false;
        private Dictionary<int, System.Delegate> packetHandlers = new Dictionary<int, System.Delegate>();

        //
        // constructor / finalizer ////////////////////////////////////////////
        //

        public PacketRouter()
        {
            Initialize();
        }

        //
        // --------------------------------------------------------------------
        //

        public PacketRouter(Assembly[] asms)
        {
            Initialize(asms);
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Route(IEnumerable<RawPacket> rawPackets)
        {
            foreach(RawPacket packet in rawPackets)
            {
                Route(packet);
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public bool Route(RawPacket rawPacket)
        {
            bool result = false;

            Packet packet = PacketFactory.Create(rawPacket);
            Dbg.Assert(packet != null, $"Failed to create packet for type {rawPacket.header.id}");
            result = Route(rawPacket.receiver, packet);
            PacketFactory.Destroy(packet);

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public bool Route(IConnection connection, Packet packet)
        {
            bool result = false;
            System.Delegate handler = null;
            if(packetHandlers.TryGetValue(packet.id, out handler))
            {
                if(handler != null)
                {
                    result = (bool)handler.DynamicInvoke(connection, packet);
                }
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public void AddRoute<PacketType>(System.Func<IConnection, PacketType, bool> handler)
        {
            PacketAttribute attrib = AssemblyUtility.GetAttribute<PacketAttribute>(typeof(PacketType));
            Dbg.Assert(attrib != null, "Attempting to add route for a packet type that is not a packet: " + typeof(PacketType).ToString());

            int id = attrib.id;
            System.Delegate result = null;
            if(packetHandlers.TryGetValue(id, out result))
            {
                System.Func<IConnection, PacketType, bool> castHandlers = (System.Func<IConnection, PacketType, bool>)result;
                castHandlers -= handler;
                castHandlers += handler;
                result = (System.Delegate)castHandlers;
            }
            else
            {
                result = (System.Delegate)handler;
            }

            packetHandlers[id] = result;
        }

        //
        // --------------------------------------------------------------------
        //

        public void RemoveRoute<PacketType>(System.Func<IConnection, PacketType, bool> handler)
        {
            PacketAttribute attrib = AssemblyUtility.GetAttribute<PacketAttribute>(typeof(PacketType));
            Dbg.Assert(attrib != null, "Attempting to add route for a packet type that is not a packet: " + typeof(PacketType).ToString());

            int id = attrib.id;
            System.Delegate result = null;
            if(packetHandlers.TryGetValue(id, out result))
            {
                System.Func<IConnection, PacketType, bool> castHandlers = (System.Func<IConnection, PacketType, bool>)result;
                castHandlers -= handler;
                result = (System.Delegate)castHandlers;
            }
            else
            {
                result = (System.Delegate)handler;
            }

            packetHandlers[id] = result;
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void Initialize()
        {
            if(isInitialized) return;
            Initialize(AssemblyUtility.GetAllAssemblies());
        }

        //
        // --------------------------------------------------------------------
        //

        private void Initialize(Assembly[] asms)
        {
            if(isInitialized) return; 
            isInitialized = true;

            packetHandlers.Clear();
        }

        //
        // end class //////////////////////////////////////////////////////////
        //
    }
}
