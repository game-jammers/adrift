//=============================================================================
//
// (C) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using ProtoBuf;

namespace blacktriangles.Network
{
    [ProtoContract]
    public class pbVector3
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        [ProtoMember(1)] public float x;
        [ProtoMember(2)] public float y;
        [ProtoMember(3)] public float z;

        //
        // operators //////////////////////////////////////////////////////////
        //

        public static implicit operator UnityEngine.Vector3(pbVector3 vec)
        {
            UnityEngine.Vector3 res = new UnityEngine.Vector3();
            res.x = vec.x;
            res.y = vec.y;
            res.z = vec.z;
            return res;
        }

        //
        // --------------------------------------------------------------------
        //

        public static implicit operator pbVector3(UnityEngine.Vector3 vec)
        {
            pbVector3 res = new pbVector3();
            res.x = vec.x;
            res.y = vec.y;
            res.z = vec.z;
            return res;
        }

        //
        // --------------------------------------------------------------------
        //

        public static implicit operator UnityEngine.Quaternion(pbVector3 vec)
        {
            return UnityEngine.Quaternion.Euler(vec);
        }

        //
        // --------------------------------------------------------------------
        //

        public static implicit operator pbVector3(UnityEngine.Quaternion quat)
        {
            return quat.eulerAngles;
        }

        //
        // end class //////////////////////////////////////////////////////////
        //
    }
}
