
//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System;
using System.Collections.Generic;

namespace blacktriangles.Network
{
    public class NetworkError
    {
        //
        // members ############################################################
        //

        public IConnection connection                           { get; private set; }
        public string message                                   { get; private set; }

        //
        // constructor / initializer ##########################################
        //

        public NetworkError( IConnection _connection, string msg )
        {
            connection = _connection;
            message = msg;
        }
    }
}
