// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using blacktriangles.Network;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace blacktriangles.Network
{
    public class TcpListener
        : IDisposable
    {
        //
        // constants //////////////////////////////////////////////////////////
        //

        const int kListenSleepTimeMs                            = 500;

        //
        // events /////////////////////////////////////////////////////////////
        //

        public event ConnectionCallbacks.Connect OnConnect;
        public event ConnectionCallbacks.Disconnect OnDisconnect;
        public event ConnectionCallbacks.Error OnNetworkError;

        //
        // members ////////////////////////////////////////////////////////////
        //

        public ConnectionConfig? config                         { get; private set; }
        public bool isActive                                    { get; private set; }
        public TcpConnection[] connections                      { get { lock(_connections) { return _connections.ToArray(); } } }
        public System.Net.Sockets.TcpListener listener          { get; private set; }

        private List<TcpConnection> _connections                = new List<TcpConnection>();
        private List<RawPacket> simulatedIncoming               = new List<RawPacket>();

        //
        // constructor / finalizer ////////////////////////////////////////////
        //

        ~TcpListener()
        {
            Dispose();
        }

        //
        // --------------------------------------------------------------------
        //

        public virtual void Dispose()
        {
            Shutdown();
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Listen(ConnectionConfig _config)
        {
            config = _config;
            if(isActive)
            {
                Dbg.Warn("Trying to start listening on a TcpListener that is already listening.  Ignoring.");
                return;
            }

            isActive = true;
            listener = new System.Net.Sockets.TcpListener(config.Value.localEndpoint);
            listener.Start();
        }

        //
        // --------------------------------------------------------------------
        //

        public void Update()
        {
            if(!isActive) return;
            CreateNewConnections();
        }

        //
        // --------------------------------------------------------------------
        //

        public void Broadcast(Packet packet, System.Predicate<TcpConnection> pred = null)
        {
            TcpConnection[] conns = connections;
            foreach(TcpConnection conn in conns)
            {
                if(pred == null || pred(conn))
                {
                    conn.Send(packet);
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void SimulateLocally(Packet packet)
        {
            lock(simulatedIncoming)
            {
                simulatedIncoming.Add(packet.Encode());
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public List<RawPacket> TakePackets()
        {
            List<RawPacket> result = new List<RawPacket>();
            TcpConnection[] conns = connections;
            foreach(TcpConnection conn in conns)
            {
                int pre = result.Count;
                result.AddRange(conn.TakePackets());
            }

            lock(simulatedIncoming)
            {
                result.AddRange(simulatedIncoming);
                simulatedIncoming.Clear();
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public void Disconnect(TcpConnection conn)
        {
            lock(_connections)
            {
                if(_connections.Contains(conn))
                {
                    _connections.Remove(conn);
                }
            }

            NotifyDisconnected(conn);
            conn.OnDisconnect -= OnConnDisconnect;
            conn.OnNetworkError -= OnConnError;
            conn.Disconnect();
        }

        //
        // --------------------------------------------------------------------
        //

        public void DisconnectAll()
        {
            TcpConnection[] conns = connections;
            foreach(TcpConnection conn in conns)
            {
                Disconnect(conn);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void Shutdown()
        {
            isActive = false;
            if(listener != null)
            {
                listener.Stop();
                listener = null;
            }

            DisconnectAll();
            NotifyDisconnected(null);
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void CreateNewConnections()
        {
            try
            {
                if(listener.Pending())
                {
                    System.Net.Sockets.TcpClient client = listener.AcceptTcpClient();
                    if(client != null)
                    {
                        // rebuild config based
                        ConnectionConfig config = ConnectionConfig.From(client);
                        TcpConnection conn = new TcpConnection(client, config);
                        conn.OnDisconnect += OnConnDisconnect;
                        conn.OnNetworkError += OnConnError;
                        lock(_connections)
                        {
                            _connections.Add(conn);
                        }
                        NotifyConnected(conn);
                    }
                }
            }
            catch(System.Exception ex)
            {
                NotifyError(null, ex.ToString());
            }
        }

        //
        // notify callbacks ///////////////////////////////////////////////////
        //

        private void NotifyConnected(TcpConnection conn)
        {
            ConnectionCallbacks.Connect cb = OnConnect;
            if(cb != null)
            {
                cb(conn);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private void NotifyDisconnected(TcpConnection conn)
        {
            ConnectionCallbacks.Disconnect cb = OnDisconnect;
            if(cb != null)
            {
                cb(conn);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private void NotifyError(TcpConnection conn, string msg)
        {
            ConnectionCallbacks.Error cb = OnNetworkError;
            if(cb != null)
            {
                NetworkError err = new NetworkError(conn, msg);
                cb(err);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private void OnConnDisconnect(IConnection iconn)
        {
            TcpConnection conn = iconn as TcpConnection;
            if(conn != null)
            {
                Disconnect(conn);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private void OnConnError(NetworkError err)
        {
            TcpConnection conn = err.connection as TcpConnection;
            if(conn != null)
            {
                NotifyError(conn, err.message);
                Disconnect(conn);
            }
        }

        //
        // End Class //////////////////////////////////////////////////////////
        //
    }
}
 
