//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;

namespace blacktriangles.Network
{
    //
    // Connection Callbacks ###################################################
    //

    public class ConnectionCallbacks
    {
        public delegate void Connect(IConnection conn);
        public delegate void Disconnect(IConnection conn);
        public delegate void Error(NetworkError err);
        public delegate void Message(IConnection conn, Packet packet);
    }
}
