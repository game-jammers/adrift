//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System.Net;
using System.Net.Sockets;

namespace blacktriangles.Network
{
    public struct ConnectionConfig
    {
        //
        // constants //////////////////////////////////////////////////////////
        //

        public const int kThreadJoinTimeout                     = 3000;

        //
        // members ////////////////////////////////////////////////////////////
        //

        public IPEndPoint localEndpoint;
        public IPEndPoint remoteEndpoint;

        public string nameOnCertificate;
        public bool useTls;

        //
        // methods ////////////////////////////////////////////////////////////
        //
        public static ConnectionConfig From(System.Net.Sockets.TcpClient tcp)
        {
            ConnectionConfig result = new ConnectionConfig();
            result.localEndpoint = (IPEndPoint)tcp.Client.LocalEndPoint;
            result.remoteEndpoint = (IPEndPoint)tcp.Client.RemoteEndPoint;
            result.useTls = false;
            return result;
        }
    }
}
