//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;
using System.Collections.Generic;

using blacktriangles;

namespace blacktriangles.Network
{
    [Packet((int)ReservedPacketType.UTF8TextPacket)]
    public class UTF8TextPacket
        : Packet
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public string text                                        { get; protected set; }

        //
        // constructor / initializer //////////////////////////////////////////
        //

        public UTF8TextPacket()
            : base()
        {
            text = System.String.Empty;
        }

        //
        // --------------------------------------------------------------------
        //

        public UTF8TextPacket( string _text )
            : base()
        {
            text = _text;
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override void Decode( RawPacket packet )
        {
            base.Decode(packet);
            text = System.Text.Encoding.UTF8.GetString( packet.payload );
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected override byte[] EncodePayload()
        {
            return System.Text.Encoding.UTF8.GetBytes( text );
        }
    }
}
