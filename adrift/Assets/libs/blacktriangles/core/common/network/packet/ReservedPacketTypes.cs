//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using blacktriangles.Network;

namespace blacktriangles.Network
{
    public enum ReservedPacketType
    {
        UTF8TextPacket      = -1,
        JsonPacket          = -2,
        DiscoveryRequest    = -3,
        DiscoveryResponse   = -4,
    }
}
