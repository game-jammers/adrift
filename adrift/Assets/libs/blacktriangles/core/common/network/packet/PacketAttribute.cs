//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//

using blacktriangles;

namespace blacktriangles.Network
{
    [System.AttributeUsage( System.AttributeTargets.Class )]
    public class PacketAttribute
        : System.Attribute
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public int id                                            { get; private set; }

        //
        // constructor / destructor ///////////////////////////////////////////
        //

        public PacketAttribute( int _id )
        {
            id = _id;
        }
    }
}
