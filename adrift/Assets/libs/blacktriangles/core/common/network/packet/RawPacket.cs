//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;
using System.IO;
using System.Net;
using System.Collections.Generic;

namespace blacktriangles.Network
{
    public class RawPacket
    {
        //
        // members ============================================================
        //

        public IConnection receiver                             = null;
        public IPEndPoint senderEndpoint                        = null;
        public Packet.Header header                             { get; private set; }
        public byte[] payload                                   { get; private set; }

        //
        // constructor / initializer ==========================================
        //

        public RawPacket(int packetId, byte[] _payload)
        {
            header = new Packet.Header( packetId, _payload.Length );
            payload = (byte[])_payload.Clone();
        }

        //
        // --------------------------------------------------------------------
        //

        public RawPacket(BinaryReader reader)
        {
            header = default(Packet.Header);
            payload = null;
            ReadFromStream( reader );
        }

        //
        // public methods =====================================================
        //

        public void WriteToStream(BinaryWriter writer)
        {
            // validate that the header payload size matches the actual payload size;
            if(header.payloadSize != payload.Length)
            {
                Dbg.Warn( "Packet header payload size does not match actual paylaod size!" );
            }

            byte[] headerBuffer = header.ToBytes();
            writer.Write(headerBuffer);
            writer.Write(payload, 0, payload.Length);
        }

        //
        // --------------------------------------------------------------------
        //

        public byte[] ToBytes()
        {
            byte[] result = new byte[Packet.kHeaderSize + payload.Length];
            BinaryWriter writer = new BinaryWriter(new MemoryStream(result));
            writer.Write(header.ToBytes());
            writer.Write(payload);

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public void ReadFromStream(BinaryReader reader)
        {
            header = new Packet.Header(reader);
            payload = reader.ReadBytes(header.payloadSize);
        }

        //
        // --------------------------------------------------------------------
        //
    }
}
