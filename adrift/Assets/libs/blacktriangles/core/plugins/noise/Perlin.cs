//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    [System.Serializable]
    public struct PerlinNoise
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public float frequency;
        public float amplitude;
        public float shift;
        public FloatRange range;
        public Vector2 offset;

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public float Sample(Vector2 pos)
        {
            Vector2 finalPos = (pos + offset) * frequency;
            float f = Mathf.PerlinNoise(finalPos.x, finalPos.y) + shift;
            f = range.Normalized(f);
            return btMath.Clamp(f*amplitude, 0f, 1f);
        }
    }
}
