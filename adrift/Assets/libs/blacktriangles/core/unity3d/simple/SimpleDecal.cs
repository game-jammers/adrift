//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections;
using UnityEngine;

namespace blacktriangles
{
    public class SimpleDecal
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public MeshRenderer mesh;

        public Texture[] textures;
        public Color startColor;
        public Color endColor;

        public double fadeTime;

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void OnEnable()
        {
            StartCoroutine(Fade());
        }
        
        //
        // private methods ////////////////////////////////////////////////////
        //

        private IEnumerator Fade()
        {
            Material mat = mesh.material;
            mat.SetColor("_BaseColor", startColor);
            mat.SetTexture("_BaseColorMap", textures.Random());

            double start = Epoch.now;
            double elapsed = 0;
            var wait = new WaitForSeconds(0f);
            while(elapsed < fadeTime)
            {
                elapsed = Epoch.now - start;
                float perc = (float)elapsed / (float)fadeTime;
                Color color = Color.Lerp(startColor, endColor, perc);
                mat.SetColor("_BaseColor", color);
                yield return wait;
            }
        }

        //
        // debug //////////////////////////////////////////////////////////////
        //
        
        #if UNITY_EDITOR
        [ContextMenu("Print Properties")]
        private void GetProperties()
        {
            Shader shader = mesh.sharedMaterial.shader;
            for(int idx = 0; idx < shader.GetPropertyCount(); ++idx)
            {
                Dbg.Log(shader.GetPropertyName(idx));
                string[] strs = shader.GetPropertyAttributes(idx);
                foreach(string str in strs)
                {
                    Dbg.Log(str);
                }
            }
        }
        #endif
        
    }
}
