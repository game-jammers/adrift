//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    [ExecuteInEditMode]
    public class Billboard
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public btCamera cam;
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void LateUpdate()
        {
            if(cam == null)
            {
                cam = BaseSceneManager.instance.sceneCam;
            }

            if(cam != null)
            {
                transform.forward = cam.unityCamera.transform.forward;
            }
        }
    }
}
