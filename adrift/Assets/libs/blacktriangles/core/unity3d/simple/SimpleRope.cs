//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    [RequireComponent(typeof(LineRenderer))]
    public class SimpleRope
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public LineRenderer line                                { get; private set; }

        public Transform endpoint;
        public int segmentCount;

        //
        // public methods /////////////////////////////////////////////////////
        //

        [ContextMenu("Build Rope")]
        public void BuildRope()
        {
        }
    }
}
