//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public class SimpleFpsController
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("References")]
        public Transform cameraMount                            = null;
        public CharacterController unityController              = null;

        [Header("Settings")]
        public float moveSpeed                                  = 10f;
        public float lookSpeed                                  = 180f;
        public float fallSpeed                                  = 10f;

        public bool isMoving                                    { get; private set; }
        public bool isGrounded                                  { get; private set; }
        public Vector2 lookDir                                 = Vector2.zero;

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Awake()
        {
            
        }

        //
        // --------------------------------------------------------------------
        //
        
        protected virtual void FixedUpdate()
        {
            SceneSettings settings = BaseSceneManager.instance.settings;
            Vector3 deltaMove = cameraMount.TransformDirection(new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"))) * moveSpeed;
            isMoving = deltaMove.sqrMagnitude > 1f;
            deltaMove.y = -fallSpeed;

            unityController.Move(deltaMove * Time.fixedDeltaTime);
            isGrounded = unityController.isGrounded;

            float mousex = Mathf.Clamp(Input.GetAxis("Mouse X"), -1f, 1f) * (settings.mouse.invertX ? -1f : 1f) * settings.mouse.speed.x;
            float mousey = Mathf.Clamp(Input.GetAxis("Mouse Y"), -1f, 1f) * (settings.mouse.invertY ? -1f : 1f) * settings.mouse.speed.y;

            lookDir += new Vector2(mousex, mousey) * lookSpeed * Time.fixedDeltaTime;

            lookDir.x = lookDir.x % 360f;
            lookDir.y = btMath.Clamp(lookDir.y, -90f, 90f);

            cameraMount.transform.localRotation = Quaternion.Euler(lookDir.y, 0f, 0f);
            transform.localRotation = Quaternion.Euler(0f, lookDir.x, 0f);
        }
    }
}
