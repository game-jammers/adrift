//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{

    //
    // path node used by the generator ////////////////////////////////////////
    //
    
    public interface IPathNode
    {
        Vector3 worldpos                                        { get; set; }
    }

}
