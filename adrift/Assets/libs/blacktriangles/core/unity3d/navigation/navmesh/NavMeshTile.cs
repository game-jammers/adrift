//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace blacktriangles
{
    [DefaultExecutionOrder(-100)]
    public class NavMeshTile
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Built Data")]
        [ReadOnly] public NavMeshData data                      = null;
        private NavMeshDataInstance dataInstance;

        [Header("Build Settings")]
        public int agentTypeId                                  = 0;
        public LayerMask mask                                   = ~0;
        public NavMeshCollectGeometry collectionType            = NavMeshCollectGeometry.RenderMeshes;
        public Bounds bounds;

        private UnityEngine.Coroutine updateNavMesh             = null;

        //
        // cache
        //

        private List<NavMeshBuildSource> sources                = null;
        private Renderer[] renderers                            = null;

        //
        // public methods /////////////////////////////////////////////////////
        //

        [ContextMenu("Recalculate Bounds")]
        public virtual void RecalculateBounds()
        {
            RecalculateBounds(true);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void RecalculateBounds(bool force)
        {
            if(renderers == null || force)
            {
                renderers = gameObject.GetComponentsInChildren<Renderer>();
            }

            bounds.center = transform.position;
            bounds.extents = Vector3.zero;
            foreach(Renderer renderer in renderers)
            {
                bounds.Encapsulate(renderer.bounds);
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void UpdateNavMesh(bool skipCache)
        {
            if(updateNavMesh != null)
            {
                StopCoroutine(updateNavMesh);
            }

            updateNavMesh = StartCoroutine(UpdateNavMeshAsync(skipCache));
        }

        //
        // --------------------------------------------------------------------
        //
        
        public IEnumerator UpdateNavMeshAsync(bool skipCache)
        {
            NavMeshBuildSettings settings = NavMesh.GetSettingsByID(agentTypeId);
            List<NavMeshBuildSource> buildSources = CollectSources(skipCache);
            AsyncOperation op = NavMeshBuilder.UpdateNavMeshDataAsync(
                data,
                settings,
                buildSources,
                bounds
            );

            yield return op;

            AddToNavMesh();

            updateNavMesh = null;
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        [ContextMenu("Add To NavMesh")]
        protected void AddToNavMesh()
        {
            RemoveFromNavMesh(); 

            if(data != null)
            {
                dataInstance = NavMesh.AddNavMeshData(data);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        [ContextMenu("Remove From NavMesh")]
        protected void RemoveFromNavMesh()
        {
            if(dataInstance.valid)
            {
                NavMesh.RemoveNavMeshData(dataInstance);
            }
        }       

        //
        // --------------------------------------------------------------------
        //
        
        protected List<NavMeshBuildSource> CollectSources(bool skipCache = true)
        {
            if(sources == null || skipCache)
            {
                sources = new List<NavMeshBuildSource>();
                NavMeshBuilder.CollectSources(
                    transform,                                  // root
                    mask,                                       // layer mask (ALL)
                    collectionType,                             // geometry
                    0,                                          // defaultArea
                    new List<NavMeshBuildMarkup>(),             // markups
                    sources                                     // results
                );
            }

            return sources;
        }

        //
        // --------------------------------------------------------------------
        //

        protected NavMeshData BuildNavMeshData(List<NavMeshBuildSource> sources)
        {
            NavMeshBuildSettings settings = NavMesh.GetSettingsByID(agentTypeId);
            NavMeshData result = NavMeshBuilder.BuildNavMeshData(
                settings,                                       // build settings
                sources,                                        // sources
                bounds,                                         // bounds
                transform.position,                             // position
                transform.rotation                              // rotation
            );

            Dbg.Assert(result != null, $"Result for ${name} is null");

            result.name = $"{name}.NavMeshData";
            return result;
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            //
            // initialize using empty data
            //
            data = BuildNavMeshData(new List<NavMeshBuildSource>());
            UpdateNavMesh(true);
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void OnDestroy()
        {
            RemoveFromNavMesh();
        }
    }
}
