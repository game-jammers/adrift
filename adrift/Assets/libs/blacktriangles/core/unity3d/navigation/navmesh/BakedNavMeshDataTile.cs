//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

#if UNITY_EDITOR
using UnityEditor;
#endif

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace blacktriangles
{
    [ExecuteInEditMode]
    [DefaultExecutionOrder(-102)]
    public class BakedNavMeshDataTile
        : NavMeshTile
    {
        //
        // public methods /////////////////////////////////////////////////////
        //

        public override void RecalculateBounds()
        {
            base.RecalculateBounds();
            #if UNITY_EDITOR
                gameObject.UpdatePrefab();
            #endif
        }

        //
        // private methods ////////////////////////////////////////////////////
        //
        
        private void DestroyNavMeshData(string path)
        {
            #if UNITY_EDITOR
            Object[] assets = AssetDatabase.LoadAllAssetsAtPath(path);
            foreach(Object asset in assets)
            {
                NavMeshData assetData = asset as NavMeshData;
                if(assetData != null)
                {
                    DestroyImmediate(asset, true);
                }
            }
            #endif
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected override void Start()
        {
            // do not call base.Start, which will rebuild the mesh since we
            // have baked it.
            AddToNavMesh();
        }

        //
        // editor /////////////////////////////////////////////////////////////
        //

        #if UNITY_EDITOR
        [ContextMenu("Clear")]
        private void Clear()
        {
            Object prefab = PrefabUtility.GetCorrespondingObjectFromSource(gameObject);
            string path = AssetDatabase.GetAssetPath(prefab);

            if(System.String.IsNullOrEmpty(path))
            {
                Dbg.Error($"GameObject {gameObject} for {prefab} has no valid prefab path");
                return;
            }

            DestroyNavMeshData(path);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        //
        // --------------------------------------------------------------------
        //
        
        [ContextMenu("Bake")]
        private void Bake()
        {
            Object prefab = PrefabUtility.GetCorrespondingObjectFromSource(gameObject);

            if(prefab == null)
            {
                Dbg.Error($"GameObject {gameObject} does not have a prefab");
                return;
            }

            string path = AssetDatabase.GetAssetPath(prefab);
            if(System.String.IsNullOrEmpty(path))
            {
                Dbg.Error($"GameObject {gameObject} for {prefab} has no valid prefab path");
                return;
            }

            DestroyNavMeshData(path);
            List<NavMeshBuildSource> sources = CollectSources();
            NavMeshData builtData = BuildNavMeshData(sources);
            AssetDatabase.AddObjectToAsset(builtData, prefab);
            AssetDatabase.SaveAssets();
            data = builtData;
            PrefabUtility.ApplyPrefabInstance(gameObject, InteractionMode.AutomatedAction);
            AssetDatabase.Refresh();
        }
        #endif
    }
}
