//
// (C) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles
{
    public class Database
    {
        //
        // members /////////////////////////////////////////////////////////////
        //

        public static Database instance                         { get { return GetInstance(); } }
        private static Database _instance                       = null;

        [ShowInInspector, ReadOnly]
        public Dictionary<System.Type, DatabaseTable> tables         = new Dictionary<System.Type, DatabaseTable>();

        //
        // constructor ////////////////////////////////////////////////////////
        //
        
        static Database()
        {
            GetInstance();
        }

        //
        // public methods //////////////////////////////////////////////////////
        //

        public T GetTable<T>()
            where T: DatabaseTable
        {
            return tables[typeof(T)] as T;
        }

        //
        // --------------------------------------------------------------------
        //

        public void Initialize()
        {
            tables.Clear();

            System.Type[] types = AssemblyUtility.CollectAllTypes((type)=>{
                return type.IsSubclassOf(typeof(DatabaseTable)) && !type.IsAbstract;
            });
            
            foreach(System.Type type in types)
            {
                DatabaseTable table = System.Activator.CreateInstance(type) as DatabaseTable;
                table.Initialize(this);
                tables[type] = table;
            }
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        static private Database GetInstance()
        {
            if(_instance == null)
            { 
                _instance = new Database();
                _instance.Initialize();
            }

            return _instance;
        }
    }
}
