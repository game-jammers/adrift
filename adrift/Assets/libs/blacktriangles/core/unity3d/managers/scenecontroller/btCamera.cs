//
// (c) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//

using blacktriangles;
using Cinemachine;
using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles
{
    public class btCamera
        : MonoBehaviour
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public enum UpdateType
        {
            Update,
            FixedUpdate,
            LateUpdate,
            Manual
        }

        //
        // --------------------------------------------------------------------
        //
        
        public struct PickResult
        {
            public RaycastHit[] hits;
            public GameObject[] objects;
        }

        //
        // members ////////////////////////////////////////////////////////////
        //

        public BaseSceneManager sceneController                 { get { return BaseSceneManager.instance; } }
        public Camera unityCamera                               = null;
        public UpdateType updateType                            = UpdateType.Manual;
        public Skybox skybox                                    { get { return GetSkybox(); } }

        public LayerMask selectable;
        public bool forceDepth                                  = false;

        public Vector3 viewForward                              { get { return unityCamera.transform.forward; } }
        public Vector3 viewUp                                   { get { return unityCamera.transform.up; } }

        public DepthTextureMode depthTextureMode                = DepthTextureMode.None;

        private Skybox _skybox                                  = null;

        public CinemachineBrain cinebrain                       { get; private set; }
        public ICinemachineCamera activeVCam                    { get; private set; }
        public CinemachineTransposer activeTrans                { get; private set; }

        //
        // camera callbacks ///////////////////////////////////////////////////
        //

        public virtual void UpdateCamera(float dt)
        {
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Awake()
        {
            unityCamera.depthTextureMode = depthTextureMode;
            cinebrain = unityCamera.GetComponent<CinemachineBrain>();
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void Start()
        {
            if(forceDepth)
            {
                unityCamera.depthTextureMode = unityCamera.depthTextureMode | DepthTextureMode.Depth;
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        protected virtual void Update()
        {
            if( updateType == UpdateType.Update )
            {
                UpdateCamera(Time.deltaTime);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void FixedUpdate()
        {
            if( updateType == UpdateType.FixedUpdate )
            {
                UpdateCamera(Time.fixedDeltaTime);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void LateUpdate()
        {
            if( updateType == UpdateType.LateUpdate )
            {
                UpdateCamera(Time.deltaTime);
            }
        }

        //
        // utilities //////////////////////////////////////////////////////////
        //

        public Vector3 TransformDirection( Vector3 dir )
        {
            return unityCamera.transform.TransformDirection(dir);
        }

        //
        // --------------------------------------------------------------------
        //

        public Vector2 TransformDirection( Vector2 dir)
        {
            return TransformDirection( dir.ToVector3XZ() ).ToVector2XZ();
        }
        
        //
        // --------------------------------------------------------------------
        //

        public Vector3 WorldToScreenPoint( Vector3 worldPos )
        {
            return unityCamera.WorldToScreenPoint( worldPos );
        }

        //
        // --------------------------------------------------------------------
        //

        public Vector3 ViewportToScreenPoint( Vector3 viewport )
        {
            return unityCamera.ViewportToScreenPoint(viewport);
        }

        //
        // --------------------------------------------------------------------
        //

        public Vector2 ScreenToCanvasPoint( Vector2 screenPos )
        {
            return new Vector2( screenPos.x - ( Screen.width / 2.0f ),
                                screenPos.y - ( Screen.height / 2.0f ) );
        }

        //
        // --------------------------------------------------------------------
        //

        public Vector2 WorldToCanvasPoint( Vector3 worldPos, out bool behind )
        {
            Vector3 screen = WorldToScreenPoint( worldPos );
            behind = screen.z < 0.0f;
            return ScreenToCanvasPoint( screen );
        }

        //
        // --------------------------------------------------------------------
        //

        public Ray ScreenToRay( Vector3 screenPos )
        {
            return unityCamera.ScreenPointToRay( screenPos );
        }

        //
        // --------------------------------------------------------------------
        //

        public Rect GetScreenBounds( Plane groundPlane )
        {
            Vector3 bottomLeft = ScreenToWorld( Vector3.zero, groundPlane );
            Vector3 topRight = ScreenToWorld( new Vector3( Screen.width, Screen.height, 0.0f ), groundPlane );
            Rect result = Rect.MinMaxRect( bottomLeft.x, bottomLeft.y, topRight.x, topRight.y );
            return result;
        }

        //
        // --------------------------------------------------------------------
        // Uses the camera view plane to project upon

        public Vector3 ScreenToWorldPoint(Vector3 screenPos, float dist)
        {
            return unityCamera.ScreenToWorldPoint(screenPos + Vector3.forward * dist);
        }

        //
        // --------------------------------------------------------------------
        //

        public Vector3 ScreenToWorld( Vector2 screenPos, Plane groundPlane )
        {
            return ScreenToWorld( screenPos.CloneWithZ(0.0f), groundPlane );
        }

        //
        // --------------------------------------------------------------------
        //

        public Vector3 ScreenToWorld( Vector3 screenPos, Plane groundPlane )
        {
            Vector3 result = Vector3.zero;

            if( unityCamera != null )
            {
                Ray ray = ScreenToRay( screenPos );

                float distance = 0f;
                groundPlane.Raycast( ray, out distance );

                result = ray.GetPoint( distance );
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public Vector3 ScreenToWorld( Vector3 screenPos )
        {
            Plane groundPlane = new Plane( Vector3.up, Vector3.zero );
            return ScreenToWorld( screenPos, groundPlane );
        }

        //
        // --------------------------------------------------------------------
        //
        
        public bool MousePick( out RaycastHit result, LayerMask mask, float distance = Mathf.Infinity )
        {
            return MousePick( 
                new Vector3( unityCamera.pixelWidth/2, unityCamera.pixelHeight/2, 0f ), 
                mask,
                distance,
                out result 
            );
        }

        //
        // --------------------------------------------------------------------
        //
        
        public bool MousePick( Vector3 screenPos, out RaycastHit result, float distance = Mathf.Infinity )
        {
            return MousePick( screenPos, selectable, distance, out result );
        }

        //
        // --------------------------------------------------------------------
        //

        public bool MousePick( Vector3 screenPos, LayerMask mask, float distance, out RaycastHit result )
        {
            result = new RaycastHit();
            bool success = false;
            if( unityCamera != null )
            {
                Ray ray = unityCamera.ScreenPointToRay( screenPos );
                success = Physics.Raycast( ray.origin, ray.direction, out result, distance, mask );
            }

            return success;
        }

        //
        // --------------------------------------------------------------------
        //
        

        public ComponentType MousePick<ComponentType>( float distance = Mathf.Infinity )
            where ComponentType : MonoBehaviour
        {
            return MousePick<ComponentType>(
                Input.mousePosition,
                distance
            );
        }

        //
        // --------------------------------------------------------------------
        //

        public ComponentType MousePick<ComponentType>( Vector3 screenPos, float distance = Mathf.Infinity )
            where ComponentType: MonoBehaviour
        {
            ComponentType result = null;
            RaycastHit hit;
            if( MousePick( screenPos, out hit, distance ) )
            {
                result = hit.collider.gameObject.GetComponent<ComponentType>();
            }
            return result;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public PickResult MousePickAll( Vector3 screenPos, float distance = Mathf.Infinity )
        {
            return MousePickAll(screenPos, selectable, distance);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public PickResult MousePickAll( Vector3 screenPos, LayerMask mask, float distance = Mathf.Infinity )
        {
            PickResult result;

            Ray ray = unityCamera.ScreenPointToRay(screenPos);
            result.hits = Physics.RaycastAll(
                ray,
                distance,
                mask
            );

            result.objects = new GameObject[result.hits.Length];
            for(int i = 0; i < result.hits.Length; ++i)
            {
                result.objects[i] = result.hits[i].collider.gameObject;
            }

            return result;
        }

        //
        // callbacks //////////////////////////////////////////////////////////
        //

        public void OnCameraActivated(ICinemachineCamera next, ICinemachineCamera prev)
        {
            activeVCam = next;
            activeTrans = (activeVCam as CinemachineVirtualCamera).GetCinemachineComponent<CinemachineTransposer>();
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private Skybox GetSkybox()
        {
            if( _skybox == null )
            {
                _skybox = unityCamera.GetComponent<Skybox>();
            }

            return _skybox;
        }
    }
}
