//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using UnityEngine;
using UnityEngine.UI;

namespace blacktriangles
{
    public abstract class BaseSceneManager
        : MonoBehaviour
    {
        //
        // members /////////////////////////////////////////////////////////////
        //

        public static BaseSceneManager instance                 { get; private set; }

        [Header("Base Scene Manager")]
        public btCamera sceneCam       = null;
        public Canvas screenCanvas     = null;
        public Canvas worldCanvas      = null;
        public UnityEngine.EventSystems.EventSystem eventSystem = null;

        [Header("Settings")]
        public SceneSettings settings                           = SceneSettings.Default;

        public RectTransform screenRectTransform                { get; private set; }
        public RectTransform worldRectTransform                 { get; private set; }

        private ICameraController cameraController              = null;

        //
        // public methods //////////////////////////////////////////////////////
        //

        public static T GetInstance<T>()
            where T: BaseSceneManager
        {
            return instance as T;
        }

        //
        // --------------------------------------------------------------------
        //

        public void RequestCamera( ICameraController newController )
        {
            if( cameraController != null )
                cameraController.OnReleaseCamera();

            newController.OnTakeCamera( sceneCam );
            cameraController = newController;
        }

        //
        // --------------------------------------------------------------------
        //

        public virtual void ReleaseCamera()
        {
            ReleaseCamera(cameraController);
        }

        //
        // --------------------------------------------------------------------
        //
        

        public virtual void ReleaseCamera( ICameraController oldController )
        {
            if(cameraController == null) return;

            if(cameraController == oldController)
            {
                cameraController.OnReleaseCamera();
                cameraController = null;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void ApplySettings()
        {
            ApplySettings(settings);
        }

        //
        // --------------------------------------------------------------------
        //

        public void ApplySettings(SceneSettings settings)
        {
            this.settings = settings;
            Cursor.lockState = settings.mouse.cursorMode;
            Cursor.visible = settings.mouse.visible;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void CaptureCursor()
        {
            settings.mouse.cursorMode = CursorLockMode.Locked;
            settings.mouse.visible = false;
            ApplySettings();
        }

        //
        // --------------------------------------------------------------------
        //

        public void ReleaseCursor()
        {
            settings.mouse.cursorMode = CursorLockMode.None;
            settings.mouse.visible = true;
            ApplySettings();
        }

        //
        // unity callbacks /////////////////////////////////////////////////////
        //

        protected virtual void Awake()
        {
            instance = this;
            screenRectTransform = screenCanvas == null ? null : screenCanvas.GetComponent<RectTransform>();
            worldRectTransform = worldCanvas == null ? null : worldCanvas.GetComponent<RectTransform>();

            ApplySettings();
        }
    }
}
