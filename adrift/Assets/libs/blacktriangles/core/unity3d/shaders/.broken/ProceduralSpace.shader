//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

Shader "blacktriangles/URP/ProceduralSpace"
{
    //
    // properties /////////////////////////////////////////////////////////////
    //
    
    Properties
    {
        _Seed("Seed", Float) = 1.0

        _StarSize("Star Size", Float) = 100
        _StarDensity("Star Density", Range(0,1)) = 0.8
        _StarColor("Star Color", Color) = (1,1,1,1)

        _NebulaColor("Nebula Color", Color) = (0.3,0.0,0.0,1.0)
        _NebulaPlxColor("Nebula Parallax Color", Color) = (0.3,0.0,0.0,1.0)
        _NebulaStr("Nebula Strength", Range(0,1)) = 0.15
        _NebulaPlx("Nebula Parallax", Range(0,1)) = 0.15
    }

    //
    // ########################################################################
    //
    
    CGINCLUDE

    //
    // includes ///////////////////////////////////////////////////////////////
    //

    #include "Lighting.cginc"
    #include "UnityCG.cginc"

    #include "includes/Conversions.cginc"
    #include "includes/NoiseCommon.cginc"
    #include "includes/Perlin.cginc"
    #include "includes/Simplex.cginc"

    //
    // variables //////////////////////////////////////////////////////////////
    //
    
    uniform float _Seed, _StarSize, _StarDensity, _NebulaStr, _NebulaPlx;
    uniform float4 _StarColor, _NebulaColor, _NebulaPlxColor;

    //
    // types //////////////////////////////////////////////////////////////////
    //
    
    // app -> vert
    struct appdata
    {
        float4 vertex : POSITION;
        float3 tex0: TEXCOORD0;
    };

    //
    // ------------------------------------------------------------------------
    //
    
    // vert -> frag
    struct v2f
    {
        float4 pos: SV_POSITION;
        float3 tex0: TEXCOORD0;
        float3 tex1: TEXCOORD1;
    };

    //
    // ------------------------------------------------------------------------
    //

    float4 pointStarsLayer(float3 t0, float size)
    {
        float p = simplex(t0*_StarSize);
        float amt = (p-size)*100/(1.0-size);
        return clamp(_StarColor * amt,0,0.4);
    }
    
    float4 pointStars(float seed, float size, float3 texCoord)
    {
        float4 result = float4(0,0,0,1);

        result += pointStarsLayer(texCoord, size);
        
        return result;
    }

    //
    // ------------------------------------------------------------------------
    //

    float4 nebula(float seed, float3 texcoord, float4 color)
    {
        float4 base = color;
        float4 main = color * perlin(texcoord+seed);
        return (base + main);
    }
   
    //
    // vertex program /////////////////////////////////////////////////////////
    //

    v2f vert(appdata v)
    {
        v2f o;
        o.pos = UnityObjectToClipPos(v.vertex);
        o.tex0 = v.tex0;
        o.tex1 = v.vertex.xyz;
        return o;
    }

    //
    // fragment program ///////////////////////////////////////////////////////
    //
    
    float4 frag(v2f i) : COLOR
    {
        float4 stars = pointStars(_Seed, 1.0 - _StarDensity, i.tex0)*0.8;

        float4 neb = nebula(_Seed, i.tex0, _NebulaColor) * _NebulaStr;
        float4 neb2 = nebula(_Seed/5.0, i.tex0*0.5, _NebulaPlxColor) * _NebulaPlx;
        float4 col = neb + neb2 + stars;
        return col;
    }

    ENDCG

    //
    // ########################################################################
    //
    
    SubShader
    {
        Tags 
        { 
            "RenderType" = "Skybox"
            "Queue" = "Background"
            "PreviewType" = "Skybox"
        }

        Pass
        {
            ZWrite Off
            Cull Off
            Fog { Mode Off }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            ENDCG
        }
    }
}
