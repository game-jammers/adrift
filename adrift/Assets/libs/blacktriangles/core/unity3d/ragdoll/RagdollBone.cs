//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public class RagdollBone
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Rigidbody rb                                     = null;
        public Collider collide                                 = null;
        public CharacterJoint joint                             = null;

        public RagdollBone parentRb                             = null;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void GoRagdoll()
        {
            rb.isKinematic = false;
            rb.velocity = Vector3.zero;
            rb.Sleep();

            if(collide != null)
            {
                collide.enabled = true;
            }

            if(joint != null && parentRb != null)
            {
                joint.connectedBody = parentRb.rb;
            }
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void OnEnable()
        {
            rb.isKinematic = true;

            if(collide != null)
            {
                collide.enabled = false;
            }

            if(joint != null)
            {
                joint.connectedBody = null;
            }
        }

        //
        // editor methods /////////////////////////////////////////////////////
        //

        #if UNITY_EDITOR
        [ContextMenu("Generate Bone")]
        public void GenerateBone(float collideScale)
        {
            if(rb != null || collide != null || joint != null)
            {
                Dbg.Warn("Cannot generate bone on {0} because rb, colllider, or joint are already set", name);
                return;
            }

            rb = gameObject.AddComponent<Rigidbody>();
            rb.isKinematic = true;

            CapsuleCollider capsule = gameObject.AddComponent<CapsuleCollider>();
            capsule.radius = collideScale * 0.25f;
            capsule.height = collideScale;
            collide = capsule;
            
            joint = gameObject.AddComponent<CharacterJoint>();

            parentRb = UnityUtils.GetComponentInParentNotMe<RagdollBone>(this);
        }

        [ContextMenu("Clear")]
        public void Clear()
        {
            joint = UnityUtils.SafeDestroy(joint);
            rb = UnityUtils.SafeDestroy(rb);
            collide = UnityUtils.SafeDestroy(collide);
        }
        #endif
    }
}
