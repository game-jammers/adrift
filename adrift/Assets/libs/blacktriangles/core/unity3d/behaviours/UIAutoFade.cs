//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    public class UIAutoFade
        : MonoBehaviour
    {
        // members /////////////////////////////////////////////////////////////
        public UIElement element                                = null;
        public bool fadeIn                                      = true;
        public float time                                       = 1.0f;

        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void Start()
        {
            if( element == null ) return;

            element.Fade( !fadeIn, 0.0f, null );
            element.Fade( fadeIn, time, null );
        }
    }
}
