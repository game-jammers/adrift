//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    [RequireComponent(typeof(CharacterController))]
    public class Walker
        : MonoBehaviour
        , ICameraController
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public enum Action
        {
            MoveX,
            MoveY,
            LookX,
            LookY
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Transform cameraBoom                             = null;
        public float lookSpeed                                  = 10f;
        public float moveSpeed                                  = 10f;

        public btCamera cam                                     { get; private set; }
        public bool hasCamera                                   { get { return cam != null; } }

        private CharacterController control                     = null;
        private InputRouter<Action> input                       = null;

        private Vector3 lookDir;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void OnTakeCamera(btCamera camera)
        {
            cam = camera;
            cam.transform.SetParent(cameraBoom);
            cam.transform.localPosition = Vector3.zero;
            cam.transform.localRotation = Quaternion.identity;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void OnReleaseCamera()
        {
            cam.transform.SetParent(null);
            cam = null;
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            control = GetComponent<CharacterController>();
            input = new InputRouter<Action>();

            input.Bind(Action.MoveX, InputAction.FromAxis(KeyCode.A, KeyCode.D));
            input.Bind(Action.MoveY, InputAction.FromAxis(KeyCode.S, KeyCode.W));
            input.Bind(Action.LookX, InputAction.FromAxis(InputAction.Axis.MouseHorizontal, false, 1f));
            input.Bind(Action.LookY, InputAction.FromAxis(InputAction.Axis.MouseVertical, true, 1f));

            RaycastHit hit = default(RaycastHit);
            if(Physics.Raycast(transform.position, Vector3.down, out hit))
            {
                transform.position = hit.point + Vector3.up;
            }

            BaseSceneManager.instance.RequestCamera(this);
        }
        
        //
        // --------------------------------------------------------------------
        //
        
        protected virtual void Update()
        {
            if(!hasCamera) return;

            //
            // look input
            //
            Vector3 deltaLook = new Vector3(
                input.GetAxis(Action.LookY) * lookSpeed,
                input.GetAxis(Action.LookX) * lookSpeed,
                0f
            );

            lookDir += deltaLook * Time.deltaTime;
            lookDir.x = btMath.Clamp(lookDir.x, -90f, 90f);
            lookDir.y = lookDir.y % 360f;
            cameraBoom.transform.rotation = Quaternion.Euler(lookDir.x, lookDir.y, 0f);

            //
            // gravity
            //
            Vector3 delta = Physics.gravity;

            //
            // move input
            //
            Vector3 moveDelta = new Vector3(
                input.GetAxis(Action.MoveX) * moveSpeed,
                0f,
                input.GetAxis(Action.MoveY) * moveSpeed
            );

            delta += cam.TransformDirection(moveDelta);

            control.Move(delta * Time.deltaTime);
        }

        //
        // --------------------------------------------------------------------
        //
    }
}
