//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using System.Collections;

namespace blacktriangles
{
    public interface ITarget
    {
        // members /////////////////////////////////////////////////////////////
        Transform target                                        { get; }

        // methods /////////////////////////////////////////////////////////////
        void SetTarget( Transform target );
    }
}
 
