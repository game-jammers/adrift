//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;
using System.Collections;

namespace blacktriangles
{
    public class RandomizeAnimSpeed
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public Animator animator                                = null;
        public float minStartDelay                              = 0f;
        public float maxStartDelay                              = 2f;
        public float min                                        = 0.5f;
        public float max                                        = 1.5f;

        // unity callbacks ////////////////////////////////////////////////////
        protected virtual void Awake()
        {
            if(animator == null) animator = GetComponent<Animator>();

            animator.speed = 0f;

            StartCoroutine(WaitToStart());
        }

        //
        // --------------------------------------------------------------------
        //
        
        private IEnumerator WaitToStart()
        {
            var wait = new WaitForSeconds(btRandom.Range(minStartDelay, maxStartDelay));
            yield return wait;

            animator.speed = btRandom.Range(min, max);
        }
    }
}
