//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using System.Collections;

namespace blacktriangles
{
    public class MoveForward
        : MonoBehaviour
    {
        // members ////////////////////////////////////////////////////////////
        public float speed                                        = 1f;
        public bool is2d                                        = false;
        private Rigidbody rb                                    = null;
        private Rigidbody2D rb2d                                = null;

        public Vector3 moveDir                                  { get; private set; }

        // unity callbacks ////////////////////////////////////////////////////
        protected virtual void Awake()
        {
            rb = GetComponent<Rigidbody>();
            rb2d = GetComponent<Rigidbody2D>();

            moveDir = is2d ? transform.up : transform.forward;
        }

        protected virtual void Update()
        {
            if( rb == null && rb2d == null )
            {
                transform.position += moveDir * Time.deltaTime * speed;
            }
        }

        protected virtual void FixedUpdate()
        {
            Vector3 newPos = transform.position + ( moveDir * Time.fixedDeltaTime * speed );
            if( rb != null )
            {
                rb.MovePosition( newPos );
            }
            else if( rb2d != null )
            {
                rb2d.MovePosition( newPos );
            }
        }
    }
}
