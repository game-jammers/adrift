//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    public class Oscillate
        : MonoBehaviour
    {
        // members /////////////////////////////////////////////////////////////
        public Vector3 speed                                    = Vector3.one;
        public Vector3 amplitude                                = Vector3.one;

        private Vector3 startPos                                = Vector3.zero;

        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void Awake()
        {
            startPos = transform.position;
        }

        protected virtual void Update()
        {
            transform.position = startPos + new Vector3(
                Mathf.Sin(Time.time * speed.x) * amplitude.x,
                Mathf.Sin(Time.time * speed.y) * amplitude.y,
                Mathf.Sin(Time.time * speed.z) * amplitude.z
            );
        }
    }
}
