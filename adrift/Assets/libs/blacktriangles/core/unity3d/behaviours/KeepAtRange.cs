//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using System.Collections;

namespace blacktriangles
{
    public class KeepAtRange
        : MonoBehaviour
        , ITarget
    {
        // members /////////////////////////////////////////////////////////////
        public Transform target                                 { get; private set; }
        [SerializeField] private Transform startTarget          = null;

        private Vector3 startPosition                           = Vector3.zero;
        private Vector3 startTargetPosition                     = Vector3.zero;

        // constructor / destructor ////////////////////////////////////////////
        public void Start()
        {
            if( startTarget != null )
            {
                SetTarget( startTarget );
            }
        }

        public void SetTarget( Transform _target )
        {
            target = _target;
            startPosition = transform.position;
            startTargetPosition = target.transform.position;
        }

        // unity callbacks /////////////////////////////////////////////////////
        protected void Update()
        {
            if( target != null )
            {
                Vector3 targetDiff = target.transform.position - startTargetPosition;
                transform.position = startPosition + targetDiff;
            }
        }
    }
}
