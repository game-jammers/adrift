//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public class GroundCast
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public LayerMask groundLayer;

        public float penetrationAmt                             = 1f;
        public float verticalOffset                             = 10f;


        //
        // public methods /////////////////////////////////////////////////////
        //
        
        [ContextMenu("Ground Cast")]
        public void CastToGround()
        {
            RaycastHit hit = default(RaycastHit);
            if(Physics.Raycast(transform.position + Vector3.up*verticalOffset,
                            -Vector3.up,
                            out hit,
                            verticalOffset + penetrationAmt,
                            groundLayer))
            {
                transform.position = hit.point;
            }
        }
    }
}
