//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

﻿using UnityEngine;
using System.Collections;

namespace blacktriangles
{
    public class MoveToTarget
        : MonoBehaviour
    {
        // members ////////////////////////////////////////////////////////////
        public float speed                                        = 1f;
        public Vector3 targetPos                                { get { return targetTrans == null ? _targetPos : targetTrans.position; } }
        public Vector3 moveDir                                    { get { return ( targetPos - transform.position ).normalized; } }

        public Transform targetTrans                            = null;
        [SerializeField] private Vector3 _targetPos                = Vector3.zero;

        private Rigidbody rb                                    = null;

        // unity callbacks ////////////////////////////////////////////////////
        protected virtual void Awake()
        {
            rb = GetComponent<Rigidbody>();
        }

        protected virtual void Update()
        {
            if( rb == null )
            {
                transform.position += moveDir * Time.deltaTime * speed;
            }
        }

        protected virtual void FixedUpdate()
        {
            if( rb != null )
            {
                rb.MovePosition( transform.position + ( moveDir * Time.fixedDeltaTime * speed ) );
            }
        }
    }
}
