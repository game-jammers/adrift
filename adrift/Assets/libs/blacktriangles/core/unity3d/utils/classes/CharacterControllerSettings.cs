//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using UnityEngine;

namespace blacktriangles
{
    [System.Serializable]
    public struct CharacterControllerSettings
    {
        // types //////////////////////////////////////////////////////////////
        public enum Direction
        {
            XAxis = 0,
            YAxis = 1,
            ZAxis = 2
        }

        // members ////////////////////////////////////////////////////////////
        public Vector3 center;
        public float height;
        public float radius;
        public Direction direction;

        public float slopeLimit;
        public float skinWidth;

        public float minMoveDistance;

        // constructor / initializer //////////////////////////////////////////

        public static CharacterControllerSettings Default()
        {
            CharacterControllerSettings result = new CharacterControllerSettings();
            result.center = Vector3.zero;
            result.height = 2.0f;
            result.radius = 1.0f;
            result.direction = Direction.YAxis;
            result.slopeLimit = 45.0f;
            result.skinWidth = 0.08f;
            result.minMoveDistance = 0.001f;
            return result;
        }

        // public methods //////////////////////////////////////////////////////
        public void Apply( CharacterController controller )
        {
            if( controller == null ) return;
            controller.center = center;
            controller.height = height;
            controller.radius = radius;

            controller.slopeLimit = slopeLimit;
            controller.skinWidth = skinWidth;

            controller.minMoveDistance = minMoveDistance;
        }

        public void Apply( CapsuleCollider collider )
        {
            if( collider == null ) return;
            collider.center = center;
            collider.radius = radius;
            collider.height = height;
            collider.direction = (int)direction;
        }

        public void Copy( CharacterController controller )
        {
            if( controller == null ) return;
            center = controller.center;
            height = controller.height;
            radius = controller.radius;

            slopeLimit = controller.slopeLimit;
            skinWidth = controller.skinWidth;

            minMoveDistance = controller.minMoveDistance;

        }

        public void Copy( CapsuleCollider collider )
        {
            if( collider == null ) return;
            center = collider.center;
            radius = collider.radius;
            height = collider.height;
            direction = (Direction)collider.direction;
        }

        #if UNITY_EDITOR
        private void CopyFromSelection()
        {
            GameObject obj = UnityEditor.Selection.activeObject as GameObject;
            if( obj != null )
            {
                CapsuleCollider col = obj.GetComponent<CapsuleCollider>();
                if( col != null )
                {
                    Copy(col);
                }
                else
                {
                    CharacterController cont = obj.GetComponent<CharacterController>();
                    if( cont != null )
                    {
                        Copy(cont);
                    }
                }
            }
        }
        #endif
    }
}
