//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System;
using UnityEngine;

namespace blacktriangles
{
    [AttributeUsage(AttributeTargets.Field)]
    public class ShowIfAttribute 
        : PropertyAttribute
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public readonly string methodName;
        public readonly bool invert;

        //
        // constructor ////////////////////////////////////////////////////////
        //
        
        public ShowIfAttribute(string methodName, bool invert = false)
        {
            this.methodName = methodName;
            this.invert = invert;
        }
    }
}
