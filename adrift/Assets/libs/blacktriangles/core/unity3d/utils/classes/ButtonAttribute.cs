//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

// https://www.reddit.com/r/Unity3D/comments/1s6czv/inspectorbutton_add_a_custom_button_to_your/

using UnityEngine;

namespace blacktriangles
{
    [System.AttributeUsage(System.AttributeTargets.Method)]
    public class ButtonAttribute 
        : System.Attribute
    {

        //
        // constants //////////////////////////////////////////////////////////
        //

        public readonly float kDefaultButtonWidth               = 80f;
        public readonly string MethodName;

        //
        // members ////////////////////////////////////////////////////////////
        //
                
        public float buttonWidth                                { get; set; }

        //
        // constructor ////////////////////////////////////////////////////////
        //
        
        public ButtonAttribute(string MethodName)
        {
            buttonWidth = kDefaultButtonWidth;
            this.MethodName = MethodName;
        }
    }
}
