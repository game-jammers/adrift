//
// (c) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//

using UnityEngine;
using System.Collections.Generic;

namespace blacktriangles
{
    public static partial class MeshUtility
    {
        //
        // types //////////////////////////////////////////////////////////////
        //
        
        public struct MergeResult
        {
            public GameObject root;
            public Animator animator;
            public SkinnedMeshRenderer skin;
            public Mesh mesh;
        };

        //
        // --------------------------------------------------------------------
        //

        [System.Serializable]
        public struct MergeSettings
        {
            public bool mergeSubmeshes;
            public bool useMatrices;

            public static MergeSettings Default()
            {
                return new MergeSettings { mergeSubmeshes = false, useMatrices = false };
            }
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public static MergeResult MergeSkinnedMeshes( GameObject rig, IEnumerable<SkinnedMeshRenderer> skins )
        {
            return MergeSkinnedMeshes(rig, skins, MergeSettings.Default());
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static MergeResult MergeSkinnedMeshes( GameObject rig, IEnumerable<SkinnedMeshRenderer> skins, MergeSettings settings )
        {
            GameObject newRigObj = GameObject.Instantiate( rig, Vector3.zero, Quaternion.identity ) as GameObject;
            GameObject newSkinObj = new GameObject("NewMesh");
            SkinnedMeshRenderer r = newSkinObj.AddComponent<SkinnedMeshRenderer>() as SkinnedMeshRenderer;

            newSkinObj.transform.SetParent( newRigObj.transform, false );

            // The SkinnedMeshRenderers that will make up a character will be
            // combined into one SkinnedMeshRenderers using multiple materials.
            // This will speed up rendering the resulting character.
            List<CombineInstance> combineInstances = new List<CombineInstance>();
            List<Material> materials = new List<Material>();
            List<Transform> bones = new List<Transform>();
            List<BoneWeight> boneWeights = new List<BoneWeight>();
            Transform[] transforms = newRigObj.GetComponentsInChildren<Transform>();

            foreach (SkinnedMeshRenderer smr in skins)
            {
                materials.AddRange(smr.sharedMaterials);

                int subMax = smr.sharedMesh.subMeshCount;
                for( int sub = 0; sub < subMax; ++sub )
                {
                    CombineInstance ci = new CombineInstance();
                    ci.mesh = smr.sharedMesh;
                    ci.subMeshIndex = sub;
                    combineInstances.Add(ci);

                    // for each submesh, we add a copy of the bone references for that submesh
                    // each submesh the bones were indexed based off the first bone being
                    // zero, however, in the combined bones array, we offset them by
                    // this sections placement within the overall list. (boneIndexOffset)
                    int boneIndexOffset = bones.Count;
                    foreach( Transform boneTrans in smr.bones )
                    {
                        string bone = boneTrans.name;
                        foreach (Transform transform in transforms)
                        {
                            if (transform.name != bone) continue;
                            bones.Add(transform);
                            break;
                        }
                    }

                    // we take the weights and offset them so they match up to the
                    // bones we just added.  We could streamline this process by using
                    // a lookup table
                    for( int i = 0; i < ci.mesh.vertexCount; ++i )
                    {
                        BoneWeight w = smr.sharedMesh.boneWeights[i];
                        w.boneIndex0 += boneIndexOffset;
                        w.boneIndex1 += boneIndexOffset;
                        w.boneIndex2 += boneIndexOffset;
                        w.boneIndex3 += boneIndexOffset;
                        boneWeights.Add( w );
                    }
                }
            }

            // Obtain and configure the SkinnedMeshRenderer attached to
            // the character base
            r.sharedMesh = new Mesh();
            r.sharedMesh.CombineMeshes(combineInstances.ToArray(), settings.mergeSubmeshes, settings.useMatrices );
            BoneWeight[] weights = boneWeights.ToArray();
            if(weights.Length != r.sharedMesh.vertices.Length)
            {
                Dbg.Warn("We have {0} weights but {1} vertices", weights.Length, r.sharedMesh.vertices.Length);
            }

            r.sharedMesh.boneWeights = weights;
            r.bones = bones.ToArray();

            r.materials = settings.mergeSubmeshes ? new Material[] { materials[0] } : materials.ToArray();

            MergeResult mergeResult = new MergeResult();
            mergeResult.root = newRigObj;
            mergeResult.animator = newRigObj.GetComponent<Animator>();
            mergeResult.skin = r;
            mergeResult.mesh = r.sharedMesh;

            mergeResult.mesh.RecalculateBounds();
            mergeResult.mesh.RecalculateNormals();

            ApplyAnimationFix(mergeResult.root.GetComponent<Animator>(), mergeResult.mesh);

            return mergeResult;
        }

        //
        // Merge Meshes ///////////////////////////////////////////////////////
        //

        public static Mesh MergeMeshes( IEnumerable<MeshFilter> meshes )
        {
            List<CombineInstance> combines = new List<CombineInstance>();
            foreach( MeshFilter filter in meshes )
            {
                CombineInstance ci = new CombineInstance();
                ci.mesh = filter.sharedMesh;
                ci.transform = filter.gameObject.transform.localToWorldMatrix;
                combines.Add( ci );
            }

            Mesh result = new Mesh();
            result.CombineMeshes( combines.ToArray(), true, true );
            return result;
        }

        //
        // Utility Functions //////////////////////////////////////////////////
        //
        
        public static void ApplyAnimationFix(Animator animator, Mesh mesh)
        {
            AnimatorCullingMode original = animator.cullingMode;
            animator.cullingMode = AnimatorCullingMode.AlwaysAnimate;
            Delay.Callback(()=>{
                mesh.RecalculateBounds();
                animator.cullingMode = original;
            }, 100);
        }
    }
}
