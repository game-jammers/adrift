//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles
{
    [System.Serializable]
    public class GameObjectPools
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Transform inactiveRoot                           = null;

        private Dictionary<GameObject, GameObjectPool> pools    = new Dictionary<GameObject, GameObjectPool>();

        //
        // public methods /////////////////////////////////////////////////////
        //

        public T Take<T>(T prefab, Vector3 position, Quaternion rotation, Transform parent, out float timestamp)
            where T: Component
        {
            timestamp = 0f;
            if(prefab == null)
                return null;

            T res = null;
            GameObject obj = Take(prefab.gameObject, position, rotation, parent, out timestamp);
            if(obj != null)
            {
                res = obj.GetComponent<T>();
            }

            return res;
        }

        //
        // --------------------------------------------------------------------
        //

        public T Take<T>(T prefab, Vector3 position, Quaternion rotation, Transform parent)
            where T: Component
        {
            float ts = 0f;
            return Take(prefab, position, rotation, parent, out ts);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public GameObject Take(GameObject prefab, Vector3 position, Quaternion rotation, Transform parent, out float timestamp)
        {
            timestamp = 0f;
            if(prefab == null) return null;

            GameObjectPool pool = null;
            if(pools.TryGetValue(prefab, out pool) == false)
            {
                pool = new GameObjectPool();
                pool.prefab = prefab;
                pool.root = inactiveRoot;
                pools[prefab] = pool;
            }

            return pool.Take(position, rotation, parent, out timestamp);

        }

        //
        // --------------------------------------------------------------------
        //

        public GameObject Take(GameObject prefab, Vector3 position, Quaternion rotation, Transform parent)
        {
            float ts = 0f;
            return Take(prefab, position, rotation, parent, out ts);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void Return(GameObject item)
        {
            ReturnToPool ret = item.GetComponent<ReturnToPool>();
            Return(ret);
        }

        //
        // --------------------------------------------------------------------
        //

        public void Return(ReturnToPool item)
        {
            Dbg.Assert(item != null, "Returning item to pool that does not have a ReturnToPool component");
            Dbg.Assert(item.pool != null, "Returning item to pool that does not have a pool");
            Dbg.Assert(item.prefab != null, "Returning item to pool that does not have a prefab");

            GameObjectPool pool = pools[item.prefab];
            pool.Return(item.gameObject);
        }
    }
}
