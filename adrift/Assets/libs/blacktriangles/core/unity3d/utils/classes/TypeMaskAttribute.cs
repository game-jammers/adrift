//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public class TypeMaskAttribute
        : PropertyAttribute
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public System.Type[] types                              { get; private set; }
        public string[] names                                   { get; private set; }

        //
        // constructor / initializer  /////////////////////////////////////////
        //
        
        public TypeMaskAttribute(System.Type baseType)
        {
            #if UNITY_EDITOR
                SetTypes(AssemblyUtility.CollectAllOfType(baseType).ToArray());
            #endif
        }

        //
        // --------------------------------------------------------------------
        //

        public TypeMaskAttribute(System.Type[] _types)
        {
            SetTypes(_types);
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void SetTypes(System.Type[] _types)
        {
            #if UNITY_EDITOR
            types = _types;
            names = new string[types.Length];
            for(int idx = 0; idx < types.Length; ++idx)
            {
                names[idx] = types[idx].Name;
            }
            #endif
        }
        
    }
}
