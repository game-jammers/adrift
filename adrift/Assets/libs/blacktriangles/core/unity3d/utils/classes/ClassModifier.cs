//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Reflection;

namespace blacktriangles
{
    //
    // ////////////////////////////////////////////////////////////////////////
    //

    public enum ClassModifierType
    {
        Add,
        Subtract,
        Multiply,
        Divide,
        Set
    }

    //
    // ////////////////////////////////////////////////////////////////////////
    //

    public class ClassModifier
    {
        public FieldInfo field;
        public ClassModifierType type;
        public float modifier;
    }
}
