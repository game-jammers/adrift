//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles
{
    //
    // ########################################################################
    //
    
    public enum TerrainNeighbor
    {
        Center,
        NorthWest,
        North,
        NorthEast,
        East,
        SouthEast,
        South,
        SouthWest,
        West
    }

    //
    // ########################################################################
    //

    public static class TerrainUtils
    {
        public static Terrain GetNorthWestNeighbor(this Terrain self)
        {
            if(self.topNeighbor != null)
                return self.topNeighbor.leftNeighbor;
            return null;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static Terrain GetNorthEastNeighbor(this Terrain self)
        {
            if(self.topNeighbor != null)
                return self.topNeighbor.rightNeighbor;
            return null;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static Terrain GetSouthWestNeighbor(this Terrain self)
        {
            if(self.bottomNeighbor != null)
                return self.bottomNeighbor.leftNeighbor;
            return null;
        }

        //
        // --------------------------------------------------------------------
        //

        public static Terrain GetSouthEastNeighbor(this Terrain self)
        {
            if(self.bottomNeighbor != null)
                return self.bottomNeighbor.rightNeighbor;
            return null;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static Terrain GetNeighbor(this Terrain self, TerrainNeighbor neighbor)
        {
            if(self == null) return null;

            switch(neighbor)
            {
                case TerrainNeighbor.Center: return self;
                case TerrainNeighbor.NorthWest: return self.GetNorthWestNeighbor();
                case TerrainNeighbor.North: return self.topNeighbor;
                case TerrainNeighbor.NorthEast: return self.GetNorthEastNeighbor();
                case TerrainNeighbor.East: return self.rightNeighbor;
                case TerrainNeighbor.SouthEast: return self.GetSouthEastNeighbor();
                case TerrainNeighbor.South: return self.bottomNeighbor;
                case TerrainNeighbor.SouthWest: return self.GetSouthWestNeighbor();
                case TerrainNeighbor.West: return self.leftNeighbor;
            }

            Dbg.Error($"Failed to get terrain neighbor with {neighbor}");
            return null;
        }

        //
        // --------------------------------------------------------------------
        //

        public static IEnumerable<Terrain> GetNeighbors(this Terrain self)
        {
            TerrainNeighbor[] neighbors = EnumUtility.GetValues<TerrainNeighbor>();
            foreach(TerrainNeighbor neighbor in neighbors)
            {
                yield return self.GetNeighbor(neighbor);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public static void ForEachNeighbor(this Terrain self, System.Action<TerrainNeighbor, Terrain> act)
        {
            TerrainNeighbor[] neighbors = EnumUtility.GetValues<TerrainNeighbor>();
            foreach(TerrainNeighbor neighbor in neighbors)
            {
                act(neighbor, self.GetNeighbor(neighbor));
            }
        }
        
    }
}
