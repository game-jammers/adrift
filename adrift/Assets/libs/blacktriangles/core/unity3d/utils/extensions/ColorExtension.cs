//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System;
using System.Globalization;
using UnityEngine;

namespace blacktriangles
{
    public static class ColorExtension
    {
        public static Color FromHexString(string str)
        {
            if(str[0] != '#') return Color.black;
            if(str.Length < 7) return Color.black;

            float r = System.UInt32.Parse(str.Substring(1,2), NumberStyles.HexNumber) / 255.0f;
            float g = System.UInt32.Parse(str.Substring(3,2), NumberStyles.HexNumber) / 255.0f;
            float b = System.UInt32.Parse(str.Substring(5,2), NumberStyles.HexNumber) / 255.0f;
            float a = 1.0f;

            if(str.Length == 9)
            {
                a = System.UInt32.Parse(str.Substring(7,2), NumberStyles.HexNumber) / 255.0f;
            }

            return new Color(r,g,b,a);
        }
        
        //
        // --------------------------------------------------------------------
        //
        
        public static string ToHexString( this Color self )
        {
            byte[] bytes = { self.r.ToByte(), self.g.ToByte(), self.b.ToByte() };
            return "#" + System.BitConverter.ToString(bytes).Replace("-","");
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static string ToHexStringRGBA( this Color self )
        {
            byte[] bytes = { self.r.ToByte(), self.g.ToByte(), self.b.ToByte(), self.a.ToByte() };
            return "#" + System.BitConverter.ToString(bytes).Replace("-","");
        }
    }
}
