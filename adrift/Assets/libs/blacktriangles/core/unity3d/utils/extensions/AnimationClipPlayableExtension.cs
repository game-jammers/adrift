//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine.Animations;
using UnityEngine.Playables;
using UnityEngine;

namespace blacktriangles
{
    public static class AnimationClipPlayableExtension
    {
        //
        // --------------------------------------------------------------------
        //
        
        public static float GetLength(this AnimationClipPlayable clip)
        {
            AnimationClip anim = clip.GetAnimationClip();
            return anim.length;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static float GetDuration(this AnimationClipPlayable clip)
        {
            AnimationClip anim = clip.GetAnimationClip();
            return (float)(anim.length * (1f / clip.GetSpeed()));
        }

        //
        // --------------------------------------------------------------------
        //
        
        public static bool IsDone(this AnimationClipPlayable clip)
        {
            return clip.GetTime() > clip.GetDuration();
        }

        //
        // --------------------------------------------------------------------
        //

        public static void Reset(this AnimationClipPlayable clip)
        {
            clip.SetTime(0);
        }
    }
}
