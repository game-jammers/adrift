//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using System.Collections;

namespace blacktriangles
{
    public static class MonoBehaviourExtensions
    {
        //
        // --------------------------------------------------------------------
        //

        public static void InvokeNextFrame(this MonoBehaviour self, System.Action func)
        {
            self.StartCoroutine(InvokeNextFrame(func));
        }

        //
        // --------------------------------------------------------------------
        //

        private static System.Collections.IEnumerator InvokeNextFrame(System.Action func)
        {
            yield return new WaitForSeconds(0f);
            func();
        }
        
        
        //
        // ---------------------------------------------------------------------
        //
        
        public static void Invoke( this MonoBehaviour self, System.Action func, float delayInSeconds )
        {
            self.StartCoroutine( Invoke( func, delayInSeconds ) );
        }

        //
        // --------------------------------------------------------------------
        //

        private static System.Collections.IEnumerator Invoke( System.Action func, float delayInSeconds )
        {
            yield return new WaitForSeconds( delayInSeconds );
            func();
        }
    }
}
