//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;

namespace blacktriangles
{
    public static class UnityUtils
    {
        public static T SafeDestroy<T>(T obj)
            where T: UnityEngine.Object
        {
            if(obj == null) return null;
            #if UNITY_EDITOR
                if(!UnityEditor.EditorApplication.isPlaying)
                {
                    Object.DestroyImmediate(obj);
                    return null;
                }
            #endif

            Object.Destroy(obj);
            return null;
        }

        //
        // --------------------------------------------------------------------
        //

        public static T GetComponentInParentNotMe<T>( Component self )
            where T: Component
        {
            if(self.transform.parent != null)
            {
                return self.transform.parent.GetComponentInParent<T>();
            }

            return null;
        }

        //
        // --------------------------------------------------------------------
        //

        public static T GetComponentInChildrenNotMe<T>( Component self  )
            where T: Component
        {
            for(int i = 0; i < self.transform.childCount; ++i)
            {
                Transform child = self.transform.GetChild(i);
                T result = child.GetComponentInChildren<T>();
                if(result != null)
                    return result;
            }

            return null;
        }
    }
}
