//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace blacktriangles
{
    public class BasePropDrawer
        : PropertyDrawer
    {
        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float lineHeight = EditorGUI.GetPropertyHeight(property, label, true);
            return lineHeight;
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected void Repaint(SerializedProperty property)
        {
             foreach (var item in ActiveEditorTracker.sharedTracker.activeEditors)
             {
                if (item.serializedObject == property.serializedObject)
                { 
                    item.Repaint(); return;
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //

        protected object GetOwner(SerializedProperty property)
        {
            string path = property.propertyPath;
            string[] paths = path.Split('.');
            paths = paths.Slice(0, paths.Length-1);

            BindingFlags flags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;

            object target = property.serializedObject.targetObject;
            foreach(string p in paths)
            {
                if(p == "Array") continue;
                int braceIdx = p.IndexOf('[');
                if(braceIdx >= 0)
                {
                    int closeIdx = p.IndexOf(']');
                    if(closeIdx >= 0)
                    {
                        int idx = System.Int32.Parse(p.Substring(braceIdx+1, closeIdx-braceIdx-1));
                        IList list = (IList)target;
                        target = list[idx];
                    }
                }
                else
                {
                    System.Type targetType = target.GetType();
                    FieldInfo info = targetType.GetField(p, flags);
                    Dbg.Assert(info != null, $"Could not find field {p} ({path}) in type {target.GetType()}");
                    target = info.GetValue(target);
                }
            }

            return target;
        }
    }

    //
    // ########################################################################
    //
    
    public class PropDrawer<T>
        : BasePropDrawer
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        protected T GetValue(SerializedProperty property)
        {
            object owner = GetOwner(property);
            return (T)fieldInfo.GetValue(owner);
        }

        //
        // --------------------------------------------------------------------
        //

        protected void SetValue(SerializedProperty property, T val)
        {
            object owner = GetOwner(property);
            fieldInfo.SetValue(owner, val);
        }
        
    }
}
