//
// (C) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;
using UnityEditor;

namespace blacktriangles
{
    //
    // TypeFilter /////////////////////////////////////////////////////////////
    //

    [CustomPropertyDrawer(typeof(TypeFilterAttribute))]
    public class TypeFilter
        : PropertyDrawer
    {
        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            TypeFilterAttribute attr = attribute as TypeFilterAttribute;
            if(attr == null) return;

            EditorGUI.BeginProperty(position, label, property);

            switch(property.propertyType)
            {
                case SerializedPropertyType.String: DrawStringProperty(position, property, label, attr); break;
                case SerializedPropertyType.Generic: DrawSerializedType(position, property, label, attr); break;
                default:
                    Dbg.Log(property.propertyPath + ' ' + property.propertyType.ToString());
                break;
            }

            EditorGUI.EndProperty();
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void DrawStringProperty(Rect position, SerializedProperty property, GUIContent label, TypeFilterAttribute attr)
        {
            var names = new System.Collections.Generic.List<string>();
            var displayNames = new System.Collections.Generic.List<string>();
            foreach(System.Type type in attr.types)
            {
                names.Add(type.AssemblyQualifiedName);
                displayNames.Add(type.Name);
            }

            displayNames.Add("<Null>");

            int selected = names.IndexOf(property.stringValue);
            selected = EditorGUI.Popup(position, label.text, selected, displayNames.ToArray());
            if(attr.types.IsValidIndex(selected))
            {
                property.stringValue = names[selected];
            }
            else
            {
                property.stringValue = null;
            }
        }

        //
        // ----------------------------------------------------------------------------
        //

        private void DrawSerializedType(Rect position, SerializedProperty root, GUIContent label, TypeFilterAttribute attr)
        {
            SerializedProperty prop = root.FindPropertyRelative("_typename");
            if(prop == null) return;

            DrawStringProperty(position, prop, label, attr);
        }
    };
}
