//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace blacktriangles
{
    [CustomPropertyDrawer(typeof(ShowIfAttribute))]
    public class ConditionalFieldAttributeDrawer 
        : BasePropDrawer
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public ShowIfAttribute attrib                           { get { return attribute as ShowIfAttribute; } }

        private bool shouldShow                                 = false;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            Refresh(property);
            float lineHeight = 0f;
            if(shouldShow)
            {
                lineHeight = EditorGUI.GetPropertyHeight(property, label, true);
            }

            return lineHeight;
        }

        //
        // --------------------------------------------------------------------
        //

        public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label)
        {
            Refresh(prop);
            if(shouldShow)
            {
                EditorGUI.PropertyField(position, prop, label, true);
            }
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void Refresh(SerializedProperty property)
        {
            bool found = false; 
            shouldShow = true;

            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance;

            object owner = GetOwner(property);
            System.Type type = owner.GetType();
            MethodInfo info = type.GetMethod(
                attrib.methodName, 
                flags,
                null, 
                CallingConventions.Any, 
                new System.Type[] {}, 
                null
            );

            if(info != null)
            {
                shouldShow = (bool)info.Invoke(owner, null);
                found = true;
            }
            else
            {
                FieldInfo fieldInfo = type.GetField(attrib.methodName, flags);
                if(fieldInfo != null && fieldInfo.FieldType == typeof(bool))
                {
                    shouldShow = (bool)fieldInfo.GetValue(owner);
                    found = true;
                }
                else
                {
                    PropertyInfo propInfo = type.GetProperty(attrib.methodName, flags);
                    if(propInfo != null && propInfo.GetMethod != null)
                    {
                        shouldShow = (bool)propInfo.GetMethod.Invoke(owner, null);
                        found = true;
                    }
                }
            }

            
            if(found && attrib.invert)
            {
                shouldShow = !shouldShow;
            }
        }
    }
}
