//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace blacktriangles
{
    [CustomPropertyDrawer(typeof(BaseEnumList), true)]
    public class EnumListDrawer
        : PropDrawer<BaseEnumList>
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        private bool unfolded                                   = true;
        private float totalSize                                 = 0f;
        
        //
        // public methods /////////////////////////////////////////////////////
        //

        public override float GetPropertyHeight(SerializedProperty property,
                                                GUIContent label)
        {
            float result = EditorGUIUtility.singleLineHeight;
            if(unfolded)
            {
                if(totalSize < result)
                {
                    BaseEnumList list = GetList(property);
                    List<BaseEnumList.BaseEntry> entries = list.GetBaseEntries();
                    
                    foreach(BaseEnumList.BaseEntry entry in entries)
                    {
                        if(entry.item == null || typeof(Object).IsAssignableFrom(list.valueType))
                        {
                            totalSize += EditorGUIUtility.singleLineHeight;
                        }
                        else
                        {
                            totalSize += btGui.CalculateReflectedHeight(entry);
                        }
                    }
                }

                result = totalSize;
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public override void OnGUI(Rect position,
                                   SerializedProperty property,
                                   GUIContent label)
        {
            float start = position.y;

            position.height = EditorGUIUtility.singleLineHeight;

            string title = char.ToUpper(property.name[0]) + property.name.Substring(1);

            unfolded = EditorGUI.Foldout(position, unfolded, title);
            position.y += EditorGUIUtility.singleLineHeight;
            
            if(unfolded)
            {
                BaseEnumList list = GetList(property);
                List<BaseEnumList.BaseEntry> entries = list.GetBaseEntries();
                
                EditorGUI.indentLevel += 1;
                foreach(BaseEnumList.BaseEntry entry in entries)
                {
                    float entryHeight = DrawEntry(property, list, entry, position);
                    position.y += entryHeight;
                }
                EditorGUI.indentLevel -= 1;
            }
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private float DrawEntry(SerializedProperty prop, BaseEnumList list, BaseEnumList.BaseEntry entry, Rect position)
        {
            float usedHeight = 0f;

            EditorGUI.BeginChangeCheck();
            string enumName = System.Enum.ToObject(list.enumType, entry.key).ToString();
            
            EditorGUI.indentLevel += 1;

            bool changed = false;

            if(typeof(Object).IsAssignableFrom(list.valueType))
            {
                object res = EditorGUI.ObjectField(position, enumName, (Object)entry.item, list.valueType, true);
                list.SetBaseEntry(entry.key, res);
                usedHeight += EditorGUIUtility.singleLineHeight;
            }
            else if(entry.item == null)
            {
                if(GUI.Button(position, "[NULL] Create?"))
                {
                    object res = System.Activator.CreateInstance(list.valueType);
                    list.SetBaseEntry(entry.key, res);
                }
                usedHeight += EditorGUIUtility.singleLineHeight;
            }
            else
            {
                usedHeight += btGui.ObjectReflectionGUI(position, enumName, ref entry.item);
                list.SetBaseEntry(entry.key, entry.item);
            }

            changed = changed || EditorGUI.EndChangeCheck();

            if(changed)
            {
                EditorUtility.SetDirty(prop.serializedObject.targetObject);
            }

            EditorGUI.indentLevel -= 1;

            return usedHeight;
        }

        //
        // --------------------------------------------------------------------
        //
        
        private BaseEnumList GetList(SerializedProperty property)
        {
            return GetValue(property);
        }
    }
}
