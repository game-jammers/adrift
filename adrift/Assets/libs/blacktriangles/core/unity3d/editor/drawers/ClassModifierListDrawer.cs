//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEditor;

namespace blacktriangles
{
    [CustomPropertyDrawer(typeof(BaseClassModifierList), true)]
    public class ClassModifierListDrawer
        : PropDrawer<BaseClassModifierList>
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        private bool unfolded                                   = true;

        const float kHeight = 20f;

        private bool initialized                                = false;
        private string title                                    = null;
        private BaseClassModifierList list                      = null;
        private List<FieldInfo> fields                          = null;
        private List<string> fieldNames                         = null;

        //
        // ////////////////////////////////////////////////////////////////////
        //

        public override float GetPropertyHeight(SerializedProperty property,
                                                GUIContent label)
        {
            Initialize(property);

            float baseHeight = base.GetPropertyHeight(property, label);
            if(unfolded)
            {
                baseHeight *= list.count + 3;
            }

            return baseHeight;
        }

        //
        // ------------------------------------------------------------------------
        //

        private void Initialize(SerializedProperty property)
        {
            if(initialized) return;

            initialized = true;

            title = char.ToUpper(property.name[0]) + property.name.Substring(1);

            list = GetValue(property);

            fields = list.GetFields();
            fieldNames = new List<string>();
            foreach(FieldInfo field in fields)
            {
                fieldNames.Add(field.Name);
            }
        }
        
        //
        // public methods /////////////////////////////////////////////////////
        //

        public override void OnGUI(Rect position,
                                   SerializedProperty property,
                                   GUIContent label)
        {
            Initialize(property);

            position.height = kHeight - 2f;

            unfolded = EditorGUI.Foldout(position, unfolded, title);
            position.y += kHeight;

            if(unfolded)
            {
                EditorGUI.BeginChangeCheck();

                List<ClassModifier> mods = list.GetModifiers();
                for(int idx = 0; idx < mods.Count; ++idx)
                {
                    ClassModifier mod = mods[idx];
                    if(mod == null || mod.field == null)
                    {
                        list.Reset();
                        return;
                    }

                    Draw(position, idx, mod);
                    position.y += kHeight;
                }

                DrawMenu(position);

                bool changed = EditorGUI.EndChangeCheck();
                if(changed)
                {
                    EditorUtility.SetDirty(property.serializedObject.targetObject);
                }
            }
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void DrawMenu(Rect position)
        {
            if(GUI.Button(position, "New Modifier"))
            {
                list.AddModifier(new ClassModifier() {
                    field = fields[0],
                    type = ClassModifierType.Multiply,
                    modifier = 1f
                });
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        
        private void Draw(Rect position, int idx, ClassModifier mod)
        {
            float kButtonWidth = 25f;
            float kWidth = (position.width-kButtonWidth) / 3;

            position.width = kWidth;

            int nameIdx = fieldNames.IndexOf(mod.field.Name);
            nameIdx = EditorGUI.Popup(position, nameIdx, fieldNames.ToArray());
            if(fields.IsValidIndex(nameIdx))
            {
                mod.field = fields[nameIdx];
            }
            else
            {
                mod.field = null;
            }
                
            position.x += kWidth;

            mod.type = (ClassModifierType)EditorGUI.Popup(position, (int)mod.type, System.Enum.GetNames(typeof(ClassModifierType)));
            position.x += kWidth;

            mod.modifier = EditorGUI.FloatField(position, mod.modifier);
            position.x += kWidth;

            position.width = kButtonWidth;
            if(GUI.Button(position, "X"))
            {
                list.RemoveModifier(idx);
            }
        }
    }
}
