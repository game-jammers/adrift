//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEditor;
using System;
using System.IO;

namespace blacktriangles
{
    public static class DebugMenu
    {
        // public methods /////////////////////////////////////////////////////
        [MenuItem("Tools/blacktriangles/Debug/Clear Progress Bar")]
        static void ClearProgressBar()
        {
            EditorUtility.ClearProgressBar();
        }
    }
}
