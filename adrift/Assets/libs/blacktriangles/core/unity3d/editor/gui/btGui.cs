//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
// Using modified bezier curve drawing code originally written by Linusmartensson
// http://forum.unity3d.com/threads/drawing-lines-in-the-editor.71979/
//
//=============================================================================

using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace blacktriangles
{
    public static partial class btGui
    {
        public static void OnGUI()
        {
        }
    }
}
