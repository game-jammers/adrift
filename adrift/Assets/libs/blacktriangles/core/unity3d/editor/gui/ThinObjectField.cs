//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
// Using modified bezier curve drawing code originally written by Linusmartensson
// http://forum.unity3d.com/threads/drawing-lines-in-the-editor.71979/
//
//=============================================================================

using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace blacktriangles
{
    public static partial class btGui
    {
        //
        // thin object field ###################################################
        //
        public static T ThinObjectField<T>( string label, T obj, bool allowSceneObjects )
            where T: UnityEngine.Object
        {
            T result = null;
            GUILayout.BeginHorizontal();
                Vector2 size = GUI.skin.label.CalcSize( new GUIContent( label ) );
                GUILayout.Label( label, GUILayout.Width( size.x ) );
                result = EditorGUILayout.ObjectField( obj, typeof( T ), allowSceneObjects ) as T;
            GUILayout.EndHorizontal();

            return result;
        }
    }
}
