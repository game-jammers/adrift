//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

namespace blacktriangles
{
    public class TemplateCreationWindow
        : EditorWindow
    {
        // constants //////////////////////////////////////////////////////////
        private const string kMenuPath                          = "Tools/blacktriangles/Config/Template Creator";
        private const string kTitle                             = "Project Configuration Tool";

        // types ///////////////////////////////////////////////////////////////
        private class TemplateItem
        {
            public DirectoryInfo info;
            public bool enabled;
        }

        // members /////////////////////////////////////////////////////////////
        private List<TemplateItem> items = new List<TemplateItem>();
        private string importFrom = "Assets/scripts";
        private string templatePath = "";
        private string nspace = System.String.Empty;

        // constructor / destructor ///////////////////////////////////////////
        [MenuItem( kMenuPath )]
        private static void OpenWindow()
        {
            TemplateCreationWindow window = GetWindow<TemplateCreationWindow>();
            window.titleContent = new GUIContent( kTitle );
            window.templatePath = EditorConfig.kTemplatesDir.Replace("Assets/","");
            window.Initialize();
        }

        private void Initialize()
        {
            items.Clear();
            string importAssetPath = System.IO.Path.GetFullPath(importFrom);
            Debug.Log(importAssetPath);
            string[] paths = Directory.GetDirectories( importAssetPath );
            foreach( string path in paths )
            {
                TemplateItem item = new TemplateItem();
                item.info = new DirectoryInfo(path);
                item.enabled = false;
                items.Add( item );
            }
        }

        // unity callbacks ////////////////////////////////////////////////////
        protected virtual void OnEnable()
        {
            Initialize();
        }

        protected virtual void OnGUI()
        {
            btGui.OnGUI();
            GUILayout.BeginVertical( GUIStyles.darkbox );
                nspace = EditorGUILayout.TextField( "Namespace", nspace );
                importFrom = EditorGUILayout.TextField( "Import From", importFrom );
                templatePath = EditorGUILayout.TextField( "Import To", templatePath );
                if( GUILayout.Button( "Reload" ) )
                {
                    Initialize();
                }
            EditorGUILayout.EndVertical();

            GUILayout.BeginVertical(GUIStyles.darkbox);
                GUILayout.Label( "Templates" );
                foreach( TemplateItem item in items )
                {
                    item.enabled = EditorGUILayout.Toggle( item.info.Name, item.enabled );
                }
            GUILayout.EndVertical();

            
            if( GUILayout.Button( "Create Templates" ) )
            {
                if( nspace == System.String.Empty )
                {
                    Dbg.Error( "Please enter the namespace you are using" );
                }
                else
                {
                    foreach( TemplateItem item in items )
                    {
                        if( item.enabled )
                        {
                            FileUtility.CopyDirectory( item.info.FullName, 
                                                       Path.Combine( "Assets", Path.Combine ( templatePath, item.info.Name ) ),
                                                       ReplaceNamespace );
                        }
                    }
                }
            }
        }

        // private methods ////////////////////////////////////////////////////
        private void ReplaceNamespace( System.IO.FileInfo info )
        {
            if( System.IO.Path.GetExtension(info.FullName) == ".cs" )
            {
                FileUtility.FindAndReplaceTextFile( info.FullName, nspace, "blacktriangles.Template" );
            }
        }
    }
}
