//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;
using UnityEditor;

namespace blacktriangles
{
    public class EnvironmentProcessor
        : EditorWindow
    {
        //
        // constants //////////////////////////////////////////////////////////
        //

        private const string kMenuPath                          = "Tools/blacktriangles/Environment/Environment Postprocessor";
        private const string kTitle                             = "Environment Postprocessor";
        private const string kEditorOnlyTag                     = "EditorOnly";

        //
        // members ////////////////////////////////////////////////////////////
        //

        private GameObject enviro                               = null;
        private Material collisionMaterial                      = null;

        //
        // constructor / destructor ///////////////////////////////////////////
        //

        [MenuItem(kMenuPath)]
        private static void OpenWindow()
        {
            EnvironmentProcessor window = GetWindow<EnvironmentProcessor>();
            window.titleContent = new GUIContent( kTitle );
            window.Initialize();
        }
        
        //
        // --------------------------------------------------------------------
        //
        
        private void Initialize()
        {
        }

        //
        // static methods /////////////////////////////////////////////////////
        //
        
        static T ObjectField<T>(string label, T obj, bool allowSceneObjects = true)
            where T: Object
        {
            return EditorGUILayout.ObjectField(label, obj, typeof(T), allowSceneObjects) as T;
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        protected virtual void OnEnable()
        {
            Initialize();
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void OnGUI()
        {
            enviro = ObjectField<GameObject>("Environment", enviro);
            if(enviro != null)
            {
                DrawEnvironmentGUI();

                if(collisionMaterial != null
                    && GUILayout.Button("Run"))
                {
                    Run();
                }
            }
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void DrawEnvironmentGUI()
        {
            collisionMaterial = ObjectField<Material>("Collision Material", collisionMaterial, false);
        }

        //
        // --------------------------------------------------------------------
        //
        
        private void Run()
        {
            Renderer[] rends = enviro.GetComponentsInChildren<Renderer>();
            foreach(Renderer rend in rends)
            {
                Dbg.Log("Processing {0}", rend.name);
                if(rend.sharedMaterial == collisionMaterial)
                {
                    GenerateCollider(rend);
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        private void GenerateCollider(Renderer rend)
        {
            if(rend.gameObject.tag != kEditorOnlyTag)
            {
                Dbg.Log("Generating collider for {0}", rend.name);
                rend.gameObject.isStatic = true;
                rend.gameObject.AddComponent<MeshCollider>();
                DestroyImmediate(rend);
            }
        }
    }
}
