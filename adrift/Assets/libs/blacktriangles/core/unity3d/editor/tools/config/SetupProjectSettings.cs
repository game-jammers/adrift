//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEditor;

namespace blacktriangles.Editor
{
    public static class ProjectSettings
    {
        [MenuItem("Tools/blacktriangles/Config/Setup Project Settings")]
        public static void Configure()
        {
            FileUtility.ForEachFile(EditorConfig.kProjectSettingsDir, (System.IO.FileInfo info)=>{
                Dbg.Log(info.FullName);

                string dstFile = FileUtility.PathCombineSystemSeparator("ProjectSettings", info.Name);
                string dst = System.IO.Path.GetFullPath(dstFile);
                Dbg.Assert( System.IO.File.Exists( dst ), "Could not find " + dst + " ... are you running in a fully initialized Unity3D project?" );

                System.IO.File.Copy( dst, dst+".bak", true);
                System.IO.File.Copy( info.FullName, dst, true);
            });
        }
    }
}
