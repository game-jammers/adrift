//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
// Using modified bezier curve drawing code originally written by Linusmartensson
// http://forum.unity3d.com/threads/drawing-lines-in-the-editor.71979/
//
//=============================================================================

using UnityEngine;
using UnityEditor;

using System.Linq;

namespace blacktriangles
{
    public static class SpriteEditorUtility
    {
        // constants //////////////////////////////////////////////////////////
        private static readonly string kReadableMessage            = "Reimporting [{0}] to enable Read/Write access.  To get rid of this warning and speed up the process, consider enabling the flag manually in the import settings, or making a Read/Write enabled copy of the assets to use at editor time.  You should not need to leave this flag set in production, as it causes additional memory usage.";

        // public methods /////////////////////////////////////////////////////
        public static Sprite[] GetAllSpritesInTexture( Texture2D texture )
        {
            if( texture == null ) return null;
            string assetPath = AssetDatabase.GetAssetPath( texture );
            return AssetDatabase.LoadAllAssetsAtPath( assetPath ).OfType<Sprite>().ToArray();
        }

        public static Texture2D[] GetAllSpriteTexturesInTexture( Texture2D texture, out Sprite[] sprites )
        {
            bool didReimport = ForceTextureReadable( texture, true, System.String.Format( kReadableMessage, texture.name ) );

            sprites = GetAllSpritesInTexture( texture );
            Texture2D[] result = new Texture2D[ sprites.Length ];
            for( int i = 0; i < sprites.Length; ++i )
            {
                result[i] = GetTextureFromSprite( sprites[i] );
            }

            if( didReimport )
                ForceTextureReadable( texture, false, System.String.Empty );

            return result;
        }

        public static Texture2D GetTextureFromSprite( Sprite sprite )
        {
            Rect rect = sprite.rect;
            int x = rect.x.FloorInt();
            int y = rect.y.FloorInt();
            int width = rect.width.FloorInt();
            int height = rect.height.FloorInt();

            Texture2D result = new Texture2D( width, height );
            result.SetPixels( 0, 0, width, height, sprite.texture.GetPixels( x, y, width, height ) );
            result.Apply();

            return result;
        }

        // private methods ////////////////////////////////////////////////////
        private static bool ForceTextureReadable( Texture2D texture, bool readable, string msg )
        {
            bool result = false;
            string path = AssetDatabase.GetAssetPath( texture );
            TextureImporter importer = TextureImporter.GetAtPath( path ) as TextureImporter;
            if( importer.isReadable != readable )
            {
                if( msg.Length > 0 )
                    Debug.LogWarning( msg );
                importer.isReadable = readable;
                importer.SaveAndReimport();
                result = true;
            }

            return result;
        }
    }
}
