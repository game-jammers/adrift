//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
// Inspired by a Answers post by Bunny83
//    http://answers.unity3d.com/questions/393992/custom-inspector-multi-select-enum-dropdown.html
//=============================================================================

using UnityEngine;
using UnityEditor;
using System.Collections;

namespace blacktriangles
{
    public static partial class UnityEditorUtility
    {
        public static int DrawBitMaskField (Rect aPosition, int aMask, System.Type aType, GUIContent aLabel)
        {
            string[] names = System.Enum.GetNames(aType) as string[];
            return EditorGUI.MaskField(aPosition, aLabel, aMask, names);
        }
    }
}
