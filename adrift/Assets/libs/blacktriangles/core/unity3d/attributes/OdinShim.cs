//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

//#define USE_SHIM

using blacktriangles;
using UnityEngine;

#if USE_SHIM
namespace Sirenix.OdinInspector
{
    public class FoldoutGroupAttribute
        : SerializeFieldAttribute
    {
        public FoldoutGroupAttribute( string name )
        {
        }
    }
}
#endif
