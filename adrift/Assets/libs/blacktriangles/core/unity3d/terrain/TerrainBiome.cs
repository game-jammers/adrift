//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles
{
    [CreateAssetMenu(fileName="TerrainBiome", menuName="blacktriangles/terrain/Biome")]
    public class TerrainBiome
        : ScriptableObject
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        [System.Serializable]
        public struct Geometry
        {
            public PerlinNoise[] layers;
        }

        //
        // --------------------------------------------------------------------
        //

        [System.Serializable]
        public struct Detail
        {
            public GameObject prefab;
            public float density;
        }

        //
        // --------------------------------------------------------------------
        //

        [System.Serializable]
        public struct LayerVisual
        {
            public TerrainLayer texture;
            public Detail[] details;
        }

        //
        // --------------------------------------------------------------------
        //

        [System.Serializable]
        public struct LayerPass
        {
            public PerlinNoise noise;
            public float strength;
            public LayerVisual below;
            public LayerVisual above;
        }

        //
        // --------------------------------------------------------------------
        //

        [System.Serializable]
        public struct Layers
        {
            public int detailResolution;
            public LayerPass[] passes;
        }

        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Requirements")]
        public FloatRange tempurateRange;
        public FloatRange moistureRange;

        [Header("Geography")]
        public TerrainData preset;
        public Geometry geometry;
        public Layers layers;
        //public Details details;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public List<TerrainLayer> AllLayers()
        {
            var result = new List<TerrainLayer>();
            foreach(LayerPass pass in layers.passes)
            {
                result.UniqueInsert(pass.above.texture);
                result.UniqueInsert(pass.below.texture);
            }

            return result;
        }
    }
}
