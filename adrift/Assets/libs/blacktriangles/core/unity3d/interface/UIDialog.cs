//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace blacktriangles
{
    public class UIDialog
        : UIElement
    {
        // callbacks //////////////////////////////////////////////////////////
        public delegate void OpenCallback( UIDialog dialog, Request request );
        public delegate void CloseCallback( UIDialog dialog, Request request, Response response );

        public event OpenCallback OnOpen;
        public event CloseCallback OnClose;

        // type ///////////////////////////////////////////////////////////////
        [System.Serializable]
        public struct Request
        {
            public UITextType.Paragraph title;
            public UITextType.Paragraph[] body;
            public float fadeInTime;
            public float fadeOutTime;
            public bool autoComplete;
            public System.Action<UIDialog, Request, Response> callback;
        }

        public enum Response
        {
            Confirm,
            Cancel
        }

        // members ////////////////////////////////////////////////////////////
        public DialogManager manager                            { get; private set; }

        [Header("Dialog Elements")]
        public TextMeshProUGUI title                            = null;
        public TextMeshProUGUI body                             = null;
        public bool isActive                                    { get { return activeRequest.HasValue; } }
        public Request? activeRequest                           { get; private set; }
        public Response lastResponse                            { get; private set; }

        private bool isOpening                                  = false;
        private UnityEngine.Coroutine openCoroutine             = null;

        // constructor / initializer //////////////////////////////////////////
        public void Initialize( DialogManager _manager )
        {
            manager = _manager;

            if(manager != null)
            {
                OnOpen += manager.OnDialogOpen;
                OnClose += manager.OnDialogClose;
                activeRequest = null;
            }
        }

        // public methods /////////////////////////////////////////////////////
        public void Open( Request request )
        {
            StartCoroutine( OpenAsync(request) );
        }

        public IEnumerator OpenAsync(Request request)
        {
            isOpening = true;

            openCoroutine = StartCoroutine(OpenCoroutine(request));

            var wait = new WaitForSeconds(0.1f);
            while(isOpening)
                yield return wait;
        }

        public IEnumerator OpenCoroutine(Request request)
        {
            isOpening = true;
            Dbg.Assert( !isActive, "Attempting to Open a dialog that is already open" );
            Reset();
            activeRequest = request;
            if( OnOpen != null ) OnOpen(this, request);
            yield return ShowAsync( activeRequest.Value.fadeInTime );
            yield return UITextType.TypeParagraphAsync( title, activeRequest.Value.title );
            yield return UITextType.TypeParagraphsAsync( body, activeRequest.Value.body );

            if( request.autoComplete )
            {
                yield return CloseAsync( Response.Confirm );
            }
            
            isOpening = false;
        }

        public void Confirm()
        {
            if(isOpening)
            {
                StopCoroutine(openCoroutine);
                openCoroutine = null;
                Show();
                title.text = activeRequest.Value.title.text;

                body.text = "";
                foreach(UITextType.Paragraph para in activeRequest.Value.body)
                {
                    body.text += para.text;
                }
                isOpening = false;
            }
            else
            {
                Close( Response.Confirm );
            }
        }

        public void Cancel()
        {
            Close( Response.Cancel );
        }

        public void Close( Response response )
        {
            StartCoroutine( CloseAsync(response) );
        }

        public IEnumerator CloseAsync( Response response )
        {
            if( !isActive )
            {
                Hide(0.0f, null);
                yield break;
            }
            yield return HideAsync( activeRequest.Value.fadeOutTime );
            if( activeRequest.Value.callback != null ) activeRequest.Value.callback(this, activeRequest.Value, response );
            if( OnClose != null ) OnClose( this, activeRequest.Value, response );
            activeRequest = null;
            lastResponse = response;
        }

        // private methods ////////////////////////////////////////////////////
        private void Reset()
        {
            activeRequest = null;
            if(title != null)
            {
                title.text = System.String.Empty;
            }

            if(body != null)
            {
                body.text = System.String.Empty;
            }
        }
    }
}
 
