//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace blacktriangles
{
    public class UIWindow
        : UIElement
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public enum DragType
        {
            None,
            Move,
            ScaleX,
            ScaleY,
            ScaleXY
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        public UIDragArea[] dragAreas= null;
        public UIDragArea horizontalScale = null;
        public UIDragArea verticalScale = null;
        public UIDragArea cornerScale = null;
        public PointerEventData.InputButton dragButton = PointerEventData.InputButton.Left;
        public bool constrainToParent = true;
        public bool resizable        = true;
        public float maxOverflow     = 0.0f;

        public DragType dragType                                { get; private set; }
        
        //[SerializeField] private TextMeshProUGUI debugText      = null;

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected override void Awake()
        {
            base.Awake();
            foreach(UIDragArea dragArea in dragAreas)
            {
                dragArea.OnStartDrag += OnStartDrag;
                dragArea.OnUpdateDrag += OnUpdateDrag;
                dragArea.OnStopDrag += OnStopDrag;
            }

            if(horizontalScale != null)
            {
                horizontalScale.OnStartDrag += OnStartHorizontalScale;
                horizontalScale.OnUpdateDrag += OnUpdateHorizontalScale;
                horizontalScale.OnStopDrag += OnStopHorizontalScale;
            }


            if(verticalScale != null)
            {
                verticalScale.OnStartDrag += OnStartVerticalScale;
                verticalScale.OnUpdateDrag += OnUpdateVerticalScale;
                verticalScale.OnStopDrag += OnStopVerticalScale;
            }

            if(cornerScale != null)
            {
                cornerScale.OnStartDrag += OnStartCornerScale;
                cornerScale.OnUpdateDrag += OnUpdateCornerScale;
                cornerScale.OnStopDrag += OnStopCornerScale;
            }
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void OnStartDrag(PointerEventData ev)
        {
            if(ev.button == dragButton && dragType == DragType.None)
            {
                dragType = DragType.Move;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private void OnUpdateDrag(PointerEventData ev)
        {
            if(dragType == DragType.Move)
            {
                rectTransform.anchoredPosition += ev.delta;

                if(constrainToParent)
                {
                    ConstrainToParent(maxOverflow);
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private void OnStopDrag(PointerEventData ev)
        {
            if(ev.button == dragButton)
            {
                dragType = DragType.None;
            }
        }

        //
        // ----------------------------------------------------------------------------
        //
        
        private void OnStartHorizontalScale(PointerEventData ev)
        {
            if(resizable && ev.button == dragButton && dragType == DragType.None)
            {
                dragType = DragType.ScaleX;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private void OnUpdateHorizontalScale(PointerEventData ev)
        {
            if(dragType == DragType.ScaleX)
            {
                rectTransform.offsetMax = rectTransform.offsetMax + new Vector2(ev.delta.x, 0f);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private void OnStopHorizontalScale(PointerEventData ev)
        {
            if(ev.button == dragButton)
            {
                dragType = DragType.None;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private void OnStartVerticalScale(PointerEventData ev)
        {
            if(resizable && ev.button == dragButton && dragType == DragType.None)
            {
                dragType = DragType.ScaleY;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private void OnUpdateVerticalScale(PointerEventData ev)
        {
            if(dragType == DragType.ScaleY)
            {
                rectTransform.offsetMin = rectTransform.offsetMin + new Vector2(0f, ev.delta.y);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private void OnStopVerticalScale(PointerEventData ev)
        {
            if(ev.button == dragButton)
            {
                dragType = DragType.None;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private void OnStartCornerScale(PointerEventData ev)
        {
            if(resizable && ev.button == dragButton && dragType == DragType.None)
            {
                dragType = DragType.ScaleXY;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private void OnUpdateCornerScale(PointerEventData ev)
        {
            if(dragType == DragType.ScaleXY)
            {
                rectTransform.offsetMax = rectTransform.offsetMax + new Vector2(ev.delta.x, 0f);
                rectTransform.offsetMin = rectTransform.offsetMin + new Vector2(0f, ev.delta.y);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private void OnStopCornerScale(PointerEventData ev)
        {
            if(ev.button == dragButton)
            {
                dragType = DragType.None;
            }
        }

        //
        // end class //////////////////////////////////////////////////////////
        //
    }
}
 
