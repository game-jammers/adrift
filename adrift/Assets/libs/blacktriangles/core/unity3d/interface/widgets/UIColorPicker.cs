//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;
using UnityEngine.UI;

namespace blacktriangles
{
    public class UIColorPicker
        : UIElement
    {
        public UIElement preview;
        public Slider red;
        public Slider green;
        public Slider blue;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Refresh()
        {
            base.Refresh();

            preview.state.SetColor( new Color(
                red.value,
                green.value,
                blue.value
            ));

            return true;
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        protected virtual void Update()
        {
            Refresh();
        }
        
    }
}
