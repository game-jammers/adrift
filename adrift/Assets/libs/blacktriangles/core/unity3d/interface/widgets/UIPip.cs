//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;
using UnityEngine.UI;

namespace blacktriangles
{
    public class UIPip
        : UIElement
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Image filledImage;
        public Image unfilledImage;
        public bool filled;

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public override bool Refresh()
        {
            base.Refresh();

            filledImage.gameObject.SetActive(filled);

            return true;
        }
        
    }
}
