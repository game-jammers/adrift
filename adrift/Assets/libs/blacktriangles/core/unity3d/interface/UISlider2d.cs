//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace blacktriangles
{
    public class UISlider2d
        : UIElement
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public delegate void SliderChangedCallback(Vector2 pos);
        public event SliderChangedCallback OnSliderChanged;
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        public UIDragArea cursor;
        public UIElement fill;

        public Vector2 position                                 { get { return GetPosition(); }  set { SetPosition(value); }}
        public Vector2 fillPosition                             { get { return GetFill(); } set { SetFill(value); } }

        private bool dragging                                   = false;

        private Vector2 positionValue                           = Vector2.zero;
        private Vector2 fillValue                               = Vector2.zero;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void SetPosition(Vector2 val)
        {
            val.x = Mathf.Clamp(val.x, -1f, 1f);
            val.y = Mathf.Clamp(val.y, -1f, 1f);
            positionValue = val;

            Rect r = rectTransform.rect;
            cursor.rectTransform.anchoredPosition = new Vector2(
                (r.width/2f-cursor.rectTransform.rect.width/2f) * val.x,
                (r.height/2f-cursor.rectTransform.rect.height/2f) * val.y
            );

            NotifySliderChanged();
        }

        //
        // --------------------------------------------------------------------
        //

        public void SetFill(Vector2 val)
        {
            val.x = Mathf.Clamp(val.x, -1f, 1f);
            val.y = Mathf.Clamp(val.y, -1f, 1f);
            fillValue = val;

            Vector2 min = Vector2.zero;
            Vector2 max = Vector2.zero;

            Vector2 halfSize = new Vector2(rectTransform.rect.width, rectTransform.rect.height) / 2.0f;
            val = val * halfSize;

            if(val.x < 0f)
            {
                min.x = val.x;
                max.x = 0f;
            }
            else
            {
                min.x = 0f;
                max.x = val.x;
            }

            if(val.y < 0f)
            {
                min.y = val.y;
                max.y = 0f;
            }
            else
            {
                min.y = 0f;
                max.y = val.y;
            }

            if(Mathf.Abs(min.y - max.y) < 1f) max.y+=1f;
            if(Mathf.Abs(min.x - max.x) < 1f) max.x+=1f;

            fill.rectTransform.offsetMax = max;
            fill.rectTransform.offsetMin = min;
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        protected override void Awake()
        {
            base.Awake();
            cursor.OnStartDrag += OnStartDrag;
            cursor.OnUpdateDrag += OnUpdateDrag;
            cursor.OnStopDrag += OnStopDrag;
        }

        //
        // --------------------------------------------------------------------
        //
        
        protected virtual void OnGUI()
        {
            GUILayout.Label(position.ToString());
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void OnStartDrag(PointerEventData ev)
        {
            dragging = true;
        }

        //
        // --------------------------------------------------------------------
        //

        private void OnUpdateDrag(PointerEventData ev)
        {
            if(dragging)
            {
                Rect r = rectTransform.rect;
                Vector2 delta = new Vector2(ev.delta.x/(r.width/2f), ev.delta.y/(r.height/2f));
                SetPosition(position + delta);
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        private void OnStopDrag(PointerEventData ev)
        {
            dragging = false;
        }

        //
        // --------------------------------------------------------------------
        //

        private Vector2 GetPosition()
        {
            return positionValue;
        }
        
        //
        // --------------------------------------------------------------------
        //

        private Vector2 GetFill()
        {
            return fillValue;
        }

        //
        // --------------------------------------------------------------------
        //
        
        private void NotifySliderChanged()
        {
            if(OnSliderChanged != null)
            {
                OnSliderChanged(position);
            }
        }
    }
}
