//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using UnityEngine;
using System;
using System.Collections.Generic;

namespace blacktriangles 
{
    //
    // Base Serialized Dictionary /////////////////////////////////////////////
    //
    
    public class SerializedDictionary<Key, Value>
        : ISerializationCallbackReceiver
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        [ShowInInspector] public Dictionary<Key, Value> map     = new Dictionary<Key, Value>();

        [SerializeField, HideInInspector] private List<Key> _keys;
        [SerializeField, HideInInspector] private List<Value> _values;

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public void Copy(SerializedDictionary<Key, Value> from)
        {
            map = new Dictionary<Key, Value>(from.map);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void ForEach(System.Action<Key, Value> action)
        {
            foreach(KeyValuePair<Key,Value> pair in map)
            {
                action(pair.Key, pair.Value);
            }
        }

        //
        // ISerializationCallbackReceiver /////////////////////////////////////
        //
        
        public void OnBeforeSerialize()
        {
            _keys = new List<Key>(map.Keys);
            _values = new List<Value>(map.Values);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void OnAfterDeserialize()
        {
            map = new Dictionary<Key, Value>();
            if(_keys != null)
            {
                for(int i = 0; i < _keys.Count; ++i)
                {
                    map[_keys[i]] = _values[i];
                }
            }
        }
    }

    //
    // Serialized Dictionary Default //////////////////////////////////////////
    //
    
    public class SerializedDictionaryDefault<Key, Value>
        : SerializedDictionary<Key, Value>
    {
        public Value this[Key key]
        {
            get
            {
                if(map.ContainsKey(key))
                {
                    return map[key];
                }

                return default(Value);
            }

            set
            {
                map[key] = value;
            }
        }
    }

}

