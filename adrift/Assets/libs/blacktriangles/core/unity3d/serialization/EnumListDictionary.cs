//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using System.Collections;
using System.Collections.Generic;

namespace blacktriangles
{
    public class EnumListDictionary<EnumType, Type, KeyType>
        : EnumList<EnumType, Dictionary<KeyType, Type>>
        , IEnumerable<KeyValuePair<KeyType, Type>>
        where EnumType : System.Enum
    {
        //
        // constructor ////////////////////////////////////////////////////////
        //
        
        public EnumListDictionary()
        {
            EnumType[] types = EnumUtility.GetValues<EnumType>();
            foreach(EnumType type in types)
            {
                this[type] = new Dictionary<KeyType, Type>();
            }
        }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public Type GetValue(EnumType etype, KeyType ktype)
        {
            return this[etype][ktype];
        }

        //
        // --------------------------------------------------------------------
        //

        public void SetValue(EnumType etype, KeyType ktype, Type val)
        {
            this[etype][ktype] = val;
        }

        //
        // --------------------------------------------------------------------
        //

        public new IEnumerator<KeyValuePair<KeyType,Type>> GetEnumerator()
        {
            foreach(Dictionary<KeyType, Type> dict in items)
            {
                if(dict != null)
                {
                    foreach(var pair in dict)
                    {
                        yield return pair;
                    }
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
