//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace blacktriangles
{
    //
    // ########################################################################
    //
    
    [System.Serializable]
    public class BaseEnumList
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public struct BaseEntry
        {
            public int key;
            public object item;
        }

        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public virtual System.Type enumType                     { get { return null; } }
        public virtual System.Type valueType                    { get { return null; } }
        public virtual int count                                { get { return 0; } }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public virtual List<BaseEntry> GetBaseEntries()
        {
            return null;
        }

        //
        // --------------------------------------------------------------------
        //

        public virtual void SetBaseEntry(int idx, object item)
        {
        }
        
    }

    //
    // ########################################################################
    //

    [System.Serializable]
    public class EnumListNoSerde<EnumType, T>
        : BaseEnumList
        , IEnumerable<T>
        where EnumType: System.Enum
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        public class Entry
        {
            public EnumType slot;
            public T item;
        };
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        public override System.Type enumType                    { get { return typeof(EnumType); } }
        public override System.Type valueType                   { get { return typeof(T); } }

        public T first                                          { get { return count <= 0 ? default(T) : items[0]; } }
        public override int count                               { get { return items.Length; } }

        //[ShowInInspector]
        protected T[] items                                     = new T[EnumUtility.Count<EnumType>()];
                
        //
        // operators //////////////////////////////////////////////////////////
        //

        public override List<BaseEntry> GetBaseEntries()
        {
            var result = new List<BaseEntry>();
            ForEach((e,t)=>{
                result.Add(new BaseEntry() {
                    key = System.Convert.ToInt32(e),
                    item = (object)t,
                });
            });

            return result;
        }

        //
        // --------------------------------------------------------------------
        //
        
        public override void SetBaseEntry(int idx, object item)
        {
            Dbg.Assert(items.IsValidIndex(idx), "Trying to set an invalid index");
            Dbg.Assert(item == null || item.GetType() == typeof(T), "Trying to set an invalid object");
            items[idx] = (T)item;
        }

        //
        // --------------------------------------------------------------------
        //

        public T Random()
        {
            return items.Random();
        }

        //
        // --------------------------------------------------------------------
        //

        public T Random(System.Random rnd)
        {
            return items.Random(rnd);
        }

        //
        // --------------------------------------------------------------------
        //

        public List<T> ToList()
        {
            return new List<T>(items);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public T[] ToArray()
        {
            return items.Clone() as T[];
        }

        //
        // --------------------------------------------------------------------
        //
        
        public U[] ToArray<U>(System.Func<T, U> convert)
        {
            U[] result = new U[items.Length];
            for(int i = 0; i < items.Length; ++i)
            {
                result[i] = convert(items[i]);
            }

            return result;
        }

        //
        // --------------------------------------------------------------------
        //

        public Dictionary<EnumType, T> ToDictionary()
        {
            var result = new Dictionary<EnumType,T>();
            ForEach((e,t)=>{
                result[e] = t;
            });

            return result;
        }

        //
        // --------------------------------------------------------------------
        //
        

        public T this[EnumType e]
        {
            get { return items[e.ToInt32()]; }
            set { items[e.ToInt32()] = value; }
        }

        //
        // --------------------------------------------------------------------
        //

        public T this[int idx]
        {
            get { return items[idx]; }
        } 

        //
        // --------------------------------------------------------------------
        //
        
        public void ForEach(System.Action<EnumType, T> cb)
        {
            for(int i = 0; i < items.Length; ++i)
            {
                EnumType t = EnumUtility.Convert<EnumType>(i);
                cb(t, items[i]);
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public Entry First(System.Func<EnumType, T, bool> pred)
        {
            for(int i = 0; i < items.Length; ++i)
            {
                EnumType key = EnumUtility.Convert<EnumType>(i);
                T item = items[i];
                if(pred(key,item))
                {
                    return new Entry() {
                        slot = key,
                        item = item,
                    };
                }
            }

            return null;
        }

        //
        // --------------------------------------------------------------------
        //
        

        public IEnumerator<T> GetEnumerator()
        {
            foreach(T t in items)
            {
                yield return t;
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }

    //
    // ########################################################################
    //
    
    [System.Serializable]
    public class EnumList<EnumType, T>
        : EnumListNoSerde<EnumType, T>
        , ISerializationCallbackReceiver
        where EnumType: System.Enum
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [SerializeField, HideInInspector] private List<EnumType> _keys;
        [SerializeField, HideInInspector] private List<T> _values;
        
        //
        // ISerializationCallbackReceiver /////////////////////////////////////
        //
        
        public virtual void OnBeforeSerialize()
        {
            _keys = new List<EnumType>();
            _values = new List<T>();

            for(int i = 0; i < items.Length; ++i)
            {
                EnumType e = EnumUtility.Convert<EnumType>(i);
                _keys.Add(e);
                _values.Add(items[i]);
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        [ContextMenu("Refresh")]
        public virtual void OnAfterDeserialize()
        {
            items = new T[EnumUtility.Count<EnumType>()];
            for(int i = 0; i < _keys.Count; ++i)
            {
                this[_keys[i]] = _values[i];
            }
        }
    }
}
