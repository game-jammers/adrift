//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

namespace blacktriangles.Graph
{
    public class DebugNode
        : StateNode
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public object input;

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public override void OnUpdate(float dt)
        {
            if(input == null)
            {
                Dbg.Log("<null>");
            }
            else
            {
                Dbg.Log(input.ToString());
            }

            Complete();
        }

        //
        // ------------------------------------------------------------------------
        //
        
        protected override void AddDynamicPorts()
        {
            base.AddDynamicPorts();
            AddInput(typeof(object), "input");
        }
    }
}
