//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using blacktriangles.Graph;
using UnityEngine;
using XNode;

namespace blacktriangles.Graph
{
    public class WaitForSignal
        : StateNode
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Input] public bool ready                               = false;
        
        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public override void OnUpdate(float dt)
        {
            if(ready)
            {
                ready = false;
                Complete();
            }
        }
    }
}
