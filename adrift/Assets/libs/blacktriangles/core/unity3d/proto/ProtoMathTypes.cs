//
// (c) BLACKTRIANGLES 2020
// http://www.blacktriangles.com
//

using blacktriangles;
using ProtoBuf;
using UnityEngine;

namespace blacktriangles
{
    [ProtoContract]
    public struct ProtoVec3
    {
        [ProtoMember(1)] public float x;
        [ProtoMember(2)] public float y;
        [ProtoMember(3)] public float z;

        public static implicit operator Vector3(ProtoVec3 rhs)
        {
            return new Vector3(rhs.x, rhs.y, rhs.z);
        }

        public static implicit operator ProtoVec3(Vector3 rhs)
        {
            return new ProtoVec3() {
                x = rhs.x, 
                y = rhs.y, 
                z = rhs.z,
            };
        }
    }

    //
    // ------------------------------------------------------------------------
    //
    
    [ProtoContract]
    public struct ProtoVec2
    {
        [ProtoMember(1)] public float x;
        [ProtoMember(2)] public float y;

        public static implicit operator Vector2(ProtoVec2 rhs)
        {
            return new Vector2(rhs.x, rhs.y);
        }

        public static implicit operator ProtoVec2(Vector2 rhs)
        {
            return new ProtoVec2() {
                x = rhs.x,
                y = rhs.y
            };
        }
    }

    //
    // ------------------------------------------------------------------------
    //

    [ProtoContract]
    public struct ProtoIntVec2
    {
        [ProtoMember(1)] public int x;
        [ProtoMember(2)] public int y;

        public static implicit operator Vector2Int(ProtoIntVec2 rhs)
        {
            return new Vector2Int(rhs.x, rhs.y);
        }

        public static implicit operator ProtoIntVec2(Vector2Int rhs)
        {
            return new ProtoIntVec2() {
                x = rhs.x,
                y = rhs.y
            };
        }
    }

    //
    // ------------------------------------------------------------------------
    //
    
    [ProtoContract]
    public struct ProtoQuat
    {
        [ProtoMember(1)] public float x;
        [ProtoMember(2)] public float y;
        [ProtoMember(3)] public float z;
        [ProtoMember(4)] public float w;

        public static implicit operator Quaternion(ProtoQuat rhs)
        {
            return new Quaternion(rhs.x, rhs.y, rhs.z, rhs.w);
        }

        public static implicit operator ProtoQuat(Quaternion rhs)
        {
            return new ProtoQuat() {
                x = rhs.x, 
                y = rhs.y, 
                z = rhs.z,
                w = rhs.w
            };
        }
    }
}
