# Developer Notes

## Todo
- Networking2
    - PacketHeader should have magic number to ensure that we are not reading partial packets.
    - ConnectionState enum rather than isActive / isConnected etc
    - ActionPacket
    - Finish unit tests for
        - RawPacket
        - TcpClient
        - TcpServer
