//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using System.Collections;
using UnityEngine;

namespace blacktriangles.Testing
{
    public static partial class Utils
    {
        //
        // Waits for a condition, or until a timeout occurs, triggers a warning
        // if it was the timeout.
        //

        public static IEnumerator WaitOrTimeout(System.Func<bool> check, double timeout)
        {
            double endtime = Epoch.now + timeout;
            while(!check() && endtime > Epoch.now) 
            {
                yield return new WaitForSeconds(0.1f);
            }

            if(!check())
            {
                Dbg.Warn("WaitOrTimeout timed out");
            }
        }
    }
}
