//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestTimeStamppedValue
    {
        // tests ///////////////////////////////////////////////////////////////
        [Test]
        public void BasicTest()
        {
            TimestampedValue<int> stampedInt = new TimestampedValue<int>();

            // newly created timestamped values have their timestamp and access stamp
            // set to the time of creation so they will be equal.
            double timestamp = stampedInt.timestamp;
            double accessTimestamp = stampedInt.accessTimestamp;
            Assert.That( accessTimestamp.IsApproximately( timestamp ) );

            // you can test for equivalency in both directions both against the
            // internal .val or the timestamped value itself through use of
            // implicit casting.
            Assert.That( stampedInt.val == 0 );
            Assert.That( stampedInt == 0 );
            Assert.That( 0 == stampedInt );

            // similarly you can set the .val or call Set
            stampedInt = new TimestampedValue<int>(10);
            Assert.That( stampedInt.val == 10 );
            Assert.That( stampedInt == 10 );
            Assert.That( 10 == stampedInt );

            stampedInt.Set(20);
            Assert.That( stampedInt.val == 20 );
            Assert.That( stampedInt == 20 );
            Assert.That( 20 == stampedInt );

            stampedInt.val = 30;
            Assert.That( stampedInt.val == 30 );
            Assert.That( stampedInt == 30 );
            Assert.That( 30 == stampedInt );
        }
    }
}
