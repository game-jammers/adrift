//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using blacktriangles.Network;
using NUnit.Framework;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestAttributeFactory
    {
        //
        // Test ///////////////////////////////////////////////////////////////
        //

        [Test]
        public void Produce()
        {
            PacketAttribute targetAttrib = Packet.GetPacketAttribute(typeof(UTF8TextPacket));
            UTF8TextPacket packet = AttributeFactory<PacketAttribute>.Produce<Packet>( (entry)=>{
                Assert.AreEqual(entry.attribute.GetType(), typeof(PacketAttribute));
                PacketAttribute attrib = entry.attribute as PacketAttribute;
                if(attrib.id == targetAttrib.id)
                {
                    Assert.AreEqual(entry.type, typeof(UTF8TextPacket));
                    return System.Activator.CreateInstance(entry.type) as Packet;
                }

                return null;
            }) as UTF8TextPacket;

            Assert.That(packet != null);
        }

        //
        // End Class //////////////////////////////////////////////////////////
        //
    }
}
