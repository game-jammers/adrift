//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using blacktriangles.Network;
using NUnit.Framework;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestPacketFactory
    {
        //
        // Test ///////////////////////////////////////////////////////////////
        //

        [Test]
        public void Create()
        {
            int id = (int)ReservedPacketType.UTF8TextPacket;
            UTF8TextPacket packet = PacketFactory.Create(id) as UTF8TextPacket;
            Assert.Less(0.1, Epoch.now - packet.timestamp);
            Assert.That(packet != null);
        }

        //
        // End Class //////////////////////////////////////////////////////////
        //
    }
}
