//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using blacktriangles.Network;
using NUnit.Framework;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestRawPacket
    {
        //
        // tests //////////////////////////////////////////////////////////////
        //

        [Test]
        public void ConstructFromNetworkBuffer()
        {
            // lets create a payload with a string //
            byte[] payload = System.Text.Encoding.ASCII.GetBytes("Hello World");
            RawPacket packet = new RawPacket(0x10, payload);
            string str = System.Text.Encoding.ASCII.GetString(packet.payload);

            // verify that everything we get back out looks legit
            Assert.AreEqual(packet.header.id, 0x10);
            Assert.AreEqual(Packet.kHeaderSize, sizeof(System.Int32) + sizeof(System.Double) + sizeof(System.Int32));
            Assert.AreEqual(packet.header.payloadSize, payload.Length);
            Assert.AreEqual("Hello World", str);
        }

        //
        // --------------------------------------------------------------------
        //

        [Test]
        public void ConstructFromBinaryReader()
        {
            // Create a payload and packet again
            byte[] payload = System.Text.Encoding.ASCII.GetBytes("This is a test");
            RawPacket packet = new RawPacket(0x10, payload);

            // make sure our raw packet got formed properly
            Assert.AreEqual(packet.header.id, 0x10);
            Assert.AreEqual(packet.header.payloadSize, payload.Length);
            Assert.AreEqual(packet.header.payloadSize, packet.payload.Length);

            // we check that this tobytes function operates as expected below
            byte[] packetBytes = packet.ToBytes();

            // create a reader with the bytes of the raw packet we just created
            BinaryReader reader = new BinaryReader(new MemoryStream(packetBytes));

            // create a new packet using this reader
            RawPacket newPacket = new RawPacket(reader);

            // double check that all the values check out in the new packet
            Assert.AreEqual(newPacket.header.id, 0x10);
            Assert.AreEqual(newPacket.header.payloadSize, payload.Length);
            string payloadStr = System.Text.Encoding.ASCII.GetString(newPacket.payload);
            Assert.AreEqual(payloadStr, "This is a test");
        }

        //
        // --------------------------------------------------------------------
        //

        [Test]
        public void WriteToStream()
        {
        }

        //
        // --------------------------------------------------------------------
        //

        /*
        [Test]
        public void ToBytes()
        {
            // lets create a payload with a string //
            byte[] payload = System.Text.Encoding.ASCII.GetBytes("Hello World");
            NetworkBuffer buffer = new NetworkBuffer(payload.Length);
            buffer.PutBytes(payload);
            RawPacket packet = new RawPacket(0x10, buffer);

            byte[] bytes = packet.ToBytes();
            
            int offset = 0;
            // lets read this memory mapped style

            int id = System.BitConverter.ToInt32(bytes, offset);
            Assert.AreEqual(id, 0x10);
            offset += sizeof( System.Int32 );

            int payloadSize = System.BitConverter.ToInt32(bytes, offset);
            Assert.AreEqual(payloadSize, payload.Length);
            offset += sizeof( System.Int32 );

            string payloadStr = System.Text.Encoding.ASCII.GetString(bytes,offset,packet.header.payloadSize);
            Assert.AreEqual(payloadStr, "Hello World");
        }

        //
        // --------------------------------------------------------------------
        //

        [Test]
        public void ReadFromStream()
        {
        }
        */

        //
        // end class //////////////////////////////////////////////////////////
        //
    }
}
 
