//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestFloatExtension
    {
        // tests ///////////////////////////////////////////////////////////////
        [Test]
        public void BasicTest()
        {
            float f = 0f;
            Assert.That( f.IsNearZero() );
            Assert.That( f.IsApproximately( 10f ) == false );

            f = 10f;
            Assert.That( f.IsNearZero() == false );
            Assert.That( f.IsApproximately( 10f ) );
            Assert.That( f.IsApproximately( 100f / 10f ) );
            Assert.That( f.IsApproximately( 100f ) == false );
        }
    }
}
