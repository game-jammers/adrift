//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestIntExtension
    {
        //
        // types //////////////////////////////////////////////////////////////
        //
        
        private enum FlagA
        {
            Zero,
            One,
            Two
        }


        //
        // tests ///////////////////////////////////////////////////////////////
        //

        [Test]
        public void IsFlagSetTest()
        {
            int flags = 0;
            Assert.AreEqual(flags.IsFlagSet(FlagA.Zero), false);
            Assert.AreEqual(flags.IsFlagSet(FlagA.One), false);
            Assert.AreEqual(flags.IsFlagSet(FlagA.Two), false);

            flags = 1;
            Assert.AreEqual(flags.IsFlagSet(FlagA.Zero), true);
            Assert.AreEqual(flags.IsFlagSet(FlagA.One), false);
            Assert.AreEqual(flags.IsFlagSet(FlagA.Two), false);

            flags = 2;
            Assert.AreEqual(flags.IsFlagSet(FlagA.Zero), false);
            Assert.AreEqual(flags.IsFlagSet(FlagA.One), true);
            Assert.AreEqual(flags.IsFlagSet(FlagA.Two), false);

            flags = 3;
            Assert.AreEqual(flags.IsFlagSet(FlagA.Zero), true);
            Assert.AreEqual(flags.IsFlagSet(FlagA.One), true);
            Assert.AreEqual(flags.IsFlagSet(FlagA.Two), false);

            flags = 4;
            Assert.AreEqual(flags.IsFlagSet(FlagA.Zero), false);
            Assert.AreEqual(flags.IsFlagSet(FlagA.One), false);
            Assert.AreEqual(flags.IsFlagSet(FlagA.Two), true);


            flags = 5;
            Assert.AreEqual(flags.IsFlagSet(FlagA.Zero), true);
            Assert.AreEqual(flags.IsFlagSet(FlagA.One), false);
            Assert.AreEqual(flags.IsFlagSet(FlagA.Two), true);

            flags = 6;
            Assert.AreEqual(flags.IsFlagSet(FlagA.Zero), false);
            Assert.AreEqual(flags.IsFlagSet(FlagA.One), true);
            Assert.AreEqual(flags.IsFlagSet(FlagA.Two), true);

            flags = 7;
            Assert.AreEqual(flags.IsFlagSet(FlagA.Zero), true);
            Assert.AreEqual(flags.IsFlagSet(FlagA.One), true);
            Assert.AreEqual(flags.IsFlagSet(FlagA.Two), true);

            flags = 8;
            Assert.AreEqual(flags.IsFlagSet(FlagA.Zero), false);
            Assert.AreEqual(flags.IsFlagSet(FlagA.One), false);
            Assert.AreEqual(flags.IsFlagSet(FlagA.Two), false);

            flags = 9;
            Assert.AreEqual(flags.IsFlagSet(FlagA.Zero), true);
            Assert.AreEqual(flags.IsFlagSet(FlagA.One), false);
            Assert.AreEqual(flags.IsFlagSet(FlagA.Two), false);
        }
    }
}
