//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestDoubleExtension
    {
        // tests ///////////////////////////////////////////////////////////////
        [Test]
        public void BasicTest()
        {
            double d = 0;
            Assert.That( d.IsNearZero() );
            Assert.That( d.IsApproximately( 10 ) == false );

            d = 10;
            Assert.That( d.IsNearZero() == false );
            Assert.That( d.IsApproximately( 10 ) );
            Assert.That( d.IsApproximately( 100 / 10 ) );
        }
    }
}
