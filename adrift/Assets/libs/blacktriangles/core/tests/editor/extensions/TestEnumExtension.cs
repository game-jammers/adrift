//
// (c) BLACKTRIANGLES 2019
// http://www.blacktriangles.com
//

using blacktriangles;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestEnumExtension
    {
        //
        // types //////////////////////////////////////////////////////////////
        //
        
        private enum FlagA
        {
            Zero,
            One,
            Two
        }

        //
        // tests ///////////////////////////////////////////////////////////////
        //

        [Test]
        public void ToFlagTest()
        {
            FlagA zero = FlagA.Zero;
            Assert.AreEqual((int)zero, 0);
            Assert.AreEqual(zero.ToFlag(), 1);

            FlagA one = FlagA.One;
            Assert.AreEqual((int)one, 1);
            Assert.AreEqual(one.ToFlag(), 2);

            FlagA two = FlagA.Two;
            Assert.AreEqual((int)two, 2);
            Assert.AreEqual(two.ToFlag(), 4);
        }

        [Test]
        public void IsInFlagsTest()
        {
            int flags = 0;

            FlagA zero = FlagA.Zero;
            FlagA one = FlagA.One;
            FlagA two = FlagA.Two;

            Assert.AreEqual(zero.IsInFlags(flags), false);
            Assert.AreEqual(one.IsInFlags(flags), false);
            Assert.AreEqual(two.IsInFlags(flags), false);

            flags = 1;
            Assert.AreEqual(zero.IsInFlags(flags), true);
            Assert.AreEqual(one.IsInFlags(flags), false);
            Assert.AreEqual(two.IsInFlags(flags), false);

            flags = 2;
            Assert.AreEqual(zero.IsInFlags(flags), false);
            Assert.AreEqual(one.IsInFlags(flags), true);
            Assert.AreEqual(two.IsInFlags(flags), false);

            flags = 3;
            Assert.AreEqual(zero.IsInFlags(flags), true);
            Assert.AreEqual(one.IsInFlags(flags), true);
            Assert.AreEqual(two.IsInFlags(flags), false);

            flags = 4;
            Assert.AreEqual(zero.IsInFlags(flags), false);
            Assert.AreEqual(one.IsInFlags(flags), false);
            Assert.AreEqual(two.IsInFlags(flags), true);


            flags = 5;
            Assert.AreEqual(zero.IsInFlags(flags), true);
            Assert.AreEqual(one.IsInFlags(flags), false);
            Assert.AreEqual(two.IsInFlags(flags), true);

            flags = 6;
            Assert.AreEqual(zero.IsInFlags(flags), false);
            Assert.AreEqual(one.IsInFlags(flags), true);
            Assert.AreEqual(two.IsInFlags(flags), true);

            flags = 7;
            Assert.AreEqual(zero.IsInFlags(flags), true);
            Assert.AreEqual(one.IsInFlags(flags), true);
            Assert.AreEqual(two.IsInFlags(flags), true);

            flags = 8;
            Assert.AreEqual(zero.IsInFlags(flags), false);
            Assert.AreEqual(one.IsInFlags(flags), false);
            Assert.AreEqual(two.IsInFlags(flags), false);

            flags = 9;
            Assert.AreEqual(zero.IsInFlags(flags), true);
            Assert.AreEqual(one.IsInFlags(flags), false);
            Assert.AreEqual(two.IsInFlags(flags), false);
        }
    }
}
