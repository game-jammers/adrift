//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using blacktriangles.Network;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.TestTools;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestTcp
    {
        //
        // constants //////////////////////////////////////////////////////////
        //

        private static readonly IPAddress kHostName             = IPAddress.Parse("127.0.0.1");
        private static readonly int kHostPort                   = 30000;

        private TcpListener server                                = null;
        private TcpConnection conn                              = null;
        private TcpConnection conn2                             = null;

        //
        // lifecycle //////////////////////////////////////////////////////////
        //

        [TearDown]
        public void TearDown()
        {
            if(server != null)
            {
                server.Shutdown();
                server = null;
            }

            if(conn != null)
            {
                conn.Disconnect();
                conn = null;
            }

            if(conn2 != null)
            {
                conn2.Disconnect();
                conn2 = null;
            }
        }

        //
        // tests //////////////////////////////////////////////////////////////
        //

        [UnityTest]
        public IEnumerator ConnectTest()
        {
            ConnectionConfig config = new ConnectionConfig();
            config.localEndpoint = config.remoteEndpoint = new IPEndPoint(kHostName, kHostPort);

            //
            // spin up a server
            //
            server = new TcpListener();
            server.OnNetworkError += OnServerNetErr;
            server.Listen(config);

            yield return Utils.WaitOrTimeout(()=>{
                return server.isActive;
            }, 1.0);
            Assert.That(server.isActive);

            //
            // spin up a client
            //
            conn = new TcpConnection();
            conn.OnNetworkError += OnClientNetErr;
            conn.Connect(config);

            yield return Utils.WaitOrTimeout(()=>{
                server.Update();
                return conn.isConnected;
            }, 5.0);
            Assert.That(conn.isConnected);
            Assert.AreEqual(1, server.connections.Length);

            //
            // send a message from the client
            //
            Dbg.Log("Sending...");
            UTF8TextPacket tx0 = new UTF8TextPacket("PING");
            conn.Send(tx0);

            yield return new WaitForSeconds(1f);

            //
            // receive on the server
            //
            List<RawPacket> rxs0 = server.TakePackets();
            Assert.AreEqual(rxs0.Count, 1);
            UTF8TextPacket rx0 = new UTF8TextPacket("UNDEFINED");
            RawPacket rxRaw0 = rxs0[0];
            Assert.AreEqual(rxRaw0.receiver, server.connections[0]);
            Assert.AreEqual(rxRaw0.senderEndpoint, conn.socket.LocalEndPoint);
            Assert.AreEqual(rxRaw0.senderEndpoint, server.connections[0].config.Value.remoteEndpoint);
            rx0.Decode(rxRaw0);
            Assert.AreEqual(tx0.text, rx0.text);

            //
            // second connection
            //
            conn2 = new TcpConnection();
            conn2.OnNetworkError += OnClientNetErr;
            conn2.Connect(config);

            yield return Utils.WaitOrTimeout(()=>{
                server.Update();
                return conn2.isConnected;
            }, 1.0);
            Assert.That(conn2.isConnected);

            // wait half a second for the server to add the connection
            yield return new WaitForSeconds(0.5f);

            Assert.AreEqual(server.connections.Length, 2);

            //
            // broadcast from server
            //
            UTF8TextPacket tx1 = new UTF8TextPacket("PONG");
            server.Broadcast(tx1);

            yield return new WaitForSeconds(0.5f);

            //
            // receive on connection 1
            //
            List<RawPacket> rxs1 = conn.TakePackets();
            Assert.AreEqual(rxs1.Count, 1);
            UTF8TextPacket rx1 = new UTF8TextPacket("UNDEFINED");
            RawPacket rxRaw1 = rxs1[0];
            Assert.AreEqual(rxRaw1.receiver, conn);
            Assert.AreEqual(rxRaw1.senderEndpoint, conn.config.Value.remoteEndpoint);
            rx1.Decode(rxRaw1);
            Assert.AreEqual(rx1.text, tx1.text);

            //
            // receive on connection 2
            //
            rxs1 = null;
            rxs1 = conn2.TakePackets();
            Assert.AreEqual(rxs1.Count, 1);
            rx1 = new UTF8TextPacket("UNDEFINED");
            rx1.Decode(rxs1[0]);
            Assert.AreEqual(rx1.text, tx1.text);

            //
            // dispose of everything
            //
            conn.Disconnect();
            conn2.Disconnect();
            server.Shutdown();

            //
            // give it some time
            //
            yield return Utils.WaitOrTimeout(()=>{
                return !(server.isActive||conn.isActive||conn2.isActive);
            }, ConnectionConfig.kThreadJoinTimeout / 1000.0f);
        }

        //
        // callbacks //////////////////////////////////////////////////////////
        //

        void OnServerNetErr(NetworkError err)
        {
            Assert.That(false);
        }

        //
        // --------------------------------------------------------------------
        //

        void OnClientNetErr(NetworkError err)
        {
            Assert.That(false);
        }
    }
}
 
