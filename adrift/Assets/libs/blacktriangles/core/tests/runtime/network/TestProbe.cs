//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using blacktriangles.Network;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.TestTools;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestProbe
    {
        //
        // constants //////////////////////////////////////////////////////////
        //

        private Beacon beacon                                   = null;
        private Beacon beacon2                                  = null;
        private Probe probe                                     = null;

        //
        // lifecycle //////////////////////////////////////////////////////////
        //

        [TearDown]
        public void TearDown()
        {
            if(beacon != null)
            {
                beacon.Stop();
                beacon = null;
            }

            if(beacon2 != null)
            {
                beacon2.Stop();
                beacon2 = null;
            }

            if(probe != null)
            {
                probe.Stop();
                probe = null;
            }
        }

        //
        // tests //////////////////////////////////////////////////////////////
        //

        [UnityTest]
        public IEnumerator ConnectTest()
        {
            //
            // this is the data that we are accepting connections on as well
            // as the session type we are opening receiving.  This is just a
            // useful byte that we can use to filter what kinds of rooms to find.
            //
            
            DiscoveryData data = new DiscoveryData();
            data.sessionType = 0x10;
            data.address = "127.0.0.1";
            data.port = 1337;

            //
            // we start a local beacon, which is just a udp socket that we
            // listen to probe requests on.
            //

            beacon = new Beacon();
            beacon.Start(data);

            //
            // now we make a probe object, which regularly pings, looking for
            // beacons.  Note the session type we pass in here has no actual
            // significance, except that it should match the one we are signalling
            // with our beacon above.
            //

            probe = new Probe();
            probe.Start(0x10);
            Assert.AreEqual(probe.sessionType, 0x10);

            //
            // lets wait for a probe to be sent out and a response to be
            // returned
            //

            yield return Utils.WaitOrTimeout(()=>{
                probe.Update();
                return 1 == probe.discovered.Length;
            }, 5f);

            //
            // now lets check our 'discovered' list, which should contain only
            // one discovered beacon
            //

            {
                Assert.AreEqual(1, probe.discovered.Length);
                Probe.Discovered disc = probe.discovered[0];
                Assert.LessOrEqual(disc.staleCount, 1);
                Assert.AreEqual(0x10, disc.res.data.sessionType);
                Assert.AreEqual(data.address, disc.res.data.address);
                Assert.AreEqual(data.port, disc.res.data.port);
            }

            //
            // lets wait a few seconds, our staleCount should never go higher
            // than 1.
            //

            yield return new WaitForSeconds(2.0f);

            //
            // should still look good!
            //

            {
                Assert.AreEqual(1, probe.discovered.Length);
                Probe.Discovered disc = probe.discovered[0];
                Assert.LessOrEqual(disc.staleCount, 1);
                Assert.AreEqual(0x10, disc.res.data.sessionType);
                Assert.AreEqual(data.address, disc.res.data.address);
                Assert.AreEqual(data.port, disc.res.data.port);
            }


            //
            // now lets kill our beacon, our discovered item should slowly
            // grow stale
            //

            beacon.Stop();

            yield return new WaitForSeconds(1.0f);

            {
                Assert.AreEqual(1, probe.discovered.Length);
                Probe.Discovered disc = probe.discovered[0];
                Assert.LessOrEqual(disc.staleCount, 2);
                Assert.AreEqual(0x10, disc.res.data.sessionType);
                Assert.AreEqual(data.address, disc.res.data.address);
                Assert.AreEqual(data.port, disc.res.data.port);
            }

            yield return Utils.WaitOrTimeout(()=>{
                probe.Update();
                return probe.discovered.Length == 0;
            }, 10f);

            Assert.AreEqual(0, probe.discovered.Length);


            //
            // lets start up our beacon again...
            //

            beacon.Start(data);

            //
            // now lets simulate a second beacon, sending out a different
            // address and port availability
            //

            DiscoveryData data2 = new DiscoveryData();
            data2.sessionType = 0x10;
            data2.address = "127.0.0.2";
            data2.port = 1234;

            beacon2 = new Beacon();
            beacon2.Start(data2);

            yield return Utils.WaitOrTimeout(()=>{
                probe.Update();
                return probe.discovered.Length == 2;
            }, 2f);


            //
            // we should see both beacons now
            //
            {
                Assert.AreEqual(probe.discovered.Length, 2);
            }

            //
            // stop beacon2 and change it so that it uses a different sessionType
            // this should prevent our probe from adding it to the discovered list
            //

            beacon2.Stop();
            
            data2.sessionType = 0x11;
            data2.address = "127.0.0.2";
            data2.port = 1234;

            beacon2.Start(data2);

            yield return Utils.WaitOrTimeout(()=>{
                probe.Update();
                return probe.discovered.Length == 1;
            }, 5500);

            beacon2.Stop();

            Assert.AreEqual(1, probe.discovered.Length);
        }
    }
}
