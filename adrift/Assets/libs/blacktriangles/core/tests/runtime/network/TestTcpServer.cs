//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using blacktriangles.Network;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;
using UnityEngine.TestTools;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestTcpServer
    {
        //
        // constants //////////////////////////////////////////////////////////
        //

        private static readonly int kHostPort                   = 30010;
        private static readonly byte kSessionType               = 1;


        //
        // members ////////////////////////////////////////////////////////////
        //

        private TcpServer server                                = null;
        private Probe probe                                     = null;

        //
        // lifecycle //////////////////////////////////////////////////////////
        //

        [TearDown]
        public void TearDown()
        {
            if(server != null)
            {
                server.Stop();
                server = null;
            }

            if(probe != null)
            {
                probe.Stop();
                probe = null;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public void Update()
        {
            if(server != null) server.Update();
            if(probe != null)  probe.Update();
        }


        //
        // tests //////////////////////////////////////////////////////////////
        //

        [UnityTest]
        public IEnumerator TestLANServer()
        {
            //
            // create the probe and start listening
            //
            probe = new Probe();
            probe.Start(kSessionType);

            yield return Utils.DoFor(Update, 2.0);

            //
            //$TODO hsmith: Sometimes when running all tests this will fail because
            // a previous tests' probe is still alive.  See what I can do to make
            // sure that doesn't happen.
            Assert.AreEqual(probe.discovered.Length, 0);

            //
            // startup the server
            //
            server = new TcpServer();
            IPEndPoint ep = new IPEndPoint(IpUtils.GetLocalIp(), kHostPort);
            server.StartHost(ep, kSessionType, true);

            yield return Utils.WaitOrTimeout(()=>
            {
                Update();
                return server.state == NetworkState.Connected;
            }, 2.0);

            Assert.AreEqual(NetworkState.Connected, server.state);

            //
            // probe should have discovered us now
            //

            yield return Utils.WaitOrTimeout(()=>{
                Update();
                return probe.discovered.Length >= 1;
            }, 5f);

            Assert.AreEqual(1, probe.discovered.Length);

            //
            // the discovered data should be telling us the connection information
            // for the host
            //
            DiscoveryData data = probe.discovered[0].res.data;
            IPEndPoint expectedEndpoint = new IPEndPoint(IpUtils.GetLocalIp(), kHostPort);
            IPEndPoint discEndpoint = new IPEndPoint(IPAddress.Parse(data.address), data.port);
            Assert.AreEqual(kSessionType, data.sessionType);
            Assert.AreEqual(expectedEndpoint, discEndpoint);
        }
    }
}
 
