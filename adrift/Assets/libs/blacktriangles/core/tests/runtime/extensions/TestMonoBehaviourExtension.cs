//=============================================================================
//
// (C) BLACKTRIANGLES 2018
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;

namespace blacktriangles.Testing
{
    [TestFixture]
    public class TestMonoBehaviourExtension
    {
        // tests ///////////////////////////////////////////////////////////////
        [UnityTest]
        public IEnumerator BasicTest()
        {
            bool didInvoke = false;
            float startTime = Time.time;

            GameObject go = new GameObject();
            MyBehaviour behave = go.AddComponent<MyBehaviour>();

            // we can ask any behaviour to call a function or action after some arbitrary delay
            // here we ask to call this anonymous function that sets the didInvoke bool to true
            // after 1 second.
            behave.Invoke( ()=>{ didInvoke = true; }, 1f );

            // lets wait a maximum of 5 seconds for the didInvoke flag to be set.
            float elapsed = 0f;
            while( !didInvoke && elapsed < 5f )
            {
                yield return new WaitForSeconds( 0f );
                elapsed = Time.time - startTime;
            }

            // if we got here its either because we timed out (bad)
            // or the didInvoke flag was set (good).
            Assert.That( didInvoke );
            Assert.That( elapsed < 5f );

            // we should've waited at least 1 second before this happened.
            Assert.That( elapsed > 1f );

        }
    }

    // utility classes /////////////////////////////////////////////////////////
    public class MyBehaviour
        : MonoBehaviour
    {
    }


}
