//
// (c) GameJammers 2020
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Adrift
{
    public class PlayerRoomDisplay
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Interface")]
        public UIElement displayPanel;
        public TextMeshProUGUI displayText;
        public TextMeshProUGUI timeText;

        private Collider lastCollided                           = null;
        private UnityEngine.Coroutine crt;
        private System.DateTime startTime                       = new System.DateTime(1987, 9, 13, 4, 23, 22);

        //
        // private methods ////////////////////////////////////////////////////
        //

        private IEnumerator RoomChange(string newName)
        {
            displayPanel.Hide();
            displayText.text = newName;
            yield return displayPanel.ShowAsync(3f);
            yield return new WaitForSeconds(5f);
            yield return displayPanel.HideAsync(2f);
        }

        //
        // --------------------------------------------------------------------
        //
        
        private IEnumerator UpdateTime()
        {
            while(true)
            {
                System.TimeSpan elapsed = new System.TimeSpan(0, 0, (int)btMath.Round(Time.time));
                System.DateTime current = startTime + elapsed;
                timeText.text = current.ToString("F");
                yield return new WaitForSeconds(1f);
            }
        }
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            StartCoroutine(UpdateTime());
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void OnTriggerEnter(Collider other)
        {
            if(other == lastCollided) return;

            lastCollided = other;
            if(crt != null)
                StopCoroutine(crt);

            RoomName room = other.GetComponent<RoomName>();
            if(room != null)
            {
                crt = StartCoroutine(RoomChange(room.roomName));
            }
        }
    }
}
