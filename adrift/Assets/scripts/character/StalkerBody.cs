//
// (c) GameJammers 2020
// http://www.jamming.games
//

using UnityEngine;

namespace Adrift
{
    public class StalkerBody
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public Stalker stalker;
        public Animator animator                                { get; private set; }
        
        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Awake()
        {
            animator = GetComponent<Animator>();
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void OnAnimatorMove()
        {
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void OnAnimatorIK(int layerIndex)
        {
            animator.SetLookAtPosition(SceneManager.instance.sceneCam.transform.position);
            animator.SetLookAtWeight(1f);
        }
        
    }
}
