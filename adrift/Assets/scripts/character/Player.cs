//
// (c) GameJammers 2020
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using TMPro;
using UnityEngine;

namespace Adrift
{
    public class Player
        : SimpleFpsController
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        [Header("Light")]
        public Light flashlight;
        public Transform lightLocation;
        [Min(0.001f)] public float lightSpeed = 1.0f;
        public float lightTime                                  { get { return 1f / lightSpeed; } }

        [Header("Camera")]
        public float interactRange                              = 2f;
        public btCamera cam;
        public Interactable target;
        public Interactable interacting;

        [Header("Interface")]
        public TextMeshProUGUI interactText                     = null;
        public UISimpleMeter fixMeter                           = null;
        public Stalker stalker                                  = null;

        [Header("Audio")]
        public AmbientController ambientController              = null;
        public ReturnToPool defaultFootstep;
        public LayerMask indoorLayer;
        public bool indoors                                     { get; private set; }
        
        #if UNITY_EDITOR
            public bool invincible                              = false;
        #else
            private bool invincible                             = false;
        #endif

        private bool dead                                       = false;

        //
        // private methods ////////////////////////////////////////////////////
        //
        
        private IEnumerator Death()
        {
            stalker.Stop();

            yield return new WaitForSeconds(1f);

            stalker.animator.SetTrigger("attack");

            yield return new WaitForSeconds(4f);

            GameManager.ChangeScene(SceneId.Death);
        }

        //
        // --------------------------------------------------------------------
        //

        private IEnumerator Footsteps()
        {
            var wait = new WaitForSeconds(0.6f);

            while(true)
            {
                yield return wait;

                ReturnToPool footstep = defaultFootstep;
                if(isMoving && isGrounded && !SceneManager.instance.pause)
                {
                    RaycastHit hit = default(RaycastHit);
                    if(Physics.Raycast(transform.position, Vector3.down, out hit, 0.1f))
                    {
                        FootstepMaterial mat = hit.collider.GetComponent<FootstepMaterial>();
                        if(mat != null)
                        {
                            footstep = mat.footstepSound;
                            indoors = hit.collider.gameObject.layer == EnvironmentLayers.IndoorAreaId;
                        }
                    }

                    SceneManager.instance.pools.Take(footstep, transform.position, transform.rotation, null);

                }
            }
        }
        

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        #if !UNITY_EDITOR
        protected override void Awake()
        {
            base.Awake();
            invincible = false;
        }
        #endif

        //
        // --------------------------------------------------------------------
        //
        
        protected virtual void Start()
        {
            indoors = false;
            StartCoroutine(Footsteps());
        }

        //
        // --------------------------------------------------------------------
        //

        protected override void FixedUpdate()
        {
            if(dead) return;
            if(SceneManager.instance.pause) return;

            base.FixedUpdate();

            Vector3 stalkerDiff = stalker.transform.position - transform.position;
            float stalkerDist = stalkerDiff.magnitude;
            if(stalkerDist <= 5f && !invincible)
            {
                if(Physics.Raycast(transform.position, stalkerDiff.normalized, stalkerDist) == false)
                {
                    dead = true;
                    Dbg.Log("DEAD");
                    StartCoroutine(Death());
                    return;
                }
            }

            fixMeter.Hide();
            
            target = cam.MousePick<Interactable>(interactRange);
            interactText.text = System.String.Empty;
            if(target != null)
            {
                interactText.text = target.interactText;
                if(target is Dinghy == false)
                {
                    interactText.text += "\n Press [E] to Interact";
                }

                if(Input.GetKey(KeyCode.E))
                {
                    if(interacting != target)
                    {
                        interacting = target;
                        interacting.StartInteract();
                    }
                    else
                    {
                        interacting.Interact();
                        if(interacting is FixInteractable fixer)
                        {
                            fixMeter.Show();
                            fixMeter.SetValue(fixer.fixPercentage);
                        }
                    }
                }
                else if(interacting != null)
                {
                    interacting.StopInteract();
                    interacting = null;
                }
            }
            else if(interacting != null)
            {
                interacting.StopInteract();
                interacting = null;
            }

            Vector3 moveVel = Vector3.zero;
            flashlight.transform.position = lightLocation.position;
            flashlight.transform.localRotation = Quaternion.RotateTowards(
                flashlight.transform.localRotation, 
                lightLocation.transform.rotation, 
                90f * lightSpeed * Time.fixedDeltaTime);

        }
    }
}
