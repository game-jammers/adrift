//
// (c) GameJammers 2020
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

namespace Adrift
{
    public class Stalker
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public NavMeshAgent agent;
        public Animator animator;
        public Transform[] waypoints;
        public GameObject killCam                               = null;
        public GameObject teleportFx                            = null;
        public GameObject movementFx                            = null;

        public float chaseDistance                              = 20f;
        public float teleportCooldown                           = 5f;
        public float teleportDist                               = 50f;
        private float lastTeleport                              = 0f;

        public Transform target                                 = null;
        public Vector3 posTarget                                = Vector3.zero;
        public bool isMoving                                    { get; private set; }
        public bool isChasing                                   = false;

        private bool stopped                                    = false;
        private bool warpingOntoMesh                            = false;

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public void Stop()
        {
            stopped = true;
            isMoving = false;
            StopAllCoroutines();
            agent.enabled = false;
            killCam.SetActive(true);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void Warp()
        {
            if(!warpingOntoMesh)
            {
                warpingOntoMesh = true;
                StartCoroutine(WarpOntoMesh());
            }
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private IEnumerator ThinkCoroutine()
        {
            var wait = new WaitForSeconds(1f);
            var waitZero = new WaitForSeconds(0f);

            IEnumerator thought = Think();

            while(true)
            {
                if(SceneManager.instance.pause || stopped)
                {
                    agent.enabled = false;
                    agent.velocity = Vector3.zero;
                    yield return wait;
                }
                else
                {
                    agent.enabled = true;
                    thought.MoveNext();
                    yield return waitZero;
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        private IEnumerator Think()
        {
            var wait = new WaitForSeconds(0.33f);
            float nextTeleCooldown = teleportCooldown;
            while(!stopped)
            {
                isMoving = false;
                isChasing = false;
                Transform walkTarget = waypoints.Random();

                yield return new WaitForSeconds(btRandom.Range(1f, 5f));

                bool forceChase = false;
                float forceChaseStart = 0f;
                float dist = 10f;

                while((dist > 4f || isChasing) && !warpingOntoMesh)
                {
                    isMoving = true;
                    isChasing = false;
                    agent.isStopped = false;

                    Player player = SceneManager.instance.environment.player;
                    dist = Vector3.Distance(transform.position, player.transform.position);

                    if(dist < chaseDistance || forceChase)
                    {
                        if(forceChase)
                        {
                            // only force chase for 10 seconds after teleporting.
                            if(Time.time - forceChaseStart > 10f)
                            {
                                forceChase = false;
                            }
                        }
                        target = player.transform;
                        isChasing = true;
                    }
                    else if(dist > teleportDist && Time.time - lastTeleport > nextTeleCooldown)
                    {
                        // don't teleport if we can see the player
                        RaycastHit hit;
                        float checkDist = Vector3.Distance(transform.position, player.transform.position);
                        if(Physics.Raycast(transform.position + Vector3.up, player.transform.position + Vector3.up, out hit, checkDist)
                            && hit.collider.gameObject.tag == "Player")
                        {
                            forceChase = true;
                            forceChaseStart = Time.time;
                        }
                        else
                        {
                            lastTeleport = Time.time;
                            Vector3 teletarget = player.transform.position + Vector3Extension.Random(0.5f,0.5f,0.5f,1f,1f,1f) * teleportDist;
                            NavMeshHit navhit;
                            if(NavMesh.SamplePosition(teletarget, out navhit, 100, -1))
                            {
                                // only teleport if the player can't see the point we are teleporting to
                                checkDist = Vector3.Distance(navhit.position, player.transform.position);
                                if(checkDist > teleportDist * 0.5f)
                                {
                                    Vector3 vecFrom = navhit.position + Vector3.up;
                                    Vector3 vecTo = player.transform.position + Vector3.up;
                                    if(Physics.Raycast(vecFrom, vecTo, out hit, checkDist*0.9f))
                                    {
                                        if( hit.collider.gameObject != player.gameObject)
                                        {
                                            agent.Warp(navhit.position);
                                            forceChase = true;
                                            forceChaseStart = Time.time;
                                        }
                                    }
                                    nextTeleCooldown = btRandom.Range(1f, 1.5f) * teleportCooldown;
                                }
                            }
                        }

                    }
                    else
                    {
                        target = walkTarget;
                        dist = Vector3.Distance(target.position, transform.position);
                    }

                    if(agent.isOnNavMesh && target != null)
                    {
                        agent.SetDestination(target.transform.position);
                    }

                    yield return wait;
                }

                isMoving = false;
                isChasing = false;
                agent.isStopped = true;
                target = null;
            }
        }

        //
        // --------------------------------------------------------------------
        //

        private IEnumerator Footsteps()
        {
            var wait = new WaitForSeconds(1f);

            while(true && movementFx != null)
            {
                yield return wait;

                if(isMoving && !SceneManager.instance.pause)
                {
                    SceneManager.instance.pools.Take(movementFx, transform.position, transform.rotation, null);
                }
            }
        }
        

        //
        // --------------------------------------------------------------------
        //

        private IEnumerator WarpOntoMesh()
        {
            if(teleportFx != null)
            {
                SceneManager.instance.pools.Take(teleportFx, transform.position, Quaternion.identity, null);
            }

            animator.SetTrigger("roar");
            yield return new WaitForSeconds(3f);

            NavMeshHit navhit;
            if(NavMesh.SamplePosition(Vector3Extension.Random(-100f, -100f, -100f, 100f, 100f, 100f), out navhit, 200, -1))
            {
                agent.Warp(navhit.position);
            }
            else
            {
                yield return WarpOntoMesh();
            }

            yield return new WaitForSeconds(2f);

            warpingOntoMesh = false;
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        protected virtual void Start()
        {
            StartCoroutine(ThinkCoroutine());
            StartCoroutine(Footsteps());
            if(GameManager.instance.explore)
            {
                gameObject.SetActive(false);
            }
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void Update()
        {
            animator.SetBool("ismoving", isMoving);
        }
    }
}
