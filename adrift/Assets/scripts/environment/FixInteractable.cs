//
// (c) GameJammers 2020
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using UnityEngine;

namespace Adrift
{
    public abstract class FixInteractable
        : Interactable
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public override string interactText                     { get { return GetInteractText(); } }
        public bool working                                     { get; protected set; }

        [Header("Reference")]
        public GameObject brokenFx;
        public AudioSource fixingAudio;

        [Header("Settings")]
        public string brokenText                                = "Appears to be completely broken";
        public string partialText                               = "Appears to be fixed about {0}%";
        public string fixedText                                 = "Apperas to be fixed";
        public Vector2 breakTimeSec                             = new Vector2(120f,240f);
        public Vector2 fixTimeSec                               = new Vector2(3f,5f);

        [Header("State")]
        public float fixRequired                                = 4f;
        public float fixApplied                                 = 0f;
        public float fixPercentage                              { get { return fixApplied / fixRequired; } }

        public bool isInteracting                               { get; private set; }
        
        //
        // public methods /////////////////////////////////////////////////////
        //

        public override void StartInteract()
        {
            isInteracting = true;
            if(fixingAudio != null)
            {
                Dbg.Log("{0} PLAY", name);
                fixingAudio.Play();
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        
        public override void Interact()
        {
            if(!working)
            {
                fixApplied += Time.deltaTime;
                if(fixPercentage >= 1f)
                {
                    Fix();
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //

        public override void StopInteract()
        {
            isInteracting = false;
            if(fixingAudio != null)
            {
                Dbg.Log("{0} STOP", name);
                fixingAudio.Stop();
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        [ContextMenu("Break")]
        public virtual void Break()
        {
            if(working == true)
            {
                working = false;
                fixApplied = 0f;
                fixRequired = btRandom.Range(fixTimeSec.x, fixTimeSec.y);
                if(brokenFx != null)
                {
                    brokenFx.SetActive(true);
                }
            }
        }

        //
        // --------------------------------------------------------------------
        //

        [ContextMenu("Fix")]
        public virtual void Fix()
        {
            if(working == false)
            {
                working = true;
                if(brokenFx != null)
                {
                    brokenFx.SetActive(false);
                }

                StartCoroutine(BreakTimer());
            }
        }

        //
        // protected methods //////////////////////////////////////////////////
        //

        protected string GetInteractText()
        {
            if(working)
            {
                return fixedText;
            }
            else if(fixApplied > 0f)
            {
                return System.String.Format(partialText, btMath.Round(fixPercentage));
            }
            else
            {
                return brokenText;
            }
        }
        
        
        //
        // private methods ////////////////////////////////////////////////////
        //
        
        private IEnumerator BreakTimer()
        {
            while(working)
            {
                float waitTime = btRandom.Range(breakTimeSec.x, breakTimeSec.y);
                Dbg.Log("{0} Waiting for: {1}", name, waitTime);
                yield return new WaitForSeconds(waitTime);
                if(btRandom.Chance(0.5f))
                {
                    Break();
                }
            }
        }       

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            working = true;
            Break();
        }
    }
}
