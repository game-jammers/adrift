//
// (c) GameJammers 2020
// http://www.jamming.games
//

using blacktriangles;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Adrift
{
    public class Letter
        : Interactable
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        [System.Serializable]
        public struct LetterData
        {
            public string title;
            public string message;
        }
        
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("Settings")]
        public TextAsset text;
        public override string interactText                     { get { return data.title; } }
        public Sprite backgroundSprite                          = null;
        public bool found                                       { get; private set; }
        public int noteIdx                                      = -1;

        [Header("FX")]
        public ReturnToPool openFx;
        public ReturnToPool closeFx;

        [Header("Interface")]
        public UIElement panel;
        public TextMeshProUGUI title;
        public TextMeshProUGUI display;
        public Image background;

        private bool open                                       = false;
        private LetterData data;

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public void Open()
        {
            found = true;
            open = true;
            title.text = data.title;
            display.text = data.message;
            background.sprite = backgroundSprite;
            if(noteIdx >= 0)
            {
                GameManager.instance.notesFound.Add(noteIdx);
            }

            panel.Show(1f, null);
            SceneManager.instance.pause = true;
            SceneManager.instance.pools.Take(openFx, transform.position, transform.rotation, null);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void Close()
        {
            if(open)
            {
                open = false;
                panel.Hide(1f, null);
                SceneManager.instance.pause = false;
                SceneManager.instance.pools.Take(closeFx, transform.position, transform.rotation, null);
            }
        }
        
        //
        // interactable ///////////////////////////////////////////////////////
        //
        
        public override void StartInteract()
        {
        }

        //
        // --------------------------------------------------------------------
        //
        
        public override void Interact()
        {
            if(!open)
            {
                Open();
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public override void StopInteract()
        {
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Awake()
        {
            open = false;
            found = false;
            data.title = System.String.Empty;
            data.message = System.String.Empty;
            data = JsonUtility.FromJson<LetterData>(text.text);
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void Update()
        {
            if( open &&
                (Input.GetKey(KeyCode.Space) || 
                 Input.GetKey(KeyCode.W) ||
                 Input.GetKey(KeyCode.A) ||
                 Input.GetKey(KeyCode.S) ||
                 Input.GetKey(KeyCode.D) ||
                 Input.GetKey(KeyCode.Escape)))
            {
                Close();
            }
        }
    }
}
