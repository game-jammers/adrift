//
// (c) GameJammers 2020
// http://www.jamming.games
//

using UnityEngine;

namespace Adrift
{
    public class RoomName
        : MonoBehaviour
    {
        public string roomName                                  = "Room";
    }
}
