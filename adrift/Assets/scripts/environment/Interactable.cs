//
// (c) GameJammers 2020
// http://www.jamming.games
//

using UnityEngine;

namespace Adrift
{
    public abstract class Interactable
        : MonoBehaviour
    {
        public abstract string interactText                     { get; }

        public abstract void StartInteract();
        public abstract void Interact();
        public abstract void StopInteract();
    }
}
