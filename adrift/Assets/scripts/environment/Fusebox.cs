//
// (c) GameJammers 2020
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using UnityEngine;

namespace Adrift
{
    public class Fusebox
        : FixInteractable
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public Light[] lights;
        [Range(0,2)] public int fuseIdx;

        public GameObject fixedArea                             = null;
        public BoxCollider safeZone                             = null;

        //
        // public methods /////////////////////////////////////////////////////
        //
        
        public override void Break()
        {
            base.Break();

            foreach(Light light in lights)
            {
                light.gameObject.SetActive(false);
            }

            if(fixedArea != null)
            {
                fixedArea.SetActive(false);
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public override void Fix()
        {
            base.Fix();

            foreach(Light light in lights)
            {
                light.gameObject.SetActive(true);
            }

            if(fixedArea != null)
            {
                fixedArea.SetActive(true);
            }

            Stalker stalker = SceneManager.instance.environment.stalker;
            if(safeZone.bounds.ClosestPoint(stalker.transform.position) == stalker.transform.position)
            {
                stalker.Warp();
            }
        }

        //
        // editor /////////////////////////////////////////////////////////////
        //

        #if UNITY_EDITOR
        [ContextMenu("Collect Lights")]
        private void CollectLights()
        {
            lights = GetComponentsInChildren<Light>();
        }
        #endif
    }
}
