//
// (c) GameJammers 2020
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using UnityEngine;

namespace Adrift
{
    public class Dinghy
        : Interactable
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public override string interactText                     { get { return GetInteractText(); } }
        public bool working                                     { get; private set; }

        //
        // public methods /////////////////////////////////////////////////////
        //

        public override void StartInteract()
        {
            if(SceneManager.instance.environment.escape)
            {
                GameManager.ChangeScene(SceneId.End);
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        public override void Interact()
        {
        }

        //
        // --------------------------------------------------------------------
        //

        public override void StopInteract()
        {
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private string GetInteractText()
        {
            if(SceneManager.instance.environment.escape)
            {
                return "Escape the ship on the life raft!";
            }
            else
            {
                return "It is too far from land to escape using this raft.";
            }
        }
    }
}
