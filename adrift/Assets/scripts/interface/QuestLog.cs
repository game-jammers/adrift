//
// (c) GameJammers 2020
// http://www.jamming.games
//

using blacktriangles;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Adrift
{
    public class QuestLog
        : UIElement
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public UIList quests;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Refresh(List<string> questlines)
        {
            quests.SetData(questlines);
        }
    }
}
