//
// (c) GameJammers 2020
// http://www.jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Adrift
{
    public class StartGameTrigger
        : MonoBehaviour
    {
        public void StartGame()
        {
            GameManager.ChangeScene(SceneId.Game);
        }
    }
}
