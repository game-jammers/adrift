//
// (c) GameJammers 2020
// http://www.jamming.games
//

using blacktriangles;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Adrift
{
    public class QuestLogListItem
        : UIListItem
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public string display                                   { get { return (string)data; } }
        public QuestLogItem item;
        
        //
        // public methods /////////////////////////////////////////////////////
        //

        public override bool Refresh()
        {
            base.Refresh();
            item.Refresh(display);
            return true;
        }
        
    }
}
