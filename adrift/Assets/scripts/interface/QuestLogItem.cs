//
// (c) GameJammers 2020
// http://www.jamming.games
//

using blacktriangles;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Adrift
{
    public class QuestLogItem
        : UIElement
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public TextMeshProUGUI display;
        
        //
        // public methods /////////////////////////////////////////////////////
        //

        public void Refresh(string quest)
        {
            display.text = quest;
        }
        
    }
}
