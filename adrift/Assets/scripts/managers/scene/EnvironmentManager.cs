//
// (c) GameJammers 2020
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Adrift
{
    public class EnvironmentManager
        : MonoBehaviour
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        [Header("References")]
        public Player player                                    = null;
        public Stalker stalker                                  = null;
        public QuestLog questlog                                = null;

        [Header("Audio")]
        public AudioCrossFader music                            = null;
        public AudioClip drone                                  = null;
        public AudioClip chase                                  = null;
        
        [Header("State")] 
        public Fusebox[] fuseboxes;
        public Steering steering;
        public Engine engine;
        public Letter[] letters;
        public bool escape                                      { get { return totalProgress >= requiredProgress; } }

        public int totalProgress                                = 0;
        public float requiredMinutes                            = 5f;
        public int requiredProgress                             { get { return (int)btMath.Round(4 * 60 * requiredMinutes); } }

        //
        // public methods //////////////////////////////////////////////////////
        //
        
        public List<string> GetQuestLines()
        {
            List<string> result = new List<string>();

            if(escape)
            {
                result.Add("Escape the ship on the life raft!");
            }
            else
            {
                // main quest
                if(!steering.working)
                {
                    result.Add(System.String.Format("Reset ship heading at the helm ({0}%)", btMath.Round(steering.fixPercentage*100f)));
                }

                if(!engine.working)
                {
                    result.Add(System.String.Format("Repair engine ({0}%).", btMath.Round(engine.fixPercentage*100f)));
                }

                // letters
                int letterCount = 0;
                foreach(Letter letter in letters)
                {
                    if(!letter.found) ++letterCount;
                }

                if(letterCount > 0)
                {
                    result.Add(System.String.Format("(Optional) Discover letters. {0}/{1}", letters.Length - letterCount, letters.Length));
                }

                if(engine.working && steering.working)
                {
                    result.Add(System.String.Format("Wait to reach land. ({0}%)", btMath.Round(totalProgress/(float)requiredProgress*100f)));
                    totalProgress += 1;
                }
            }

            return result;
        }

        //
        // private methods ////////////////////////////////////////////////////
        //

        private void Refresh()
        {
            questlog.Refresh(GetQuestLines());

            if(stalker.isChasing)
            {
                music.CrossFade(chase, 2f);
            }
            else
            {
                music.CrossFade(drone, 5f);
            }
        }

        //
        // --------------------------------------------------------------------
        //
        
        private IEnumerator UIRefresh()
        {
            var wait = new WaitForSeconds(0.25f);
            while(true)
            {
                yield return wait;
                Refresh();
            }
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected virtual void Start()
        {
            StartCoroutine(UIRefresh());
        }
    }
}
