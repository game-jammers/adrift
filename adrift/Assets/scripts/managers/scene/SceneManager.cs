//
// (c) Game Jammers 2019
// http://jamming.games
//

using blacktriangles;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace Adrift
{
    public class SceneManager
        : BaseSceneManager
    {
        //
        // events //////////////////////////////////////////////////////////////
        //

        public delegate void SceneReadyCallback();
        public event SceneReadyCallback OnSceneReady;

        //
        // members /////////////////////////////////////////////////////////////
        //

        public static new SceneManager instance                 { get; private set; }

        public bool pause                                       = false;
        public GameObjectPools pools                            = null;
        public EnvironmentManager environment                   = null;
        public Volume globalVolume                              = null;

        //
        // unity callbacks /////////////////////////////////////////////////////
        //

        protected override void Awake()
        {
            base.Awake();
            instance = this;
            GameManager.EnsureExists();
        }

        //
        // --------------------------------------------------------------------
        //

        protected virtual void Start()
        {
            if(globalVolume != null)
            {
                float gamma = GameManager.instance.settings.brightnessAdjust;
                LiftGammaGain lgg = null;
                if(globalVolume.sharedProfile.TryGet<LiftGammaGain>(out lgg))
                {
                    lgg.gamma.value = new Vector4(0f, 0f, 0f, gamma);
                }
            }
        }

        //
        // protected methods ///////////////////////////////////////////////////
        //
        protected void NotifySceneReady()
        {
            if( OnSceneReady != null )
            {
                OnSceneReady();
            }
        }
    }
}
