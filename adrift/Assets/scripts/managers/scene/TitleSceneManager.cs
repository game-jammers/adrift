//
// (c) GameJammers 2020
// http://www.jamming.games
//

using blacktriangles;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;
using UnityEngine.UI;

namespace Adrift
{
    public class TitleSceneManager
        : SceneManager
    {
        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public TextMeshProUGUI helpText;
        public Slider gammaSlider;

        //
        // public methods /////////////////////////////////////////////////////
        //

        public void HoverStory()
        {
            #if UNITY_WEBGL
                helpText.text = "Reduced threat gives you freedom to explore the MS Miskatonic (Disabled in web)";
            #else
                helpText.text = "Reduced threat gives you freedom to explore the MS Miskatonic.";
            #endif
        }

        //
        // --------------------------------------------------------------------
        //

        public void HoverSurvival()
        {
            #if UNITY_WEBGL
                helpText.text = "The darkness pursues your relentlessly.  The way the game was designed to be played. (Disabled in web)";
            #else
                helpText.text = "The darkness pursues your relentlessly.  The way the game was designed to be played.";
            #endif
        }

        //
        // --------------------------------------------------------------------
        //

        public void HoverDemo()
        {
            helpText.text = "Demo mode (primarily for Web plaform) with reduced game features and graphics due to platform limitations.";
        }
        
        //
        // --------------------------------------------------------------------
        //
        
        public void ClearHelpText()
        {
            helpText.text = System.String.Empty;
        }

        //
        // --------------------------------------------------------------------
        //

        public void StartSurvival()
        {
            GameManager.instance.explore = false;
            GameManager.ChangeScene(SceneId.Game);
        }

        //
        // --------------------------------------------------------------------
        //

        public void StartExplore()
        {
            GameManager.instance.explore = true;
            GameManager.ChangeScene(SceneId.Game);
        }
        
        //
        // --------------------------------------------------------------------
        //

        public void AdjustGamma()
        {
            float gamma = gammaSlider.value;
            GameManager.instance.settings.brightnessAdjust = gamma;
            LiftGammaGain lgg = null;
            if(globalVolume.sharedProfile.TryGet<LiftGammaGain>(out lgg))
            {
                lgg.gamma.value = new Vector4(0f, 0f, 0f, gamma);
            }
        }
    }
}
