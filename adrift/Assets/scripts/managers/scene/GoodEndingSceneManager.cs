//
// (c) GameJammers 2020
// http://www.jamming.games
//

using blacktriangles;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Adrift
{
    public class GoodEndingSceneManager
        : SceneManager
    {
        //
        // members ////////////////////////////////////////////////////////////
        //

        public UIElement panel;
        public UIElement content;
        public TextMeshProUGUI notesDisplay;
        public TextAsset[] allNotes;
        public float scrollSpeed                                = 1;

        //
        // unity callbacks ////////////////////////////////////////////////////
        //

        protected override void Start()
        {
            base.Start();
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            for(int i = 0; i < allNotes.Length; ++i)
            {
                if(GameManager.instance.notesFound.Contains(i))
                {
                    string json = allNotes[i].text;
                    Letter.LetterData data = JsonUtility.FromJson<Letter.LetterData>(json);
                    sb.AppendLine(data.message);
                }
                else
                {
                    sb.AppendLine("<Missing>");
                }

                sb.AppendLine(System.String.Empty);
            }
            notesDisplay.text = sb.ToString();
        }

        //
        // --------------------------------------------------------------------
        //

        public void Update()
        {
            if(content != null)
            {
                content.rectTransform.offsetMax -= new Vector2(0f, scrollSpeed * Time.deltaTime);
            }
        }
        
    }
}
