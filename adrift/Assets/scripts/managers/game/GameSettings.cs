//
// (c) Game Jammers 2019
// http://jamming.games
//

using UnityEngine;

namespace Adrift
{
    [System.Serializable]
    public struct GameSettings
    {
        //
        // types //////////////////////////////////////////////////////////////
        //

        //
        // members ////////////////////////////////////////////////////////////
        //

        public float brightnessAdjust;
        
        //
        // default ////////////////////////////////////////////////////////////
        //

        public static GameSettings Default
        {
            get 
            {
                return new GameSettings() {
                    brightnessAdjust = 0f
                };
            }
        }
    };
}
