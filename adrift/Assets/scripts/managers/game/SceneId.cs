//
// (c) Game Jammers 2019
// http://jamming.games
//

using blacktriangles;
using UnityEngine;

namespace Adrift
{
    public enum SceneId
    {
        Title   = 0,
        Game    = 1,
        End     = 2,
        Death   = 3
    }
}
