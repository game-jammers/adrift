//
// (c) Game Jammers 2019
// http://jamming.games
//

using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Adrift
{
    public class GameManager
        : BaseGameManager
    {
        //
        // constants //////////////////////////////////////////////////////////
        //
        
        public static readonly string kPrefabPath               = "managers/GameManager";

        //
        // accessors ////////////////////////////////////////////////////////////
        //

        public static new GameManager instance                  { get; private set; }

        //
        // members ////////////////////////////////////////////////////////////
        //
        
        public GameSettings settings                            = GameSettings.Default;
        public bool explore                                     = false;
        public List<int> notesFound                             = new List<int>();

        //
        // public methods /////////////////////////////////////////////////////
        //

        public static GameManager EnsureExists()
        {
            if( instance == null )
            {
                instance = EnsureExists<GameManager>( kPrefabPath );
                instance.name = "GameManager";
            }

            return instance;
        }

        //
        // --------------------------------------------------------------------
        //

        public static void ChangeScene( SceneId scene )
        {
            UnityEngine.SceneManagement.SceneManager.LoadSceneAsync( (int)scene, UnityEngine.SceneManagement.LoadSceneMode.Single );
        }

        //
        // --------------------------------------------------------------------
        //

        public static void AddScene( SceneId scene )
        {
            UnityEngine.SceneManagement.SceneManager.LoadSceneAsync( (int)scene, UnityEngine.SceneManagement.LoadSceneMode.Additive );
        }

        //
        // --------------------------------------------------------------------
        //

        public void ApplySettings()
        {
            ApplySettings(settings);
        }

        //
        // --------------------------------------------------------------------
        //
        
        public void ApplySettings(GameSettings settings)
        {
            this.settings = settings;
        }

        //
        // unity callbacks ////////////////////////////////////////////////////
        //
        
        protected override void Awake()
        {
            base.Awake();
            ApplySettings();
        }
    };
}
